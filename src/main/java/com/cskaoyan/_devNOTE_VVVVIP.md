1. 各自的模块在**各层**新建一个包，公共的先放在外面
2. 
3. 文件上传已经有工具类了,配置类已经配好 ，
4. 需要类似这个需求 可以用BeanCreateUtils
```java
    //
    public static StorageListVO create(Long total,Integer pages, Integer limit,Integer page, List<StorageListPOJO> list){
        StorageListVO storageListVO = new StorageListVO();
        storageListVO.setTotal(Math.toIntExact(total));
        storageListVO.setPages(pages);
        storageListVO.setLimit(limit);
        storageListVO.setPage(page);
        storageListVO.setList(list);
        return storageListVO;
    }
```
可以这样用
```java
StorageListPOJO bean = BeanCreateUtils.createBean(StorageListPOJO.class, null, fileInfo.getKey(), fileInfo.getName(), fileInfo.getType(), Math.toIntExact(fileInfo.getSize()), fileInfo.getUrl(), add_time, update_time, false);
```


#问题记录



## 1 json参数没有接受成功

![](https://s2.loli.net/2022/01/06/43LGarVU7KfE1eO.png)

## 2 可能的扩展性不好

现在因为前端传过来的数据比较多，但是用的可能只有一两个，比如删除，全部都穿过来，其实只用到id

编辑，也就是修改，因为前端只有修改名称，所以也只用到了一个，但是为了拓展性，

还是全部都修改吗？即使没有修改。



## 3 懒加载导致的json封装问题

com.fasterxml.jackson.databind.exc.InvalidDefinitionException: No serializer found for class org.apache.ibatis.executor.loader.javassist.JavassistProxyFactory$EnhancedResultObjectProxyImpl and no properties discovered to create BeanSerializer (to avoid exception, disable SerializationFeature.FAIL_ON_EMPTY_BEANS) (through reference chain: xyz.cybertheye.mall36.model.BaseRespVo["data"]->xyz.cybertheye.mall36.model.mallmanage.RegionVO["list"]->java.util.ArrayList[0]->xyz.cybertheye.mall36.model.mallmanage.ProvinceDTO_$$_jvstf65_0["children"]->java.util.ArrayList[0]->xyz.cybertheye.mall36.model.mallmanage.CityDTO_$$_jvstf65_1["handler"])

![](https://s2.loli.net/2022/01/07/5TnmY6Ra2i4bP9O.png)

![](https://s2.loli.net/2022/01/07/HQW6XePMFmlwR7I.png)

解决后

没有handler这一块

![](https://s2.loli.net/2022/01/07/AM4dCgLmwVsyUft.png)



## 4 MultipartFile 为null

![](https://s2.loli.net/2022/01/07/BqjJsmHG3a7zv2c.png)

请求参数名和形参名不一致

![](https://s2.loli.net/2022/01/07/WLhUJpd6xy2CckN.png)



## 5 split(".") -> split("\\\\\.")

![](https://s2.loli.net/2022/01/07/BetpgDEfaydNcVA.png)



## 6 @Value 为null

![](https://s2.loli.net/2022/01/07/xn75NKLwlqRvUSD.png)

FileUploadUtils不是容器中的组件！！！

![](https://s2.loli.net/2022/01/07/DCfPWHB2aZnovtb.png)

需要在application.yml中配置

![](https://s2.loli.net/2022/01/07/bIf6Ax1QNMhys2p.png)

![](https://s2.loli.net/2022/01/07/z4OUGBJyNXCLebw.png)

还是为null

不能给static成员变量赋值，因为它的初始化是在类加载

在controller层里面去@Value

## 7 如果要在POJO里面获取配置文件中的？

![](https://s2.loli.net/2022/01/07/kYqTfIP1ZXgexnv.png)

url需要加上basePath的值



## 8 反射的getDeclaredFields只能拿到自己的，如果有爸爸，拿不到爸爸的成员变量

![](https://s2.loli.net/2022/01/07/IELHJvB2XdeKUDS.png)



## 9  AdminServiceImpl里面的getAdminInfo，

![](https://s2.loli.net/2022/01/07/BHNT5bJ21Fsz6vW.png)

![](https://s2.loli.net/2022/01/07/U8PxhAXwcTojHNW.png)

username写在baseParam后面好像也可以

AdminVO的内部类提到外面来就好了

这里的问题是

![](https://s2.loli.net/2022/01/07/32DbstrypFNvuRU.png)

total是Integer类型

我传入的是long类型的

![](https://s2.loli.net/2022/01/07/mujDniUYXRVqTba.png)

强转一下

![](https://s2.loli.net/2022/01/07/ezJIj2u86bFcRgT.png)



## 10 admin/admin/list 请求返回是有的，但是前端页面显示不出来

![](https://s2.loli.net/2022/01/07/kaLpTjbofsl7rmy.png)



## 11 怎么获取ip？





## 12 为什么mybatis的提示里面没有role_ids

<img src="https://s2.loli.net/2022/01/07/tNsUaE3z4RbHO58.png" style="zoom:33%;" />

<img src="https://s2.loli.net/2022/01/07/QY9GJHzny7R3Z2V.png" style="zoom:33%;" />

这里明明有





## 13 新增管理员这里的头像上传后显示不出来

![](https://s2.loli.net/2022/01/07/rcAniR1m4U9KT3F.png)

# 
