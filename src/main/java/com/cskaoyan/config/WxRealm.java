package com.cskaoyan.config;

import com.cskaoyan.dao.wx.homemanage.WXLoginDao;
import com.cskaoyan.model.realm.UsernamePasswordTypeToken;
import com.cskaoyan.model.realm.WxLoginUserData;
import com.cskaoyan.utils.BeanCreateUtils;
import com.cskaoyan.utils.DateUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 * @author : [Dragon]
 * @createTime : [2022/1/10 19:43]
 * @description :
 */
@Component
public class WxRealm extends AuthorizingRealm {

    @Autowired
    WXLoginDao wxLoginDao;


    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
/*        //把当前用户的所有的权限信息找到
        //首先获得当前用户信息 → doGetAuthen方法封装的第一个参数
        String primaryPrincipal = (String) principalCollection.getPrimaryPrincipal();
        //根据用户信息查询权限列表
        List<String> permissions = queryPermissionFromDB(primaryPrincipal);
        SimpleAuthorizationInfo simpleAuthorizationInfo = new SimpleAuthorizationInfo();
        simpleAuthorizationInfo.addStringPermissions(permissions);*/
        return null;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        //用户名
        UsernamePasswordTypeToken token = (UsernamePasswordTypeToken) authenticationToken;
        String principal = (String) token.getPrincipal();

        //根据数据库查询密码
        String credentials = queryFromDb(principal);

        //查询到需要的相关信息
        Integer id = queryUserIdFromDb(principal);
        String nowTime = DateUtils.getNowDateTime();
        String login_ip = token.getHost();
        WxLoginUserData bean = null;
        try {
            bean = BeanCreateUtils.createBean(WxLoginUserData.class, id, principal, nowTime, login_ip);
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        //参数1：principal 认证成功后保存在session中的用户信息（自己决定） → shiro提供的方法获得（subject的方法）
        //参数2：credentials 凭证信息，来源于数据库的凭证信息
        //参数3：realm的名称
        return new SimpleAuthenticationInfo(bean, credentials, getName());
        //构造认证信息：会和token中的密码做比对
    }

    private Integer queryUserIdFromDb(String principal) {
        //todo !!!根据username 选出userid
        Integer id = wxLoginDao.selectUserIdByUsername(principal);
        return id;
    }

/*    private List<String> queryPermissionFromDB(String primaryPrincipal) {
        //todo: 来源于数据库
        return Arrays.asList(new String[]{"wx:user:index", "wx:auth:login"});
    }*/

    //应该是使用dao层，查询该用户对应的密码
    private String queryFromDb(String principal) {
        //todo: 查询principal信息在数据库中的密码
        String password = wxLoginDao.selectPassword(principal);
        return password;
    }
}
