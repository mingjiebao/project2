package com.cskaoyan.config;

import com.cskaoyan.market.config.MarketSessionManager;
import com.cskaoyan.market.config.ShiroProperties;
import org.apache.shiro.realm.Realm;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.apache.shiro.web.session.mgt.DefaultWebSessionManager;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author:
 * @description:
 * @keypoint:
 * @tags:
 * @related:
 */

/*@Configuration
@EnableConfigurationProperties({ShiroProperties.class})*/
public class ShiroConfig {
//    ShiroProperties shiroProperties;
//
//    public ShiroConfig(ShiroProperties shiroProperties) {
//        this.shiroProperties = shiroProperties;
//    }


    @Bean
    public DefaultWebSecurityManager securityManager(WxRealm wxRealm, AdminRealm adminRealm ,DefaultWebSessionManager sessionManager,CustomModularRealmAuthenticator modularRealmAuthenticator) {
        DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();
        List<Realm> realm = new ArrayList<>();
        realm.add(wxRealm);
        realm.add(adminRealm);
        securityManager.setRealms(realm);
        securityManager.setAuthenticator(modularRealmAuthenticator);
        securityManager.setSessionManager(sessionManager);


        return securityManager;
    }

    @Bean
    public CustomModularRealmAuthenticator modularRealmAuthenticator(AdminRealm adminRealm,WxRealm wxRealm){
        //这个认证器需要的realm，要set进去
        CustomModularRealmAuthenticator authenticator = new CustomModularRealmAuthenticator();
        ArrayList<Realm> realms = new ArrayList<>();
        realms.add(adminRealm);
        realms.add(wxRealm);
        authenticator.setRealms(realms);
        return authenticator;
    }

    @Bean
    public DefaultWebSessionManager sessionManager() {
        String admin = "X-CskaoyanMarket-Admin-Token";
        String wx = "X-CskaoyanMarket-Token";
        return new MarketSessionManager(admin,wx);
    }
}
