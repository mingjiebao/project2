package com.cskaoyan.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import java.util.HashMap;

/**
 * @description: 文件上传的配置类
 * @keypoint:
 * @tags:
 * @related:
 */

@Configuration
public class FileUploadConfig {

    @Value("${upload.macBasePath}")
    String macBasePath;

    @Value("${upload.windowsBasePath}")
    String windowsBasePath;

//    @Bean
//    public MultipartResolver multipartResolver(){
//        return new CommonsMultipartResolver();
//    }


    @Bean
    public HashMap<String,String> uploadPath(){
        HashMap<String, String> path= new HashMap<>();
        path.put("mac",macBasePath);
        path.put("windows",windowsBasePath);
        return path;
    }



}
