package com.cskaoyan.config;

import com.cskaoyan.interceptor.AdminManageHandlerInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.HashMap;

/**
 * @Author:
 * @description:
 * @keypoint:
 * @tags:
 * @related:
 */
@Component
public class ProjectMallConfig implements WebMvcConfigurer {


    @Autowired
    HashMap<String,String> uploadPath;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new AdminManageHandlerInterceptor()).addPathPatterns("/admin/**");
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {

        String os = System.getProperty("os.name").toLowerCase();
        String basePath = null;
        if(os.contains("mac")){
            basePath = uploadPath.get("mac");
        }else{
            basePath = uploadPath.get("windows");
        }

        basePath = "file:"+basePath + "adminmanage/";
        registry.addResourceHandler("/adminmanage/**")
                .addResourceLocations(basePath);
    }

}
