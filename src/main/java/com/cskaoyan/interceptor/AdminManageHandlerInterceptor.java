package com.cskaoyan.interceptor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.regex.Pattern;

/**
 * @Author:
 * @description:
 * @keypoint:
 * @tags:
 * @related:
 */

@Component
public class AdminManageHandlerInterceptor implements HandlerInterceptor {

//    @Autowired
//    HttpServletRequest req;
//    @Autowired
//    HttpSession session;
//
//    @Override
//    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
//
//        //todo 管理员的添加，删除，修改 ； 角色管理 -- 只能是超级管理员
//        String servletPath = req.getServletPath();
//        String op = servletPath.replace("/admin/", "");
//        String[] split = op.split("/");
//        String module = split[0];
//        String function = split[1];
//
//        //
//        if ("role".equals(module) || "admin".equals(module)) {
//            if (!function.contains("list") && !function.contains("read")) {
//                //从 session里面取出来
//                String adminName = (String) session.getAttribute("admin");
//
//                if (!"admin123".equals(adminName)) {//如果不一样，不能修改没有权限
//                    return false;
//                }
//            }
//        }
//        return true;
//    }
}
