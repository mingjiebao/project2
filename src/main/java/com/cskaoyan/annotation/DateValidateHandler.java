package com.cskaoyan.annotation;

import com.cskaoyan.exception.ParamsException;
import com.cskaoyan.model.admin.coupon.bo.CouponCreateInfoBo;
import lombok.SneakyThrows;

import javax.validation.ClockProvider;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/***
 *  用户验证日期的正确性，天数必须为整数，开始日期早于结束日期，且都晚于当前日期。
 */
public class DateValidateHandler implements ConstraintValidator<DateValidate, CouponCreateInfoBo> {
    @Override
    public boolean isValid(CouponCreateInfoBo couponInfoBo, ConstraintValidatorContext constraintValidatorContext) {
        if (couponInfoBo.getTimeType() == 0) {
            if (couponInfoBo.getDays() <= 0) {
                return false;
            }
            return true;
        } else {
            if (couponInfoBo.getStartTime() == null || couponInfoBo.getEndTime() == null) {
                return false;
            }
            String now = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(LocalDateTime.now());
            String endTime = couponInfoBo.getEndTime();
            String startTime = couponInfoBo.getStartTime();

            if (now.compareTo(endTime) >= 0 || now.compareTo(startTime) >= 0 || endTime.compareTo(startTime) <= 0) {
                return false;
            }
            return true;

        }


    }
}
