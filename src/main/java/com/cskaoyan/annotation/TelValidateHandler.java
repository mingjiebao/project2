package com.cskaoyan.annotation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * @author : [Dragon]
 * @createTime : [2022/1/11 17:22]
 * @description :
 */
public class TelValidateHandler implements ConstraintValidator<TelValidate, String> {
    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        if (s == null || s.isEmpty()) {
            return false;
        }

        try {
            char[] chars = s.toCharArray();
            //手机号位数11位
            if (chars.length != 11) {
                return false;
            }
            //是否为正整数
            for (char aChar : chars) {
                int i = Integer.parseInt(String.valueOf(aChar));
            }
        } catch (Exception e) {
            return false;
        }

        return true;
    }
}
