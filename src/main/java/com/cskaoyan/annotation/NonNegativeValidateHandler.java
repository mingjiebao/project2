package com.cskaoyan.annotation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/***
 *  用于验证一个字符串小数，必须满足为正数
 */
public class NonNegativeValidateHandler implements ConstraintValidator<NonNegativeValidate, String> {
    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {

        if (s == null || s.isEmpty()) {
            return false;
        }
        try {
            final double v = Double.parseDouble(s);
            if (v > 0) {
                return true;
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }
}
