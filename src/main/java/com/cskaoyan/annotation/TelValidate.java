package com.cskaoyan.annotation;


import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @createTime: 2022/1/11 17:36
 * @author: Dragon
 * @description: 手机号验证
 * */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE, ElementType.CONSTRUCTOR, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = TelValidateHandler.class)
public @interface TelValidate {
    String message();

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
