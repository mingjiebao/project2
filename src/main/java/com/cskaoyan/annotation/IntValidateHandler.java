package com.cskaoyan.annotation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class IntValidateHandler implements ConstraintValidator<IntValidate, String> {
    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        if (s == null || s.isEmpty()) {
            return false;
        }

        try {
            int result = Integer.parseInt(s);
            return result >= 0;
        } catch (Exception e) {
            return false;
        }
    }
}
