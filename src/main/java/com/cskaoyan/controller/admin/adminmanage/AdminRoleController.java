package com.cskaoyan.controller.admin.adminmanage;

import com.cskaoyan.exception.adminmanage.AdminRoleException;
import com.cskaoyan.model.BaseParam;
import com.cskaoyan.model.BaseRespVo;
import com.cskaoyan.model.admin.adminmanage.role.*;
import com.cskaoyan.service.admin.adminmanage.RoleService;
import com.cskaoyan.utils.AdminUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

/**
 * @description:
 * @keypoint:
 * @tags:
 * @related:
 */
@RequestMapping("admin/role")
@RestController
public class AdminRoleController {

    @Autowired
    RoleService roleServiceImpl;


    @RequestMapping("options")
    public BaseRespVo<RoleVO> showAllRole() throws InstantiationException, IllegalAccessException {
        RoleVO roleVO = roleServiceImpl.getSimpleRole();
        return BaseRespVo.ok(roleVO);
    }


    @RequestMapping(value = "build")
    public BaseRespVo createSystem(@RequestBody SystemPermissonVO systemPermissonVO) throws AdminRoleException {
        roleServiceImpl.initDataBase(systemPermissonVO);

        return BaseRespVo.ok();
    }

    /**
     * post方法
     * @return
     */
    @PostMapping("permissions")
    public BaseRespVo updatePermission(@RequestBody PermissionUpdateBO permissionUpdateBO){
        roleServiceImpl.updateRolePermission(permissionUpdateBO);
//        String name = roleServiceImpl.selectRoleNameById(permissionUpdateBO.getRoleId());
//        Session session = SecurityUtils.getSubject().getSession();
//        session.setAttribute("resultValue",name);
        return BaseRespVo.ok();
    }

    @GetMapping("permissions")
    public BaseRespVo<RolePermissionVO> getRolePermission(Integer roleId) throws InstantiationException, IllegalAccessException {

        RolePermissionVO rolePermissionVO = roleServiceImpl.getRolePermission(roleId);

        return BaseRespVo.ok(rolePermissionVO);
    }

    @RequestMapping("list")
    public BaseRespVo<RoleDetailVO> show(BaseParam baseParam,String name) throws InstantiationException, IllegalAccessException {
        RoleDetailVO roleDetailVO =  roleServiceImpl.getRoleDetail(baseParam,name);
        return BaseRespVo.ok(roleDetailVO);
    }


    @RequestMapping("create")
    public BaseRespVo<RoleDetailDTO> create(@RequestBody RoleCreateBO roleCreateBO) throws InstantiationException, IllegalAccessException, AdminRoleException {
        RoleDetailDTO roleDetailDTO = roleServiceImpl.createNewRole(roleCreateBO);
        Session session = SecurityUtils.getSubject().getSession();
        session.setAttribute("resultValue",roleCreateBO.getName());
        return BaseRespVo.ok(roleDetailDTO);
    }

    @RequestMapping("update")
    public BaseRespVo update(@RequestBody RoleUpdateDeleteBO roleUpdateDeleteBO) throws AdminRoleException {
        roleServiceImpl.updateRoleInfo(roleUpdateDeleteBO);

        Session session = SecurityUtils.getSubject().getSession();
        session.setAttribute("resultValue",roleUpdateDeleteBO.getName());

        return BaseRespVo.ok();
    }


    @RequestMapping("delete")
    public BaseRespVo delete(@RequestBody RoleUpdateDeleteBO roleUpdateDeleteBO) throws AdminRoleException {
        roleServiceImpl.deleteRoleInfo(roleUpdateDeleteBO);

        Session session = SecurityUtils.getSubject().getSession();
        session.setAttribute("resultValue",roleUpdateDeleteBO.getName());
        return BaseRespVo.ok();
    }

}
