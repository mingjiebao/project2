package com.cskaoyan.controller.admin.adminmanage;

import com.cskaoyan.exception.ParamsException;
import com.cskaoyan.exception.adminmanage.AdminAdminException;
import com.cskaoyan.model.BaseParam;
import com.cskaoyan.model.BaseRespVo;
import com.cskaoyan.model.admin.adminmanage.admin.*;
import com.cskaoyan.service.admin.adminmanage.AdminService;
import com.cskaoyan.utils.AdminUtils;
import com.cskaoyan.utils.ValidationUtil;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

/**
 * @description:
 * @keypoint:
 * @tags:
 * @related: 为什么要用TypeHandler，主要是因为数据里面保存的[1,2,3]数组数据，用字符串的形式保存，
 * 但我们在java中需要处理整型数组，所以在mybatis的输出输入映射的时候加以处理
 * TypeHandler
 * 通过json的工具类jackson
 * public interface TypeHandler<T> {
 * //输入映射 Integer[] -> String -> varchar
 * void setParameter(PreparedStatement var1, int var2, T var3, JdbcType var4) throws SQLException;
 * <p>
 * //输出映射 varchar -> String -> Integer[]
 * T getResult(ResultSet var1, String var2) throws SQLException;
 * <p>
 * T getResult(ResultSet var1, int var2) throws SQLException;
 * <p>
 * T getResult(CallableStatement var1, int var2) throws SQLException;
 * }
 */

@RequestMapping("admin/admin")
@RestController
public class AdminAdminController {

    @Autowired
    AdminService adminServiceImpl;

    /**
     * 管理员的显示
     *
     * @param baseParam {@link BaseParam}
     * @param username  如果有这个参数的话，返回模糊匹配的查询结果
     * @return
     */
    @RequestMapping("list")
    public BaseRespVo<AdminVO> show(BaseParam baseParam, String username) throws InstantiationException, IllegalAccessException {
        AdminVO adminVO = adminServiceImpl.getAdminInfo(baseParam, username);
        return BaseRespVo.ok(adminVO);
    }

    /**
     * 管理员的创建
     *
     * @param adminCreateBO
     * @return
     * @throws AdminAdminException
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    @RequestMapping("create")
    public BaseRespVo<AdminCreateVO> create(@RequestBody @Valid AdminCreateBO adminCreateBO,BindingResult bindingResult) throws AdminAdminException, InstantiationException, IllegalAccessException, ParamsException {
        ValidationUtil.validate(bindingResult);
        AdminCreateVO adminCreateVO = adminServiceImpl.addNewAdmin(adminCreateBO);
        Session session = SecurityUtils.getSubject().getSession();
        session.setAttribute("resultValue",adminCreateVO.getUsername());
        return BaseRespVo.ok(adminCreateVO);
    }


    /**
     * 编辑请求过来的，
     * todo 这个编辑 需要编辑管理员密码，一个是如果是修改自己的，那么修改信息一定要输入密码，吗？万一输错了就相当于是修改密码了，那就意思是如果我只想要修改名字
     * todo 那一定要修改密码？ 二是在权限管理的时候，修改其他人？
     *
     * @param adminUpdateBO
     * @return
     */
    @RequestMapping("update")
    public BaseRespVo<AdminUpdateVO> update(@RequestBody @Valid AdminUpdateBO adminUpdateBO, BindingResult result) throws AdminAdminException, InstantiationException, IllegalAccessException, ParamsException {
        ValidationUtil.validate(result);
        AdminUpdateVO adminUpdateVO = adminServiceImpl.updateAdminInfo(adminUpdateBO);

        Session session = SecurityUtils.getSubject().getSession();
        session.setAttribute("resultValue",adminUpdateBO.getUsername());

        return BaseRespVo.ok(adminUpdateVO);

    }

    @RequestMapping("delete")
    public BaseRespVo delete(@RequestBody AdminDeleteBO adminDeleteBO) throws AdminAdminException {
        adminServiceImpl.deleteAdmin(adminDeleteBO.getId());
        Session session = SecurityUtils.getSubject().getSession();
        session.setAttribute("resultValue",adminDeleteBO.getUsername());
        return BaseRespVo.ok();
    }
}
