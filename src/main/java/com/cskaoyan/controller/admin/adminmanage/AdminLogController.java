package com.cskaoyan.controller.admin.adminmanage;

import com.cskaoyan.model.BaseParam;
import com.cskaoyan.model.BaseRespVo;
import com.cskaoyan.model.admin.adminmanage.log.LogListVO;
import com.cskaoyan.service.admin.adminmanage.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @description:  操作日志
 * @keypoint:
 * @tags:
 * @related:
 */


@RestController
@RequestMapping("admin/log")
public class AdminLogController {

    @Autowired
    LogService logServiceImpl;
    @RequestMapping("list")
    public BaseRespVo<LogListVO> list(BaseParam baseParam, String name) throws InstantiationException, IllegalAccessException {

        LogListVO logListVO =  logServiceImpl.showLogInfo(baseParam,name);
        return BaseRespVo.ok(logListVO);
    }
}
