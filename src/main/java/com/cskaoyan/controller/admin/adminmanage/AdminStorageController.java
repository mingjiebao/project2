package com.cskaoyan.controller.admin.adminmanage;

import com.cskaoyan.exception.adminmanage.AdminStorageException;
import com.cskaoyan.model.BaseRespVo;
import com.cskaoyan.model.FileUploadDTO;
import com.cskaoyan.model.admin.adminmanage.storage.StorageListPOJO;
import com.cskaoyan.model.admin.adminmanage.storage.StorageListVO;
import com.cskaoyan.model.admin.adminmanage.storage.StorageParam;
import com.cskaoyan.service.admin.adminmanage.StorageService;
import com.cskaoyan.utils.BeanCreateUtils;
import com.cskaoyan.utils.FileUploadUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @description: 对象存储
 * @keypoint:
 * @tags:
 * @related:
 */

@RestController
@RequestMapping("admin/storage")
public class AdminStorageController {



    @Autowired
    StorageService storageServiceImpl;

    /**
     * 对象存储页面的显示
     * 前端的请求 类似
     * <p>
     * page=1&limit=20&sort=add_time&order=desc
     * page=1&limit=20&name=tomc&sort=add_time&order=desc
     * page=1&limit=20&key=054ttxyj&name=tomc&sort=add_time&order=desc
     *
     * @param storageParam key和name不一定有
     * @return StorageListVO类 json
     */
    @RequestMapping("list")
    public BaseRespVo<StorageListVO> list(StorageParam storageParam) {
        StorageListVO storageListVO = storageServiceImpl.getStorageInfo(storageParam);

        return BaseRespVo.ok(storageListVO);
    }

    /**
     * 逻辑删除，把deleted字段设为1
     *
     * @param storageListDTO
     * @return 成功，失败
     */
    @RequestMapping("delete")
    public BaseRespVo delete(@RequestBody StorageListPOJO storageListDTO) throws AdminStorageException {
        storageServiceImpl.deleteStorageInfo(storageListDTO.getId());

        return BaseRespVo.ok();
    }


    /**
     * 对象存储的编辑
     * 传过来的参数类似
     * {"id":1,"key":"4fj0ifja","name":"test2","type":"jpg","size":9994,"url":"test","addTime":"2022-01-06 22:26:11","updateTime":"2022-01-06 22:26:11","deleted":false}
     *
     * @param storageListDTO
     * @return
     */
    @RequestMapping("update")
    public BaseRespVo<StorageListPOJO> update(@RequestBody StorageListPOJO storageListDTO) throws AdminStorageException {
        storageServiceImpl.updateStorageInfo(storageListDTO);

        return BaseRespVo.ok(storageListDTO);
    }


    /**
     * 对象存储的上传
     *
     * @param file
     * @return
     */
    @RequestMapping("create")
    public BaseRespVo<StorageListPOJO> create(MultipartFile file) throws AdminStorageException {
        StorageListPOJO storageListPOJO =  storageServiceImpl.addNewStorage(file);
        return BaseRespVo.ok(storageListPOJO);
    }
}
