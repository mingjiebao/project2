package com.cskaoyan.controller.admin.market;




import com.cskaoyan.exception.ParamsException;
import com.cskaoyan.model.BaseParam;
import com.cskaoyan.model.BaseRespParam;
import com.cskaoyan.model.BaseRespVo;
import com.cskaoyan.model.admin.marketbeans.bo.*;
import com.cskaoyan.model.admin.marketbeans.pojo.*;
import com.cskaoyan.model.admin.marketbeans.vo.*;
import com.cskaoyan.model.admin.marketbeans.vo.Regionvo.RegionBeanVo;
import com.cskaoyan.model.admin.marketbeans.vo.brandmsgvo.BrandMsgVo;
import com.cskaoyan.model.admin.marketbeans.bo.OrderShipBo;
import com.cskaoyan.utils.ValidationUtil;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * @package: com.cskaoyan.controller
 * @Description: 商城管理模块
 * @author: 北青
 * @date: 2022/1/6 21:24
 */
@RestController
@RequestMapping("admin")
public class MarketController {

    @Autowired
    com.cskaoyan.service.marketmanage.MarketService marketService;


    /**
     * 售后管理——全部售后信息显示
     *
     * @param marketAftersaleListBO
     * @return
     */

    @GetMapping("aftersale/list")

    public BaseRespVo getAftersaleList(MarketAftersaleListBo marketAftersaleListBO) {
        MarketAftersaleListVo marketAftersaleListInfo = marketService.getMarketAftersaleListInfo(marketAftersaleListBO);
        return BaseRespVo.ok(marketAftersaleListInfo);
    }


    /**
     * 售后信息处理——通过
     *
     * @return
     */
    @PostMapping("aftersale/recept")
    public BaseRespVo recept(@RequestBody MarketAftersale marketAftersale) {
        marketService.recept(marketAftersale);
        return BaseRespVo.ok("已通过");
    }

    /**
     * 售后信息处理——拒绝
     *
     * @return
     */
    @PostMapping("aftersale/reject")
    public BaseRespVo reject(@RequestBody MarketAftersale marketAftersale) {
        marketService.reject(marketAftersale);
        return BaseRespVo.ok("已拒绝");
    }


    /**
     * 售后信息处理——批量通过
     *
     * @param ids : 选中的售后信息的id
     * @return
     */
    @PostMapping("aftersale/batch-recept")
    public BaseRespVo batchRecept(@RequestBody BatchIdsBo ids) {
        marketService.batchRecept(ids.getIds());
        return BaseRespVo.ok();
    }

    /**
     * 售后信息处理——批量通过
     *
     * @param ids : 选中的售后信息的id
     * @return
     */
    @PostMapping("aftersale/batch-reject")
    public BaseRespVo batchReject(@RequestBody BatchIdsBo ids) {
        marketService.batchReject(ids.getIds());
        return BaseRespVo.ok();
    }

    /**
     * 售后信息处理——退款
     *
     * @return
     */
    @PostMapping("aftersale/refund")
    public BaseRespVo refund(@RequestBody MarketAftersale marketAftersale) {
        marketService.refund(marketAftersale);
        return BaseRespVo.ok("已退款");
    }

    /**
     * 通用问题模块——获取通用问题信息
     *
     * @return
     */
    @GetMapping("issue/list")
    public BaseRespVo getIssueListInfo(String question, BaseParam baseParam) {
        MarketIssueListVo issueListInfo = marketService.getIssueListInfo(question, baseParam);
        return BaseRespVo.ok(issueListInfo);
    }

    /**
     * 通用问题模块——新增问题
     *
     * @param marketIssue: 请求参数(问题 + 回答)
     * @return
     */
    @PostMapping("issue/create")
    public BaseRespVo createIssue(@RequestBody MarketIssue marketIssue) {

        String format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(System.currentTimeMillis());//当前时间
        marketIssue.setAddTime(format);
        marketIssue.setUpdateTime(format);
        marketIssue.setDeleted(false);
        marketService.createIssue(marketIssue);

        return BaseRespVo.ok(marketIssue);
    }

    /**
     * 通用问题模块——修改问题信息
     *
     * @return
     */
    @PostMapping("issue/update")
    public BaseRespVo updateIssue(@RequestBody MarketIssue marketIssue) {
        String format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(System.currentTimeMillis());//当前时间
        marketIssue.setUpdateTime(format);
        marketService.updateIssue(marketIssue);
        return BaseRespVo.ok(marketIssue);
    }

    /**
     * 通用问题模块——删除问题信息
     *
     * @param marketIssue
     * @return
     */
    @PostMapping("issue/delete")
    public BaseRespVo deleteIssue(@RequestBody MarketIssue marketIssue) {
        marketIssue.setDeleted(true);
        marketService.deleteIssue(marketIssue);
        return BaseRespVo.ok();
    }

    /**
     * 商城——关键词——关键词信息显示
     *
     * @return
     */
    @GetMapping("keyword/list")
    public BaseRespVo keywordInfo(MarketKeywordListBo marketKeywordListBo) {
        MarketKeywordListVo marketKeywordInfo = marketService.getMarketKeywordInfo(marketKeywordListBo);
        return BaseRespVo.ok(marketKeywordInfo);
    }

    /**
     * 商城——关键词——添加关键词信息
     *
     * @return
     */
    @PostMapping("keyword/create")
    public BaseRespVo createKeyword(@RequestBody MarketKeyword marketKeyword) {
        Date date = new Date(System.currentTimeMillis()); //当前时间
        marketKeyword.setDeleted(false);
        marketKeyword.setAddTime(date);
        marketKeyword.setUpdateTime(date);
        marketKeyword.setSortOrder(100);

        //执行添加操作
        marketService.createKeyword(marketKeyword);

        return BaseRespVo.ok(marketKeyword);
    }

    /**
     * 商城——关键词——修改关键词信息
     *
     * @param marketKeyword
     * @return
     */
    @PostMapping("keyword/update")
    public BaseRespVo updateKeyword(@RequestBody MarketKeyword marketKeyword) {
        marketKeyword.setUpdateTime(new Date(System.currentTimeMillis()));//修改一下更新时间
        marketService.updateKeyword(marketKeyword);
        return BaseRespVo.ok(marketKeyword);
    }

    /**
     * 商城——关键词——删除关键词信息
     *
     * @param marketKeyword
     * @return
     */
    @PostMapping("keyword/delete")
    public BaseRespVo deleteKeyword(@RequestBody MarketKeyword marketKeyword) {
        marketKeyword.setDeleted(true);
        marketService.updateKeyword(marketKeyword);
        return BaseRespVo.ok(marketKeyword);
    }


    /**
     * 商城——订单管理——订单信息显示
     *
     * @return
     */
    @GetMapping("order/list")
    public BaseRespVo getOrderListInfo(MarketOrderListBo marketOrderListBo, HttpSession session) {

        MarketOrderInfoVo marketOrderInfoVo = null;
        if (marketOrderListBo.getOrderId() != null) {
            return this.detail(marketOrderListBo.getOrderId());
        } else {
            marketOrderInfoVo = marketService.getOrderListInfo(marketOrderListBo);

            //分页信息存入session
            session.setAttribute("page", BaseRespParam.getPage(marketOrderInfoVo.getTotal(), marketOrderInfoVo.getPages(), marketOrderInfoVo.getLimit(), marketOrderInfoVo.getPage()));
        }
        return BaseRespVo.ok(marketOrderInfoVo);
    }

    /**
     * 商城——订单管理——快递公司信息显示
     *
     * @return
     */
    @GetMapping("order/channel")
    public BaseRespVo getChannel() {
        List<MarketChannelVo> marketChannelVoList = marketService.getChannel();
        return BaseRespVo.ok(marketChannelVoList);
    }

    /**
     * 根据订单id精确查询订单编号
     *
     * @return
     */
    @GetMapping("order/detail")
    public BaseRespVo detail(Integer id) {

//        MarketOrderListBo marketOrderListBo = new MarketOrderListBo();
//        //获得分页信息
//        BaseRespParam page = (BaseRespParam) session.getAttribute("page");
//        marketOrderListBo.setOrderId(orderId);
//        MarketOrderInfoVo orderListInfo = marketService.getOrderListInfo(marketOrderListBo);
//        if (page != null){
//            orderListInfo.setTotal(page.getTotal());
//            orderListInfo.setLimit(page.getLimit());
//            orderListInfo.setPages(page.getPages());
//            orderListInfo.setPage(page.getPage());
//            return BaseRespVo.ok(orderListInfo);
//        }
//        MarketOrderDetailVo marketOrderDetailVo = new MarketOrderDetailVo();
//        marketOrderDetailVo.setOrder(orderListInfo.getList().get(0));
//        return BaseRespVo.ok(marketOrderDetailVo);
        MarketOrderDetailVo marketOrderDetailVo = marketService.getOrderDetail(id);
        return BaseRespVo.ok(marketOrderDetailVo);
    }

    /**
     * @createTime: 2022/1/7 20:53
     * @author: Dragon
     * @description: 商城管理模块-行政区域查询全部信息controller
     */
    @RequestMapping("region/list")
    public BaseRespVo getAllRegion() {
        RegionBeanVo regionList = marketService.getRegionList();
        return BaseRespVo.ok(regionList);
    }

    /**
     * @createTime: 2022/1/7 20:53
     * @author: Dragon
     * @description: 商城管理模块-品牌制造商查询全部信息controller
     */
    @RequestMapping("brand/list")
    public BaseRespVo getAllBrand(BaseParam baseParam, String id, String name) {
        if (id != null) {
            try {
                Integer.parseInt(id);
            } catch (NumberFormatException e){
                return BaseRespVo.fail("请输入数字");
            }
        }
        BrandMsgVo brandList = marketService.getBrandList(baseParam, id, name);
        return BaseRespVo.ok(brandList);
    }

    /**
     * @createTime: 2022/1/8 19:32
     * @author: Dragon
     * @description: 商城管理模块-品牌制造商-添加品牌controller
     */
    @RequestMapping("brand/create")
    public BaseRespVo addNewBrand(@RequestBody @Validated AddBrandBo addBrandBo, BindingResult bindingResult) throws ParamsException {
        ValidationUtil.validate(bindingResult);
        BrandAddVo brandAddVo = marketService.addNewBrand(addBrandBo);
        return BaseRespVo.ok(brandAddVo);
    }


    /**
     * @createTime: 2022/1/8 20:26
     * @author: Dragon
     * @description: 商城管理模块-品牌制造商-修改信息controller
     */
    @RequestMapping("brand/update")
    public BaseRespVo renewBrand(@RequestBody @Validated UpdateBrand updateBrand, BindingResult bindingResult) throws ParamsException, ParamsException {
        ValidationUtil.validate(bindingResult);
        UpdateBrand updateBrandResp = marketService.renewBrand(updateBrand);
        return BaseRespVo.ok(updateBrandResp);
    }

    /**
     * @createTime: 2022/1/8 20:53
     * @author: Dragon
     * @description: 商城管理模块-品牌制造商-删除信息controller
     */
    @RequestMapping("brand/delete")
    public BaseRespVo removeBrand(@RequestBody Map map) {
        Integer id = (Integer) map.get("id");
        int i = marketService.removeBrand(id);
        if (i == 1) {

            return BaseRespVo.ok();
        }
        return BaseRespVo.fail("删除失败，请确认品牌是否存在");
    }

    /**
     * @createTime: 2022/1/8 21:19
     * @author: Dragon
     * @description: 商城管理模块-商品类目-查询全部信息controller
     */
    @RequestMapping("category/list")
    public BaseRespVo getAllCategory() {
        CategoryListVo allCategoryMsg = marketService.getAllCategoryMsg();
        return BaseRespVo.ok(allCategoryMsg);
    }

    /**
     * @createTime: 2022/1/8 21:46
     * @author: Dragon
     * @description: 商城管理模块-商品类目-查询level1信息controller
     */
    @RequestMapping("category/l1")
    public BaseRespVo getLevel1Category() {
        GetLevelFirstCategory level1CategoryMsg = marketService.getLevel1CategoryMsg();
        return BaseRespVo.ok(level1CategoryMsg);
    }

    /**
     * @createTime: 2022/1/8 22:14
     * @author: Dragon
     * @description: 商城管理模块-商品类目-增加类目controller
     */
    @RequestMapping("category/create")
    public BaseRespVo addCategory(@RequestBody @Validated AddCategory addCategory, BindingResult bindingResult) throws ParamsException {
        ValidationUtil.validate(bindingResult);
        AddCategory addCategoryResp = marketService.addCategory(addCategory);
        return BaseRespVo.ok(addCategoryResp);
    }

    /**
     * @createTime: 2022/1/8 22:57
     * @author: Dragon
     * @description: 商城管理模块-商品类目-删除类目信息controller
     */
    @RequestMapping("category/delete")
    public BaseRespVo deleteCategory(@RequestBody Map map) {
        Integer id = (Integer) map.get("id");
        int i = marketService.deleteCategory(id);
        if (i == 1) {
            return BaseRespVo.ok();
        }
        return BaseRespVo.fail("删除失败，请检查类目是否存在");
    }

    /**
     * @createTime: 2022/1/9 11:07
     * @author: Dragon
     * @description: 商城管理模块-商品类目-修改类目信息controller
     */
    @RequestMapping("category/update")
    public BaseRespVo updateCategory(@RequestBody @Validated UpdateCategoryBo updateCategoryBo, BindingResult bindingResult) {
        marketService.updateCategory(updateCategoryBo);
        return BaseRespVo.ok();
    }

    /**
     * @createTime: 2022/1/9 20:05
     * @author: Dragon
     * @description: 商城管理模块-商品类目-发货controller
     */
    @RequestMapping("order/ship")
    public BaseRespVo shipOrder(@RequestBody @Validated OrderShipBo orderShipBo, BindingResult bindingResult) {
        marketService.shipOrder(orderShipBo);
        return BaseRespVo.ok();
    }

    /**
     * 订单管理——删除订单
     *
     * @param map 封装了订单ID的map
     * @return
     */
    @PostMapping("order/delete")
    public BaseRespVo deleteOrder(@RequestBody Map<String, Integer> map) {
        Integer orderId = map.get("orderId");
        marketService.deleteOrder(orderId);
        return BaseRespVo.ok();
    }

}
