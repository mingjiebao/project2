package com.cskaoyan.controller.admin.polularize;

import com.cskaoyan.exception.ParamsException;
import com.cskaoyan.model.admin.coupon.bo.CouponCreateInfoBo;
import com.cskaoyan.model.admin.coupon.bo.CouponListBo;
import com.cskaoyan.model.BaseRespVo;
import com.cskaoyan.model.admin.coupon.bo.CouponUpdateBo;
import com.cskaoyan.model.admin.coupon.bo.CouponUserBo;
import com.cskaoyan.model.admin.coupon.vo.CouponAddDataVo;
import com.cskaoyan.model.admin.coupon.vo.CouponReadVo;
import com.cskaoyan.model.admin.coupon.vo.CouponResponseVo;
import com.cskaoyan.model.realm.AdminLoginAdminData;
import com.cskaoyan.model.realm.WxLoginUserData;
import com.cskaoyan.service.admin.popularize.CouPonService;
import com.cskaoyan.utils.ValidationUtil;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;


// todo: 优惠券相关的内容需要权限验证，【查看不需要，但是增，删，改需要】

@RestController
@RequestMapping("/admin/coupon")
public class CouponController {

    @Autowired
    CouPonService service;

    /**
     * 查询优惠券的信息
     */
    @RequestMapping("list")
    public BaseRespVo getCouponList(CouponListBo bo) {
        CouponResponseVo couponList = service.getCouponList(bo);
        return BaseRespVo.ok(couponList);
    }

    /**
     * 创建新的优惠券信息：
     * 1. 注意验证条件
     * 2. annotation有自己写的验证注解
     *
     * @throws ParamsException :自定义验证
     */
    @RequestMapping("create")
    public BaseRespVo createNewCoupon(@RequestBody @Valid CouponCreateInfoBo bo, BindingResult result) throws ParamsException {
        ValidationUtil.validate(result);
        CouponAddDataVo vo = service.addNewCoupon(bo);
        return BaseRespVo.ok(vo);
    }


    /**
     * 更新优惠券信息
     *
     * @return
     */
    @RequestMapping("update")
    public BaseRespVo updateCouponInfo(@RequestBody @Valid CouponUpdateBo bo, BindingResult result) throws ParamsException {

        ValidationUtil.validate(result);

        CouponAddDataVo vo = service.updateCouponInfo(bo);

        if (vo == null) {
            return BaseRespVo.fail("操作失败，更新对象不存在，请刷新页面重试");
        }
        return BaseRespVo.ok(vo);
    }

    @RequestMapping("delete")
    public BaseRespVo deleteCoupon(@RequestBody CouponUpdateBo bo) throws ParamsException {

        AdminLoginAdminData principal = (AdminLoginAdminData) SecurityUtils.getSubject().getPrincipal();

        bo.setDeleted(true);
        CouponAddDataVo vo = service.updateCouponInfo(bo);

        if (vo == null) {
            return BaseRespVo.fail("操作失败，删除对象不存在，请刷新页面重试");
        }
        return BaseRespVo.ok();
    }

    @RequestMapping("read")
    public BaseRespVo couponRead(Integer id) {
        CouponReadVo result = service.getCouponRead(id);
        return BaseRespVo.ok(result);
    }

    @RequestMapping("listuser")
    public BaseRespVo getCouponUsedUser(CouponUserBo bo) {

        CouponResponseVo resp = service.getUserInfo(bo);
        return BaseRespVo.ok(resp);
    }
}
