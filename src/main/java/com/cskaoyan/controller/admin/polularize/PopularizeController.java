package com.cskaoyan.controller.admin.polularize;

import com.cskaoyan.model.BaseRespVo;
import com.cskaoyan.model.admin.ad.bo.AdCreateBO;
import com.cskaoyan.model.admin.ad.bo.AdListBO;
import com.cskaoyan.model.admin.ad.bo.AdUpdateBo;
import com.cskaoyan.model.admin.ad.vo.AdListDataVo;
import com.cskaoyan.model.admin.ad.vo.AdListVo;
import com.cskaoyan.service.admin.popularize.PopularizeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * 推广管理控制接口
 */

@RestController
@RequestMapping("/admin/ad")
public class PopularizeController {


    @Autowired
    PopularizeService service;

    /**
     * 功能：根据广告标题等信息搜索所有的广告信息
     *
     * @param adList:传递过来的参数，通过get params方式传递，而非json
     * @return :返回查询的结果
     */
    @RequestMapping("list")
    public BaseRespVo getAdListInfo(AdListBO adList) {
        AdListVo result = service.getAdList(adList);
        return BaseRespVo.ok(result);
    }

    /**
     * 功能：添加新的广告条目信息
     *
     * @param bo:传递过来的参数需要处理一下
     * @return :返回的是插入数据库的一条条目的详细信息
     */
    @RequestMapping("create")
    public BaseRespVo addAdInfo(@RequestBody AdCreateBO bo) {
        AdListDataVo vo = service.addNewAdInfo(bo);
        return BaseRespVo.ok(vo);
    }

    /**
     * 更新广告信息
     */
    @RequestMapping("update")
    public BaseRespVo updateAdInfo(@RequestBody @Valid AdUpdateBo bo) {
        AdListDataVo vo = service.updateAdInfo(bo);

        return BaseRespVo.ok(vo);
    }

    /***
     *  删除算是一种更新操作[伪删除]
     * @param bo
     * @return
     */
    @RequestMapping("delete")
    public BaseRespVo deleteAdInfo(@RequestBody AdUpdateBo bo) {
        bo.setDeleted(true);
        AdListDataVo vo = service.updateAdInfo(bo);
        return BaseRespVo.ok(vo);
    }
}
