package com.cskaoyan.controller.admin.configmanage;

import com.cskaoyan.exception.ParamsException;
import com.cskaoyan.model.BaseRespVo;
import com.cskaoyan.model.admin.configmanage.bo.ConfigExpressBO;
import com.cskaoyan.model.admin.configmanage.bo.ConfigMallBO;
import com.cskaoyan.model.admin.configmanage.bo.ConfigOrderBO;
import com.cskaoyan.model.admin.configmanage.bo.ConfigWxBO;
import com.cskaoyan.model.admin.configmanage.vo.ConfigExpressVO;
import com.cskaoyan.model.admin.configmanage.vo.ConfigMallVO;
import com.cskaoyan.model.admin.configmanage.vo.ConfigOrderVO;
import com.cskaoyan.model.admin.configmanage.vo.ConfigWxVO;
import com.cskaoyan.service.admin.configmanage.ConfigService;
import com.cskaoyan.utils.ValidationUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("admin/config")
public class ConfigController {

    @Autowired
    ConfigService configService;

    @GetMapping("mall")
    //商场配置 页面数据显示
    public BaseRespVo mallInfo() {
        ConfigMallVO configMallVO = configService.configMallInfo();
        return BaseRespVo.ok(configMallVO);
    }

    @PostMapping("mall")
    //商场配置 确认插入或更新配置项
    public BaseRespVo mallConfirm(@RequestBody @Validated ConfigMallBO configMallBO, BindingResult bindingResult) throws ParamsException {
        //参数校验
        ValidationUtil.validate(bindingResult);
        configService.confirmConfigMall(configMallBO);
        return BaseRespVo.ok();
    }

    @GetMapping("express")
    //运费配置 页面数据显示
    public BaseRespVo expressInfo() {
        ConfigExpressVO configExpressVO = configService.configExpressInfo();
        return BaseRespVo.ok(configExpressVO);
    }

    @PostMapping("express")
    //运费配置 确认插入或更新配置项
    public BaseRespVo expressConfirm(@RequestBody @Validated ConfigExpressBO configExpressBO, BindingResult bindingResult) throws ParamsException {
        //参数校验
        ValidationUtil.validate(bindingResult);
        configService.confirmConfigExpress(configExpressBO);
        return BaseRespVo.ok();
    }

    @GetMapping("order")
    //订单配置 页面数据显示
    public BaseRespVo orderInfo() {
        ConfigOrderVO configOrderVO = configService.configOrderInfo();
        return BaseRespVo.ok(configOrderVO);
    }

    @PostMapping("order")
    //订单配置 确认插入或更新配置项
    public BaseRespVo orderConfirm(@RequestBody @Validated ConfigOrderBO configOrderBO, BindingResult bindingResult) throws ParamsException {
        //参数校验
        ValidationUtil.validate(bindingResult);
        configService.confirmConfigOrder(configOrderBO);
        return BaseRespVo.ok();
    }

    @GetMapping("wx")
    //小程序配置 页面数据显示
    public BaseRespVo wxInfo() {
        ConfigWxVO configWxVO = configService.configWxInfo();
        return BaseRespVo.ok(configWxVO);
    }

    @PostMapping("wx")
    //小程序配置 确认插入或更新配置项
    public BaseRespVo wxConfirm(@RequestBody @Validated ConfigWxBO configWxBO, BindingResult bindingResult) throws ParamsException {
        //参数校验
        ValidationUtil.validate(bindingResult);
        configService.confirmConfigWx(configWxBO);
        return BaseRespVo.ok();
    }
}
