package com.cskaoyan.controller.admin.statisticsreport;

import com.cskaoyan.model.BaseRespVo;
import com.cskaoyan.model.admin.statisticsreport.vo.GoodsStatisticsVO;
import com.cskaoyan.model.admin.statisticsreport.vo.OrderStatisticsVO;
import com.cskaoyan.model.admin.statisticsreport.vo.UserStatisticsVO;
import com.cskaoyan.service.admin.statisticsreport.StatisticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("admin/stat")
public class StatisticsController {

    @Autowired
    StatisticsService statisticsService;

    @GetMapping("user")
    //用户统计
    public BaseRespVo userStatistics() {
        UserStatisticsVO userStatisticsVo = statisticsService.userStatistics();
        return BaseRespVo.ok(userStatisticsVo);
    }

    @GetMapping("order")
    //订单统计
    public BaseRespVo orderStatistics() {
        OrderStatisticsVO orderStatisticsVo = statisticsService.orderStatistics();
        return BaseRespVo.ok(orderStatisticsVo);
    }

    @GetMapping("goods")
    //商品统计
    public BaseRespVo goodsStatistics() {
        GoodsStatisticsVO goodsStatisticsVo = statisticsService.goodsStatistics();
        return BaseRespVo.ok(goodsStatisticsVo);
    }
}
