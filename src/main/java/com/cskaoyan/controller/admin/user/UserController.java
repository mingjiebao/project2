package com.cskaoyan.controller.admin.user;

import com.cskaoyan.exception.ParamsException;
import com.cskaoyan.model.BaseRespVo;

import com.cskaoyan.model.admin.user.param.BaseParam;
import com.cskaoyan.model.admin.user.userbeans.DashHoardVo;
import com.cskaoyan.model.admin.user.userbeans.User;
import com.cskaoyan.model.admin.user.userbeans.UserData;
import com.cskaoyan.model.admin.user.userbeans.UserListVo;
import com.cskaoyan.service.admin.usermanage.UserService;
import com.cskaoyan.utils.ValidationUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @description:
 * @author: LI
 * @date: 2022/1/6 21:48
 */
@RequestMapping("admin")
@RestController
public class UserController {
    @Autowired()
    UserService userService;

    /*
     * @createTime  2022/1/7 15:10
     * @description:首页
     * @param
     * @return 返回dashHaordVO类型*/
    @RequestMapping("dashboard")
    public BaseRespVo adminDashBoard() {
        DashHoardVo dashHoardVO = userService.dashboard();
        return BaseRespVo.ok(dashHoardVO);
    }


    /*
     * @createTime  2022/1/7 15:23
     * @description:会员管理(list、按用户名、电话查找)
     * @param
     * @return */
    @RequestMapping("user/list")
    public BaseRespVo adminUserList(String username, String mobile, BaseParam param) {
        //用户名可以不进行判断
        if (mobile != null) {
            try {
                Integer.parseInt(mobile);
            } catch (NumberFormatException e) {
                return BaseRespVo.fail("参数值不对");
            }
        }

        UserData userListVO = userService.adminUserList(username, mobile, param);
        return BaseRespVo.ok(userListVO);
    }


    /*
     * @createTime  2022/1/7 17:33
     * @description:会员管理（按id查找）
     * @param
     * @return */
    @RequestMapping("user/detail")
    public BaseRespVo userDetail(String id) {
        if (id != null) {
            try {
                Integer.parseInt(id);
            } catch (NumberFormatException e) {
                return BaseRespVo.fail("参数值不对");
            }
        }
        UserListVo userListVO = userService.userDetail(id);
        return BaseRespVo.ok(userListVO);
    }


    /*
     * @createTime  2022/1/7 17:35
     * @description:修改用户信息
     * @param
     * @return */
    @PostMapping("user/update")
    public BaseRespVo userUpdate(@RequestBody @Validated User user, BindingResult bindingResult) throws ParamsException {
        ValidationUtil.validate(bindingResult);
        Integer number = userService.userUpdate(user);
        return BaseRespVo.ok(number);
    }


    /*
     * @createTime  2022/1/7 19:54
     * @description:收货地址（list、按id、收货人姓名查找）
     * @param
     * @return */
    @RequestMapping("address/list")
    public BaseRespVo addressList(String name, String userId, BaseParam param) {
        if (userId != null) {
            try {
                Integer.parseInt(userId);
            } catch (NumberFormatException e) {
                return BaseRespVo.fail("参数值不对");
            }
        }
        UserData userData = userService.addressList(name, userId, param);
        return BaseRespVo.ok(userData);
    }


    /*
     * @createTime  2022/1/7 20:51
     * @description:会员收藏
     * @param
     * @return */
    @RequestMapping("collect/list")
    public BaseRespVo collectList(String userId, String valueId, BaseParam param) {
        if (userId != null) {
            try {
                Integer.parseInt(userId);
            } catch (NumberFormatException e) {
                return BaseRespVo.fail("参数值不对");
            }
        }
        if (valueId != null) {
            try {
                Integer.parseInt(valueId);
            } catch (NumberFormatException e) {
                return BaseRespVo.fail("参数值不对");
            }
        }
        UserData userData = userService.collectList(userId, valueId, param);
        return BaseRespVo.ok(userData);
    }


    /*
     * @createTime  2022/1/7 22:22
     * @description:会员足迹
     * @param
     * @return */
    @RequestMapping("footprint/list")
    public BaseRespVo footPringtList(String userId, String goodsId, BaseParam param) {
        if (userId != null) {
            try {
                Integer.parseInt(userId);
            } catch (NumberFormatException e) {
                return BaseRespVo.fail("参数值不对");
            }
        }
        if (goodsId != null) {
            try {
                Integer.parseInt(goodsId);
            } catch (NumberFormatException e) {
                return BaseRespVo.fail("参数值不对");
            }
        }
        UserData userData = userService.footPrintList(userId, goodsId, param);
        return BaseRespVo.ok(userData);
    }

    /*
     * @createTime  2022/1/7 22:43
     * @description:搜索历史
     * @param
     * @return */
    @RequestMapping("history/list")
    public BaseRespVo adminHistory(String userId, String keyword, BaseParam param) {
        if (userId != null) {
            try {
                Integer.parseInt(userId);
            } catch (NumberFormatException e) {
                return BaseRespVo.fail("参数值不对");
            }
        }
        UserData userData = userService.adminHistory(userId, keyword, param);
        return BaseRespVo.ok(userData);
    }


    /*
     * @createTime  2022/1/8 10:07
     * @description:意见反馈
     * @param
     * @return */
    @RequestMapping("feedback/list")
    public BaseRespVo feedbackList(String username, String id, BaseParam param) {
        if (id != null) {
            try {
                Integer.parseInt(id);
            } catch (NumberFormatException e) {
                return BaseRespVo.fail("参数值不对");
            }
        }
        UserData userData = userService.feedbackList(username, id, param);
        return BaseRespVo.ok(userData);
    }

}


