package com.cskaoyan.controller.admin.goods;

import com.cskaoyan.model.BaseData;
import com.cskaoyan.model.BaseParam;
import com.cskaoyan.model.BaseRespVo;
import com.cskaoyan.model.admin.goodsbean.Comment;
import com.cskaoyan.service.admin.goods.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author AhaNg
 * @Date 2022/1/8 23:29
 * @description:
 * @return:
 */
@RequestMapping("admin/comment")
@RestController
public class CommentController {

    @Autowired
    CommentService commentService;

/**
 * @Author: AhaNg
 * @description: 商品管理->商品评论页面的显示
 * @return:
 */
    @RequestMapping("list")
    public BaseRespVo list(Integer userId, Integer valueId, BaseParam param){
        BaseData baseData=commentService.list(userId,valueId,param);
        return BaseRespVo.ok(baseData);
    }

    /**
     * @Author: AhaNg
     * @description: 商品管理->商品评论的删除
     * @return:
     */
   @RequestMapping("delete")
    public BaseRespVo delete(@RequestBody Comment comment){
       try {
           commentService.delete(comment);
       } catch (Exception e) {
           e.printStackTrace();
           return BaseRespVo.fail("删除失败");
       }
       return BaseRespVo.ok();
   }

}
