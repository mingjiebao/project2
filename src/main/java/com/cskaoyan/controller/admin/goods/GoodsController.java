package com.cskaoyan.controller.admin.goods;

import com.cskaoyan.exception.ParamsException;
import com.cskaoyan.model.BaseRespVo;
import com.cskaoyan.model.admin.goodsbean.Goods;
import com.cskaoyan.model.admin.goodsbean.bo.GoodsCreateBo;
import com.cskaoyan.model.admin.goodsbean.bo.GoodsUpdateBo;
import com.cskaoyan.model.admin.goodsbean.bo.list.GoodsListBO;
import com.cskaoyan.model.admin.goodsbean.vo.CatAndBrandVo;
import com.cskaoyan.model.admin.goodsbean.vo.GoodsDataVO;
import com.cskaoyan.model.admin.goodsbean.vo.GoodsDetailVo;
import com.cskaoyan.service.admin.goods.GoodsService;
import com.cskaoyan.utils.ValidationUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * @Author AhaNg
 * @Date 2022/1/7 20:01
 * @description: 商品管理模块Controller
 * @return:
 */
@RequestMapping("admin/goods")
@RestController
public class GoodsController {
    @Autowired
    GoodsService goodsService;

    /**
     * @Author: AhaNg
     * @description: 分页查询商品信息
     * @return:
     */
    @RequestMapping("list")
    public BaseRespVo getGoodsList(GoodsListBO goodsListBO) {
        if (goodsListBO.getLimit() == 0) {
            return BaseRespVo.ok();
        } else {
            GoodsDataVO goodsDataVO = goodsService.allGoodsList(goodsListBO);
            return BaseRespVo.ok(goodsDataVO);
        }
    }

    /**
     * @Author: AhaNg
     * @description: 商品品牌及类目
     * @return:
     */
    @RequestMapping("catAndBrand")
    public BaseRespVo catAndBrand() {
        CatAndBrandVo catAndBrandVo = goodsService.CatAndBrand();
        return BaseRespVo.ok(catAndBrandVo);
    }

    /**
     * @Author: AhaNg
     * @description: 商品的创建
     * @return:
     */
    @RequestMapping("create")
    public BaseRespVo create(@RequestBody @Valid GoodsCreateBo goodsCreateBo, BindingResult result) throws ParamsException {
        ValidationUtil.validate(result);
        goodsService.create(goodsCreateBo);
        return BaseRespVo.ok(null);
    }

    /**
     * @Author: AhaNg
     * @description: 显示商品详情
     * @return:
     */
    @RequestMapping("detail")
    public BaseRespVo detail(Integer id) {
        GoodsDetailVo goodsDetailVo = goodsService.detail(id);
        return BaseRespVo.ok(goodsDetailVo);
    }

    /**
     * @Author: AhaNg
     * @description: 商品信息的更新修改
     * @return:
     */

    @RequestMapping("update")
    public BaseRespVo update(@RequestBody @Valid GoodsUpdateBo goodsUpdateBo, BindingResult result) throws ParamsException {
        ValidationUtil.validate(result);

        goodsService.update(goodsUpdateBo);
        return BaseRespVo.ok(null);
    }

    /**
     * @Author: AhaNg
     * @description: 商品的删除
     * @return:
     */
    @RequestMapping("delete")
    public BaseRespVo delete(@RequestBody Goods goods) {
        goodsService.delete(goods);
        return BaseRespVo.ok();
    }
}
