package com.cskaoyan.controller.admin.auth;


import com.cskaoyan.dao.adminmanage.AdminMapper;
import com.cskaoyan.exception.adminmanage.AdminAdminException;
import com.cskaoyan.model.admin.adminmanage.admin.AdminInfoBean;
import com.cskaoyan.model.BaseRespVo;
import com.cskaoyan.model.InfoData;
import com.cskaoyan.model.admin.adminmanage.admin.AdminLoginUserDataVO;
import com.cskaoyan.model.realm.AdminLoginAdminData;
import com.cskaoyan.model.realm.UsernamePasswordTypeToken;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

//shiro整合之后，在做具体的开发
//响应结果都是JSON，Controller组件上我们都用@RestController注解
@RestController
@RequestMapping("admin/auth")
public class AuthController {
    @Autowired
    AdminMapper adminMapper;
//    @Autowired
//    HttpSession httpSession;
//
//    @Autowired
//    HttpServletRequest request;

    /**
     * 如果参数比较少，类型比较简单的话，使用map来接收也可以
     */
    @PostMapping("login")
    public BaseRespVo<AdminLoginUserDataVO> login(@RequestBody Map map, HttpServletRequest req) throws AdminAdminException {
        String username = (String) map.get("username");
        String password = (String) map.get("password");

        //subject的login
        Subject subject = SecurityUtils.getSubject();
        String host = req.getRemoteAddr();
        UsernamePasswordTypeToken token = new UsernamePasswordTypeToken(username, password, host, "admin");

        try {
            subject.login(token);
        } catch (AuthenticationException e) {
            throw new AdminAdminException("username or password error");
        }

        AdminLoginAdminData principal = (AdminLoginAdminData) subject.getPrincipal();
        Session session = subject.getSession();
        String id = (String) session.getId();

        AdminLoginUserDataVO loginUserData = new AdminLoginUserDataVO();
        AdminInfoBean adminInfo = new AdminInfoBean();
        adminInfo.setAvatar(principal.getAvatar());
        adminInfo.setNickName(principal.getUsername());
        loginUserData.setAdminInfo(adminInfo);
        loginUserData.setToken(id);


        return BaseRespVo.ok(loginUserData);
    }

    @RequestMapping("info")
    public BaseRespVo info(String token, HttpServletRequest req) throws JsonProcessingException {

        //开发完shiro之后，再整合
        InfoData infoData = new InfoData();

        Subject subject = SecurityUtils.getSubject();

        AdminLoginAdminData principal = (AdminLoginAdminData) subject.getPrincipal();
        if (principal == null) {
            return BaseRespVo.fail("未登录");
        }
        Integer id = principal.getId();
        String username = principal.getUsername();
        String avatar = principal.getAvatar();
        infoData.setName(username);

        //根据primaryPrincipal做查询
        infoData.setAvatar(avatar);

        infoData.setRoles(principal.getRoleNames());
        infoData.setPerms(principal.getPerms());


        return BaseRespVo.ok(infoData);
    }



    @PostMapping("logout")
    public BaseRespVo logout() {
        Subject subject = SecurityUtils.getSubject();
        subject.logout();
        return BaseRespVo.ok();
    }

    @RequestMapping("401")
    public BaseRespVo unAuc() {
        return BaseRespVo.fail("please login first");
    }
}
