package com.cskaoyan.controller.wx.goods;

import com.cskaoyan.model.BaseRespVo;
import com.cskaoyan.model.realm.WxLoginUserData;
import com.cskaoyan.model.wx.wxgoodsbean.bo.WxGoodsListBo;
import com.cskaoyan.model.wx.wxgoodsbean.vo.WXGoodsCategoryVo;
import com.cskaoyan.model.wx.wxgoodsbean.vo.WXGoodsListVo;
import com.cskaoyan.model.wx.wxgoodsbean.vo.WXGoodsRelatedVo;
import com.cskaoyan.model.wx.wxuserdetailbean.WXUserDetailVo;
import com.cskaoyan.service.wx.coupon.WXCouponService;
import com.cskaoyan.service.wx.footprint.FootPrintService;
import com.cskaoyan.service.wx.footprint.FootPrintServiceImpl;
import com.cskaoyan.service.wx.goodsmanage.WXGoodsService;
import com.cskaoyan.service.wx.usermanage.WXUserService;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author : [Dragon]
 * @createTime : [2022/1/10 14:57]
 * @description :
 */

@RestController
@RequestMapping("wx/goods")
public class WXGoodsController {

    @Autowired
    WXGoodsService wxGoodsService;

    @Autowired
    WXUserService userService;

    @Autowired
    FootPrintService service;

    @GetMapping("category")
    public BaseRespVo getCategory(Integer id) {
        WXGoodsCategoryVo categoryMsg = wxGoodsService.getCategoryMsg(id);
        return BaseRespVo.ok(categoryMsg);
    }

    /***
     *      WxGoodsListBo中的brandId和Keyword【搜索场景】使用场景冲突
     *
     */
    @GetMapping("list")
    public BaseRespVo getGoodsList(WxGoodsListBo bo) {
        WXGoodsListVo goodsList = null;

        if (bo.getIsNew() != null || bo.getIsHot() != null) {
            goodsList = wxGoodsService.getGoodsListHot(bo);
            return BaseRespVo.ok(goodsList);
        }

        if (bo.getKeyword() != null){
            goodsList = wxGoodsService.getGoodsListKeyword(bo);
            wxGoodsService.addSearchHistory(bo);
            return BaseRespVo.ok(goodsList);
        }

        if (bo.getCategoryId() != null || bo.getBrandId() != null) {
            goodsList = wxGoodsService.getGoodsList(bo.getCategoryId(), bo.getBrandId(), bo.getPage(), bo.getLimit());
        }

        return BaseRespVo.ok(goodsList);
    }

    @GetMapping("related")
    public BaseRespVo getGoodsRelated(Integer id) {
        WXGoodsRelatedVo goodsRelated = wxGoodsService.getGoodsRelated(id);

        Object principal = SecurityUtils.getSubject().getPrincipal();
        if (principal != null) {
            service.addFootPrint(((WxLoginUserData) principal).getId(), id);
        }

        return BaseRespVo.ok(goodsRelated);
    }

    @GetMapping("count")
    public BaseRespVo getGoodsCount() {
        Integer number = wxGoodsService.getGoodsCount();
        return BaseRespVo.ok(number);
    }

    @GetMapping("detail")
    public BaseRespVo getUserDetail(Integer id) {
        WXUserDetailVo wxUserDetailVo = userService.getUserDetail(id);
        return BaseRespVo.ok(wxUserDetailVo);
    }
}
