package com.cskaoyan.controller.wx.footprint;

import com.cskaoyan.model.BaseRespVo;
import com.cskaoyan.model.wx.footprint.vo.FootPrintListVo;
import com.cskaoyan.service.wx.footprint.FootPrintService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RequestMapping("wx/footprint")
@RestController
public class FootPrintController {

    @Autowired
    FootPrintService service;

    @RequestMapping("list")
    public BaseRespVo getFootPrintList(Integer page, Integer limit) {

        FootPrintListVo result = service.getFootPrintList(page, limit);
        return BaseRespVo.ok(result);
    }

    /**
     *  长按三秒删除
     */
    @RequestMapping(value = "delete",method = RequestMethod.POST)
    public BaseRespVo deleteFootPrintData(@RequestBody Map map){

        Integer footId= (Integer) map.get("id");
        service.deleteFootPrint(footId);
        return BaseRespVo.ok("删除成功");
    }
}
