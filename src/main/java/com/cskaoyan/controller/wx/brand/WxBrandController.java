package com.cskaoyan.controller.wx.brand;

import com.cskaoyan.model.BaseRespVo;
import com.cskaoyan.model.admin.goodsbean.Brand;
import com.cskaoyan.model.wx.brand.WxBaseParam;
import com.cskaoyan.model.wx.brand.WxBrandListVO;
import com.cskaoyan.service.wx.brand.BrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author AhaNg
 * @Date 2022/1/11 10:31
 * @description:
 * @return:
 */
@RestController
@RequestMapping("wx/brand")
public class WxBrandController {

    @Autowired
    BrandService brandService;

    /**
     * @Author: AhaNg
     * @description: 所有的brand的数据信息
     * @return:
     */
    @RequestMapping(value = "list", method = RequestMethod.GET)
//    @GetMapping("list")
    public BaseRespVo list(WxBaseParam baseParam) {
        WxBrandListVO wxBrandListVO = brandService.wxList(baseParam);
        return BaseRespVo.ok(wxBrandListVO);
    }

    /**
     * @Author: AhaNg
     * @description: 指定brand里的详细信息
     * @return:
     */

    @RequestMapping(value = "detail", method = RequestMethod.GET)
    public BaseRespVo detail(String id) {
        Brand brand = brandService.queryBrandById(id);
        return BaseRespVo.ok(brand);
    }
}
