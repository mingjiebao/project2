package com.cskaoyan.controller.wx.comment;

import com.cskaoyan.model.BaseRespVo;
import com.cskaoyan.model.wx.comment.bo.CommentCountBO;
import com.cskaoyan.model.wx.comment.bo.CommentListBO;
import com.cskaoyan.model.wx.comment.bo.CommentPostBO;
import com.cskaoyan.model.wx.comment.vo.CommentCountVO;
import com.cskaoyan.model.wx.comment.vo.CommentListVO;
import com.cskaoyan.model.wx.comment.vo.CommentPostVO;
import com.cskaoyan.service.wx.comment.WXCommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("wx/comment")
public class WXCommentController {

    @Autowired
    WXCommentService wxCommentService;

    @GetMapping("list")
    //评论显示
    public BaseRespVo commentListInfo(CommentListBO commentListBO) {
        CommentListVO commentListVO = wxCommentService.commentListInfo(commentListBO);
        return BaseRespVo.ok(commentListVO);
    }

    @GetMapping("count")
    //评论数量
    public BaseRespVo commentCount(CommentCountBO commentCountBO) {
        CommentCountVO commentCountVO = wxCommentService.commentCount(commentCountBO);
        return BaseRespVo.ok(commentCountVO);
    }

    @PostMapping("post")
    //上传评论
    public BaseRespVo commentPost(@RequestBody CommentPostBO commentPostBO) {
        CommentPostVO commentPostVO = wxCommentService.commentPost(commentPostBO);
        return BaseRespVo.ok(commentPostVO);
    }
}
