package com.cskaoyan.controller.wx.aftersale;

import com.cskaoyan.model.BaseRespVo;
import com.cskaoyan.model.admin.marketbeans.pojo.MarketAftersale;
import com.cskaoyan.model.wx.aftersale.WxAftersaleListVo;
import com.cskaoyan.service.wx.WxOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @package: com.cskaoyan.controller.wx.aftersale
 * @Description: 微信小程序申请售后
 * @author: 北青
 * @date: 2022/1/11 20:56
 */
@RestController
@RequestMapping("wx/aftersale")
public class AftersaleController {

    @Autowired
    WxOrderService wxOrderService;
    /**
     * 提交售后信息
     * @return
     */
    @PostMapping("submit")
    public BaseRespVo submitAftersale(@RequestBody MarketAftersale marketAftersale){
        wxOrderService.submitAftersale(marketAftersale);
        return BaseRespVo.ok();
    }

    /**
     * 显示售后信息
     * @param status
     * @param page
     * @param limit
     * @return
     */
    @GetMapping("list")
    public BaseRespVo getAftersaleListInfo(Integer status, Integer page, Integer limit){
        WxAftersaleListVo aftersaleListInfo = wxOrderService.getAftersaleListInfo(status, page, limit);
        return BaseRespVo.ok(aftersaleListInfo);
    }

    @GetMapping("detail")
    public BaseRespVo getAftersaleDetail(Integer orderId){
        return BaseRespVo.ok("未开放该功能");
    }

}
