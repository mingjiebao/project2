package com.cskaoyan.controller.wx.coupon;

import com.cskaoyan.model.BaseData;
import com.cskaoyan.model.BaseRespVo;
import com.cskaoyan.model.realm.WxLoginUserData;
import com.cskaoyan.model.wx.wxcoupon.bo.CouponBaseBo;
import com.cskaoyan.model.wx.wxcoupon.vo.CouponBaseVo;
import com.cskaoyan.model.wx.wxcoupon.vo.CouponSelectVo;
import com.cskaoyan.service.wx.coupon.WXCouponService;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("wx/coupon")
public class WxCouponController {

    @Autowired
    WXCouponService service;

    @RequestMapping("list")
    public BaseRespVo getCouponList(Integer limit, Integer page) {
        BaseData result = service.getCouponListInfo(limit, page);
        return BaseRespVo.ok(result);
    }

    /**
     * 个人首页的我的优惠券
     */
    @RequestMapping("mylist")
    public BaseRespVo getMyCouponList(CouponBaseBo info) {
        CouponBaseVo result = service.getCouponMyList(info);
        return BaseRespVo.ok(result);
    }


    /**
     * @param map： json数据 code:"兑换码"
     * @return :返回操作结果
     */
    @RequestMapping("exchange")
    public BaseRespVo exchangeCoupon(@RequestBody Map map) {
        String code = (String) map.get("code");
        WxLoginUserData principal = (WxLoginUserData) SecurityUtils.getSubject().getPrincipal();

        boolean result = service.exchangeCoupon(code, principal.getId());

        if (!result) {
            return BaseRespVo.fail("已经兑过了");
        }
        return BaseRespVo.ok("成功");
    }


    /**
     * @param map: map中只接收一个参数  couponId:[value]
     * @return
     */
    @RequestMapping("receive")
    public BaseRespVo receiveCoupon(@RequestBody Map map) {

        boolean result = service.receiveCoupon((int) map.get("couponId"));
        if (result) {
            return BaseRespVo.ok();
        } else {
            return BaseRespVo.fail("不满足领取条件");
        }
    }

    @RequestMapping("selectlist")
    public BaseRespVo couponSelectList(Integer cartId, Integer grouponRulesId) {
        BaseData result = service.getCouponSelectList(cartId, grouponRulesId);
        return BaseRespVo.ok(result);
    }

}
