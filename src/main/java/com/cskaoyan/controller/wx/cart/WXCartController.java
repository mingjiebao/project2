package com.cskaoyan.controller.wx.cart;

import com.cskaoyan.exception.wx.cart.WxCartException;
import com.cskaoyan.model.BaseRespVo;
import com.cskaoyan.model.wx.cart.*;
import com.cskaoyan.service.wx.cart.WxCartService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Author:
 * @description:
 * @keypoint:
 * @tags:
 * @related:
 */

@RestController
@RequestMapping("wx/cart")
public class WXCartController {

    @Autowired
    WxCartService wxCartServiceImpl;
    /**
     * 购物车图标上面那个数字，也就是购物车里面总的数量
     * 有些的* 2 ，也要算上
     * @return
     */
    @RequestMapping("goodscount")
    public BaseRespVo<Integer> getTotalGoodsNumInCart() throws WxCartException {
        Integer num = wxCartServiceImpl.selectGoodsCountInCart();

        return BaseRespVo.ok(num);
    }

    /**
     * 添加到购物车
     * 传过来的参数{goodsId: 1181000, number: 1, productId: 2}
     * goodsId: 1181000
     * number: 1
     * productId: 2
     *
     * 返回目前购物车的数量，goodscount接口
     */
    @RequestMapping("add")
    public BaseRespVo addCart(@RequestBody GoodsAndProductAndNumber goodsAndProductAndNumber) throws WxCartException {
        //商品插入
        wxCartServiceImpl.insertItemToCart(goodsAndProductAndNumber,false);
        //返回
        return getTotalGoodsNumInCart(); //todo: 使用redis取出当前的+1

    }


    /**
     * 购物车页面的显示
     * @return
     */
    @RequestMapping("index")
    public BaseRespVo<CartIndexVO> listCart() throws WxCartException, InstantiationException, IllegalAccessException {
        CartIndexVO cartIndexVO = wxCartServiceImpl.selectCartInfo();
        return BaseRespVo.ok(cartIndexVO);
    }

    /**
     * 前端看起来，只能更新数量
     * {"productId":257,"goodsId":1181013,"number":2,"id":203}
     */
    @RequestMapping("update")
    public BaseRespVo update(@RequestBody CartUpdateBO cartUpdateBO) throws WxCartException {
        wxCartServiceImpl.updateCartInfo(cartUpdateBO);

        return BaseRespVo.ok();
    }


    /**
     * 选中与否
     * 返回的就checkedGoodsAmount，和checkedGoodsCount 不一样，
     * {"productIds":[247,244,4,239,101,14,24],"isChecked":0}
     */
    @RequestMapping("checked")
    public BaseRespVo<CartIndexVO> setCheckStatus(@RequestBody ProductIdsAndChecked productIdsAndChecked) throws WxCartException, InstantiationException, IllegalAccessException {
        wxCartServiceImpl.updateCartChecked(productIdsAndChecked);
        return listCart();
    }


    /**
     * 立即购买跳转过来的
     */
    @RequestMapping("fastadd")
    public BaseRespVo buyItemImmediately(@RequestBody GoodsAndProductAndNumber goodsAndProductAndNumber) throws WxCartException {

        Integer id = wxCartServiceImpl.insertItemToCart(goodsAndProductAndNumber,true);

        return BaseRespVo.ok(id); //todo： 这里要返回给前端的数据data是什么意思？？
    }

    /**
     * 选中购物车商品结算 ，点击下单
     * checkout?cartId=0&addressId=15&couponId=0&userCouponId=11&grouponRulesId=0
     */
    @RequestMapping("checkout")
    public BaseRespVo<CheckOutVO> checkOut(CheckOut checkOut) throws InstantiationException, IllegalAccessException, WxCartException {

        CheckOutVO checkOutVO = wxCartServiceImpl.getCheckOutInfo(checkOut);
        return BaseRespVo.ok(checkOutVO);
    }

    /**
     * 购物车编辑那里 ，选中，删除
     * {"productIds":[204,70,2,5,3]}
     */
    @RequestMapping("delete")
    public BaseRespVo delete(@RequestBody ProductIdsAndChecked productIds) throws WxCartException, InstantiationException, IllegalAccessException {
        List<Integer> productIds1 = productIds.getProductIds();
        wxCartServiceImpl.deleteCart(productIds1);

        return listCart();
    }

}
