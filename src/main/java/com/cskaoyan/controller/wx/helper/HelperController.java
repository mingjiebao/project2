package com.cskaoyan.controller.wx.helper;

import com.cskaoyan.model.BaseRespVo;
import com.cskaoyan.model.PageLimitRepsVo;
import com.cskaoyan.service.wx.helper.HelperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("wx/issue")
@RestController
public class HelperController {

    @Autowired
    HelperService service;

    @GetMapping("list")
    public BaseRespVo getIssueList(Integer page, Integer limit) {
        PageLimitRepsVo result = service.getHelperIssueInfo(page, limit);
        return BaseRespVo.ok(result);
    }
}
