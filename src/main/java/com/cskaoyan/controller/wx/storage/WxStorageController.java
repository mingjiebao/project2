package com.cskaoyan.controller.wx.storage;

import com.cskaoyan.exception.adminmanage.AdminStorageException;
import com.cskaoyan.model.BaseRespVo;
import com.cskaoyan.model.admin.adminmanage.storage.StorageListPOJO;
import com.cskaoyan.service.admin.adminmanage.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @Author:
 * @description:
 * @keypoint:
 * @tags:
 * @related:
 */

@RestController
@RequestMapping("wx/storage")
public class WxStorageController {

    @Autowired
    StorageService storageServiceImpl;

    @RequestMapping("upload")
    public BaseRespVo<StorageListPOJO> upload(MultipartFile file) throws AdminStorageException {
        StorageListPOJO storageListPOJO = storageServiceImpl.addNewStorage(file);
        return BaseRespVo.ok(storageListPOJO);
    }
}
