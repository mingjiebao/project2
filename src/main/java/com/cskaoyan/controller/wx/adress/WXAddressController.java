package com.cskaoyan.controller.wx.adress;

import com.cskaoyan.exception.ParamsException;
import com.cskaoyan.model.BaseRespVo;
import com.cskaoyan.model.wx.wxadressbean.bo.WXAddressSaveBo;
import com.cskaoyan.model.wx.wxadressbean.vo.WXAddressDetailVo;
import com.cskaoyan.model.wx.wxadressbean.vo.WXAdressList;
import com.cskaoyan.service.wx.Shippingaddress.AddressService;
import com.cskaoyan.utils.ValidationUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @description:
 * @author: XM
 * @date: 2022/1/10 20:17
 */
@RequestMapping("wx/address")
@RestController
public class WXAddressController {
    @Autowired
    AddressService addressService;

    @RequestMapping("list")
    public BaseRespVo addressList() {
        WXAdressList wxAdressList = addressService.addressList();
        return BaseRespVo.ok(wxAdressList);
    }

    @GetMapping("detail")
    public BaseRespVo addressDetail(Integer id){
        WXAddressDetailVo adressDetail = addressService.getAdressDetail(id);
        return BaseRespVo.ok(adressDetail);
    }

    @PostMapping("save")
    public BaseRespVo addressSave(@RequestBody @Validated WXAddressSaveBo addressSaveBo, BindingResult bindingResult) throws ParamsException {
        ValidationUtil.validate(bindingResult);
        Integer id = addressService.addressSaveMsg(addressSaveBo);
        return BaseRespVo.ok(id);
    }

    @RequestMapping("delete")
    public BaseRespVo addressDelete(@RequestBody Map map){
        Integer id = (Integer) map.get("id");
        Integer i = addressService.addressDelete(id);
        if (i==1){
            return BaseRespVo.ok();
        }
        return BaseRespVo.fail("删除失败，服务器繁忙请稍后..");
    }
}