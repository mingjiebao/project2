package com.cskaoyan.controller.wx.feedback;


import com.cskaoyan.model.BaseRespVo;
import com.cskaoyan.model.wx.feedback.bo.FeedBackSubmitBo;
import com.cskaoyan.service.wx.feedback.FeedBackService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("wx/feedback")
@RestController
public class FeedBackController {

//    @Autowired
//    FeedBackService service;

    // 调用接口前必须认证过
    @RequestMapping("submit")
    public BaseRespVo submitFeedBackMsg(@RequestBody FeedBackSubmitBo bo) {

        // todo:想办法拿到username 和 id
        Subject subject = SecurityUtils.getSubject();
        final Session session = subject.getSession();
        final Object username = session.getAttribute("username");
//        service.submitFeedBack(bo, "user123", 1);
        return BaseRespVo.ok();
    }
}

