package com.cskaoyan.controller.wx.ctalog;

import com.cskaoyan.model.BaseRespVo;
import com.cskaoyan.model.wx.catalog.CatalogIndexVO;
import com.cskaoyan.model.wx.catalog.CurrentVO;
import com.cskaoyan.service.wx.catalog.CatalogService;
import com.fasterxml.jackson.databind.ser.Serializers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author AhaNg
 * @Date 2022/1/11 12:20
 * @description:
 * @return:
 */
@RestController
@RequestMapping("wx/catalog")
public class WxCatalogController {

    @Autowired
    CatalogService catalogService;

    /**
     * @Author: AhaNg
     * @description:  分类的详情显示
     * @return:
     */
    @GetMapping("current")
    public BaseRespVo current(Integer id){
        CurrentVO currentVO=catalogService.current(id);
        return BaseRespVo.ok(currentVO);
    }


    /**
     * @Author: AhaNg
     * @description: 进入分类页面的显示
     * @return:
     */
    @GetMapping("index")
    public BaseRespVo index(){

        CatalogIndexVO currentVO=catalogService.index();
        return  BaseRespVo.ok(currentVO);
    }
}
