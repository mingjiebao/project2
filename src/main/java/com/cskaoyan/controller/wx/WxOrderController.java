package com.cskaoyan.controller.wx;

import com.cskaoyan.model.BaseParam;
import com.cskaoyan.model.BaseRespVo;
import com.cskaoyan.model.admin.goodsbean.Comment;
import com.cskaoyan.model.wx.order.bo.CommentSession;
import com.cskaoyan.model.wx.order.bo.WxOrderSubmitBo;
import com.cskaoyan.model.wx.order.vo.WxOrderDetailGoodsVo;
import com.cskaoyan.model.wx.order.vo.WxOrderDetailInfoVo;
import com.cskaoyan.model.wx.order.vo.WxOrderListInfoVo;
import com.cskaoyan.model.wx.order.vo.WxOrderSubmitVo;
import com.cskaoyan.service.wx.WxOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;


/**
 * @package: com.cskaoyan.controller.wx
 * @Description: 微信小程序前台订单模块
 * @author: 北青
 * @date: 2022/1/10 15:00
 */
@RestController
@RequestMapping("wx/order")
public class WxOrderController {

    @Autowired
    WxOrderService wxOrderService;

    /**
     * 微信小程序个人中心查看订单——显示订单详细信息
     * @return
     */
    @RequestMapping("list")
    public BaseRespVo getWxOrderListInfo(BaseParam baseParam, Integer showType){
        WxOrderListInfoVo wxOrderListInfo = wxOrderService.getWxOrderListInfo(baseParam, showType);
        return BaseRespVo.ok(wxOrderListInfo);
    }

    /**
     * 显示订单详情
     * @param orderId: 订单id
     * @return
     */
    @GetMapping("detail")
    public BaseRespVo getWxOrderDetailInfo(Integer orderId, HttpSession session){
        session.setAttribute("orderId", orderId);
        WxOrderDetailInfoVo wxOrderDetailVo = wxOrderService.getWxOrderDetailInfo(orderId);
        session.setAttribute("orderGoods", wxOrderDetailVo.getOrderGoods());
        return BaseRespVo.ok(wxOrderDetailVo);
    }

    /**
     * 取消订单
     * @return
     */
    @PostMapping("cancel")
    public BaseRespVo cancelOrder(HttpSession session){
        Integer orderId = (Integer) session.getAttribute("orderId");
        wxOrderService.cancelOrder(orderId);
        //将订单中的商品数返回
        List<WxOrderDetailGoodsVo> orderGoods = (List<WxOrderDetailGoodsVo>) session.getAttribute("orderGoods");
        wxOrderService.returnGoods(orderGoods);
        return BaseRespVo.ok("成功");
    }

    /**
     * 申请退款
     * @return
     */
    @PostMapping("refund")
    public BaseRespVo refund(HttpSession session){
        Integer orderId = (Integer) session.getAttribute("orderId");
        wxOrderService.refund(orderId);
        //将订单中的商品数返回
        List<WxOrderDetailGoodsVo> orderGoods = (List<WxOrderDetailGoodsVo>) session.getAttribute("orderGoods");
        wxOrderService.returnGoods(orderGoods);
        return BaseRespVo.ok();
    }

    /**
     * 删除订单
     * @param session
     * @return
     */
    @PostMapping("delete")
    public BaseRespVo deleteOrder(HttpSession session){
        Integer orderId = (Integer) session.getAttribute("orderId");
        wxOrderService.deleteOrder(orderId);
        return BaseRespVo.ok();
    }

    /**
     * 确认收货
     * @param session
     * @return
     */
    @PostMapping("confirm")
    public BaseRespVo confirm(HttpSession session){
        Integer orderId = (Integer) session.getAttribute("orderId");
        wxOrderService.confirm(orderId);
        return BaseRespVo.ok();
    }

    /**
     * 评价
     * @return
     */
    @GetMapping("goods")
    public BaseRespVo beforeCommentGoodsInfo(Integer orderId, Integer goodsId, HttpSession session){

        CommentSession commentValue = CommentSession.getComment(orderId, goodsId);
        session.setAttribute("commentValue", commentValue);

        WxOrderDetailInfoVo wxOrderDetailInfo = wxOrderService.getWxOrderDetailInfo(orderId);
        List<WxOrderDetailGoodsVo> orderGoods = wxOrderDetailInfo.getOrderGoods();
        for (WxOrderDetailGoodsVo orderGood : orderGoods) {
            if (orderGood.getId().equals(goodsId)) return BaseRespVo.ok(orderGood);
        }
        return BaseRespVo.ok();
    }

    /**
     * 提交评价内容
     * @return
     */
    @PostMapping("comment")
    public BaseRespVo comment(@RequestBody Comment comment, HttpSession session){
        //拿到评价对应的商品和订单id
        CommentSession commentValue = (CommentSession) session.getAttribute("commentValue");

        wxOrderService.comment(comment, commentValue);
        return BaseRespVo.ok();
    }

    /**
     * 订单付款
     * @param map 封装了一个参数,是订单ID
     * @return
     */
    @PostMapping("prepay")
    public BaseRespVo prepay(@RequestBody Map<String, Integer> map){ //map当中封装的是订单id
        Integer orderId = map.get("orderId");
        wxOrderService.prepay(orderId);
        return BaseRespVo.ok("付款成功");
    }

    /**
     * 提交订单
     * @param wxOrderSubmitBo
     * @return
     */
    @PostMapping("submit")
    public BaseRespVo submit(@RequestBody WxOrderSubmitBo wxOrderSubmitBo){
        //新增订单表
        WxOrderSubmitVo order = wxOrderService.createOrder(wxOrderSubmitBo);
        return BaseRespVo.ok(order);
    }

}
