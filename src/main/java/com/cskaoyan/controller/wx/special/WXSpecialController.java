package com.cskaoyan.controller.wx.special;

import com.cskaoyan.model.BaseRespVo;
import com.cskaoyan.model.wx.special.bo.SpecialBO;
import com.cskaoyan.model.wx.special.vo.SpecialDetailVO;
import com.cskaoyan.model.wx.special.vo.SpecialListVO;
import com.cskaoyan.model.wx.special.vo.SpecialRelatedVO;
import com.cskaoyan.service.wx.special.WXSpecialService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("wx/topic")
public class WXSpecialController {

    @Autowired
    WXSpecialService wxSpecialService;

    @GetMapping("list")
    //专题精选 页面数据显示
    public BaseRespVo specialListInfo(SpecialBO specialBO) {
        SpecialListVO specialListVO = wxSpecialService.specialListInfo(specialBO);
        return BaseRespVo.ok(specialListVO);
    }

    @GetMapping("detail")
    //专题精选 专题详情
    public BaseRespVo specialDetail(Integer id) {
        SpecialDetailVO specialDetailVO = wxSpecialService.specialDetail(id);
        return BaseRespVo.ok(specialDetailVO);
    }

    @GetMapping("related")
    //专题精选 专题相关推荐
    public BaseRespVo specialRelated(Integer id) {
        SpecialRelatedVO specialRelatedVO = wxSpecialService.specialRelated(id);
        return BaseRespVo.ok(specialRelatedVO);
    }
}
