package com.cskaoyan.controller.wx.home;

import com.cskaoyan.model.BaseRespVo;
import com.cskaoyan.model.wx.wxhomeindexbean.WXHomeIndexVo;
import com.cskaoyan.service.wx.homeindex.WXHomeIndexservice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @description:
 * @author: XM
 * @date: 2022/1/11 3:53
 */
@RequestMapping("wx/home")
@RestController
public class WXHomeController {
@Autowired
WXHomeIndexservice WXHomeIndexservice;

@RequestMapping("index")
    public BaseRespVo homeIndex(){
    WXHomeIndexVo data = WXHomeIndexservice.homeIndex();
    return BaseRespVo.ok(data);
}
}