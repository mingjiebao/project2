package com.cskaoyan.controller.wx.home;

import com.cskaoyan.model.BaseRespVo;
import com.cskaoyan.model.wx.wxindexbean.LoginIndexVo;
import com.cskaoyan.model.wx.wxuserdetailbean.WXUserDetailVo;
import com.cskaoyan.service.wx.usermanage.WXUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author : [Dragon]
 * @createTime : [2022/1/10 14:59]
 * @description :
 */

@RestController
@RequestMapping("wx/user")
public class WXUserController {

    @Autowired
    WXUserService wxUserService;

    @GetMapping("index")
    public BaseRespVo getIndex(){
        LoginIndexVo loginIndexVo=wxUserService.getIndex();
        /*
        //拿到principal
        WxLoginUserData principal = (WxLoginUserData) SecurityUtils.getSubject().getPrincipal();
        //获取登录用户的id
        Integer id = principal.getId();
        //获取登录用户的username
        String username = principal.getUsername();
        //获取登录用户的登陆时间
        String login_time = principal.getLogin_time();
        //获取登录用户的登录ip
        String login_ip = principal.getLogin_ip();*/

        return BaseRespVo.ok(loginIndexVo);
    }
/*@GetMapping("detail")
    public BaseRespVo getUserDetail(Integer id){
    WXUserDetailVo wxUserDetailVo=wxUserService.getUserDetail(id);
        return BaseRespVo.ok(wxUserDetailVo);
}*/
}
