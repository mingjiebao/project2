package com.cskaoyan.controller.wx.home;

import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.cskaoyan.model.BaseRespVo;
import com.cskaoyan.model.wx.wxhomeindexbean.WXRegisterBo;
import com.cskaoyan.model.wx.wxloginbean.LoginVo;
import com.cskaoyan.model.wx.wxloginbean.UserInfo;
import com.cskaoyan.service.wx.loginmanage.WXLoginService;
import org.apache.shiro.SecurityUtils;

import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.omg.CORBA.BAD_CONTEXT;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.util.Map;

import com.cskaoyan.model.realm.UsernamePasswordTypeToken;

/**
 * @author : [Dragon]
 * @createTime : [2022/1/10 14:18]
 * @description :
 */

@RestController
@RequestMapping("wx/auth")
public class WXLoginController {

    @Autowired
    WXLoginService wxLoginService;

    @RequestMapping("login")
    public BaseRespVo login(@RequestBody Map map, HttpServletRequest req) {
        String username = (String) map.get("username");
        String password = (String) map.get("password");

        Subject subject = SecurityUtils.getSubject();
        String host = req.getRemoteAddr();
        UsernamePasswordTypeToken token = new UsernamePasswordTypeToken(username, password, host, "wx");
        try {
            subject.login(token);
        } catch (Exception e) {
            return BaseRespVo.fail("账号或密码错误，请重新登录");
        }
        Session session = subject.getSession();
        Serializable id = session.getId();

        UserInfo userInfo = wxLoginService.userLogin(username, password);

        /*   token.setRememberMe(true);*/
        return BaseRespVo.ok(new LoginVo(userInfo, (String) id));
    }


    @RequestMapping("logout")
    public BaseRespVo loginOut() {
        SecurityUtils.getSubject().logout();
        return BaseRespVo.ok("退出成功");
    }

    @PostMapping("regCaptcha")
    public BaseRespVo regCaptcha(@RequestBody Map map) {
        String mobile = (String) map.get("mobile");
        System.out.println("----------------" + mobile);
        String accessKeyId = "LTAI5t8gpxPTCR6W58RnZq4u";
        String accessKeySecret = "mVPbpbxia0JQotb7HyJAREV8QUuq8h";
        String signName = "stone4j";
        String templateCode = "SMS_173765187";

        DefaultProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId, accessKeySecret);
        DefaultAcsClient client = new DefaultAcsClient(profile);

        CommonRequest request = new CommonRequest();
        request.setMethod(MethodType.POST);
        request.setDomain("dysmsapi.aliyuncs.com");
        request.setVersion("2017-05-25");
        request.setAction("SendSms");
        request.putQueryParameter("RegionId", "cn-hangzhou");
        request.putQueryParameter("PhoneNumbers", mobile);
        request.putQueryParameter("TemplateCode", templateCode);
        request.putQueryParameter("SignName", signName);
        request.putQueryParameter("TemplateParam", "{\"code\":\"65536\"}");

        CommonResponse response = null;
        try {
            response = client.getCommonResponse(request);
            System.out.println(response.getData());
        } catch (ClientException e) {
            e.printStackTrace();
        }
        return BaseRespVo.ok(response.getData());
    }

    @RequestMapping("register")
    public BaseRespVo register(@RequestBody @Validated WXRegisterBo wxRegisterBo, BindingResult bindingResult) {
        wxLoginService.userRegister(wxRegisterBo);
        return BaseRespVo.ok();
    }
}
