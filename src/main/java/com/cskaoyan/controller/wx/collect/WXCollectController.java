package com.cskaoyan.controller.wx.collect;

import com.cskaoyan.model.BaseRespVo;
import com.cskaoyan.model.wx.wxcollect.bo.CollectAddorDeleteBo;

import com.cskaoyan.model.wx.wxcollect.vo.WXCollectListVo;
import com.cskaoyan.service.wx.goodscollect.WXCollectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @description:
 * @author: XM
 * @date: 2022/1/10 14:59
 */
@RestController
@RequestMapping("wx/collect")
public class WXCollectController {
    @Autowired
    WXCollectService collectService;

    @RequestMapping("list")
    public BaseRespVo collectList(Integer type,Integer page,Integer limit){
        WXCollectListVo wxCollectListVo = collectService.collectList(type, page, limit);
        return BaseRespVo.ok(wxCollectListVo);
    }

    @PostMapping("addordelete")
    public BaseRespVo collectAddordelete(@RequestBody CollectAddorDeleteBo collectAddorDeleteBo){
        Integer number=collectService.collectAddordelete(collectAddorDeleteBo);
        if (number==1){
            return BaseRespVo.ok();
        }
        return BaseRespVo.fail();
    }
}