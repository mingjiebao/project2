package com.cskaoyan.exception;

/**
 * @author stone
 * @date 2022/01/03 17:55
 */
public class ParamsException extends Exception{
    public ParamsException(String message) {
        super(message);
    }
}
