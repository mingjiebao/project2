package com.cskaoyan.exception;


import com.cskaoyan.exception.adminmanage.AdminStorageException;
import com.cskaoyan.exception.wx.cart.WxCartException;
import com.cskaoyan.model.BaseRespVo;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.cskaoyan.exception.adminmanage.AdminAdminException;
import com.cskaoyan.exception.adminmanage.AdminLogException;
import com.cskaoyan.exception.adminmanage.AdminRoleException;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;


@RestControllerAdvice
public class ExceptionHandle {
    @ExceptionHandler(ParamsException.class)
    public BaseRespVo validationException(ParamsException exception) {
        String message = exception.getMessage();
        return BaseRespVo.fail(message);
    }

    /**
     * 处理系统管理模块的异常
     *
     * @param exception
     * @return
     * @Author Yeah
     * @Author Yeah
     */
    @ExceptionHandler({AdminAdminException.class, AdminRoleException.class, AdminStorageException.class, AdminLogException.class})
    public BaseRespVo adminAdminExceptionHandle(Exception exception) {
        exception.printStackTrace();
        String message = exception.getMessage();
        return BaseRespVo.fail(message);
    }


    /**
     * 购物车的异常处理
     * @param exception
     * @return
     */
    @ExceptionHandler({WxCartException.class})
    public BaseRespVo wxCartExceptionHandle(Exception exception){
        exception.printStackTrace();
        String message = exception.getMessage();
        return BaseRespVo.fail(message);
    }

    /**
     * 查询订单信息时传入不合法的ID
     *
     * @param exception
     * @return
     */
    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public BaseRespVo marketOrderDetailException(MethodArgumentTypeMismatchException exception) {
        exception.printStackTrace();
        return BaseRespVo.fail("请输入一个合法的订单ID");
    }

    @ExceptionHandler(Exception.class)
    public void deTest(Exception e){
        e.printStackTrace();
    }
}
