package com.cskaoyan.exception.adminmanage;

/**
 * @description:
 * @keypoint:
 * @tags:
 * @related:
 */

public class AdminStorageException extends Exception{
    public AdminStorageException(String message) {
        super(message);
    }
}
