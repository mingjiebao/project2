package com.cskaoyan.exception.adminmanage;

/**
 * @Author:
 * @description:
 * @keypoint:
 * @tags:
 * @related:
 */

public class AdminAdminException extends Exception{
    public AdminAdminException(String message) {
        super(message);
    }
}
