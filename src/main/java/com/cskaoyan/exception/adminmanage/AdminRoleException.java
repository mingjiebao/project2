package com.cskaoyan.exception.adminmanage;

/**
 * @Author:
 * @description:
 * @keypoint:
 * @tags:
 * @related:
 */

public class AdminRoleException extends Exception {
    public AdminRoleException(String message) {
        super(message);
    }
}
