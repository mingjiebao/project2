package com.cskaoyan.exception.adminmanage;

/**
 * @Author:
 * @description:
 * @keypoint:
 * @tags:
 * @related:
 */

public class AdminLogException extends Exception{
    public AdminLogException(String message) {
        super(message);
    }
}
