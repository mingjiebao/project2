package com.cskaoyan.exception.wx.cart;

/**
 * @Author:
 * @description:
 * @keypoint:
 * @tags:
 * @related:
 */

public class WxCartException extends Exception {

    public WxCartException(String message) {
        super(message);
    }
}
