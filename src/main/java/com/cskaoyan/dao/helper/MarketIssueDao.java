package com.cskaoyan.dao.helper;

import com.cskaoyan.model.wx.helper.HelperListVo;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MarketIssueDao {
    List<HelperListVo> getIssueListInfo();
}
