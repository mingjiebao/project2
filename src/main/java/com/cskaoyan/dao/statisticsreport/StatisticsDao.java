package com.cskaoyan.dao.statisticsreport;

import com.cskaoyan.model.admin.statisticsreport.vo.GoodsStatisticsRowVO;
import com.cskaoyan.model.admin.statisticsreport.vo.OrderStatisticsRowVO;

import java.util.List;

public interface StatisticsDao {
    //用户统计
    //查询用户加入的日期
    List<String> selectUserAddTime();

    //通过加入时间查询出人数
    Integer selectUserCountByAddTime(String date);

    //订单统计
    //查询订单创建的日期
    List<String> selectOrderAddTime();

    //通过时间查询出其余字段
    OrderStatisticsRowVO selectOrderByAddTime(String date);

    //商品统计
    //查询订单创建的日期
    List<String> selectGoodsAddTime();

    //通过时间查询出其余字段
    GoodsStatisticsRowVO selectGoodsByAddTime(String date);
}
