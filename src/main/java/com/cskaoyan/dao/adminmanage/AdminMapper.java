package com.cskaoyan.dao.adminmanage;

import com.cskaoyan.model.admin.adminmanage.admin.AdminPOJO;
import com.cskaoyan.model.admin.adminmanage.admin.AdminListVO;
import com.cskaoyan.model.realm.AdminLoginAdminData;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author:
 * @description:
 * @keypoint:
 * @tags:
 * @related:
 */
public interface AdminMapper {

    List<AdminListVO> selectAdminInfo(@Param("sort") String sort, @Param("order") String order, @Param("username") String username);

    Integer insertNewAdmin(AdminPOJO bean);

    Integer updateAdminDeletedById(Integer id);

    Integer updateAdminInfo(@Param("pojo") AdminPOJO pojo);

    AdminLoginAdminData selectAdminPasswordByAdminName(String adminName);

    String selectRolesById(Integer id);

    List<String> selectRolesNameByRolesId(@Param("rolesIds")Integer[] integers);

    List<String> selectPermsByRolesId(@Param("rolesIds")Integer[] integers);
}
