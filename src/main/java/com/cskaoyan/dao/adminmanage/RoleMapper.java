package com.cskaoyan.dao.adminmanage;

import com.cskaoyan.model.admin.adminmanage.role.*;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author:
 * @description:
 * @keypoint:
 * @tags:
 * @related:
 */

public interface RoleMapper {

    Integer getItemNum();

    List<RoleDTO> selectSimpleRole();

    Integer insertToModule(@Param("sys") List<ModuleVO> systemPermissonBO);

    Integer insertToFunction(@Param("sys") List<FunctionVO> systemPermissonBO);

    Integer insertToApi(@Param("sys") List<ApiVO> systemPermissonBO);

    Integer deleteAllPermissionById(Integer roleId);

    Integer insertRolePersmission(@Param("per") PermissionUpdateBO permissionUpdateBO);

    List<ModuleVO> getAllSystemPermission();

    List<String> getAllRolePermissionById(Integer roleId);

    List<RoleDetailDTO> selectAllRole(@Param("sort") String sort, @Param("order") String order, @Param("name") String name);

    Integer insertNewRole(@Param("bean") RoleDetailDTO bean);

    Integer updateRoleInfo(@Param("id")Integer id, @Param("name") String name,@Param("desc") String desc);

    Integer deleteRole(Integer id);
}
