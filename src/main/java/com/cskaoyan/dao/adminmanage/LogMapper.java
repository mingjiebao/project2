package com.cskaoyan.dao.adminmanage;

import com.cskaoyan.model.admin.adminmanage.log.LogListDTO;
import com.cskaoyan.model.admin.adminmanage.log.LogMessagePOJO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author:
 * @description:
 * @keypoint:
 * @tags:
 * @related:
 */

public interface LogMapper {
    List<LogListDTO> selectLogInfo(@Param("sort") String sort, @Param("order") String order, @Param("name") String name);

    String selectLabelByApi(String api);

    Integer insertLog(LogMessagePOJO logMessagePOJO);
}
