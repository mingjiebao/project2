package com.cskaoyan.dao.adminmanage;

import com.cskaoyan.model.admin.adminmanage.storage.StorageListPOJO;
import com.cskaoyan.model.admin.adminmanage.storage.StorageParam;
import com.cskaoyan.model.admin.adminmanage.storage.StorageVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @description:
 * @keypoint:
 * @tags:
 * @related:
 */

public interface StorageMapper {
    List<StorageVO> selectStorageInfoByStorageParam(@Param("storageParam") StorageParam storageParam);

    Integer setDeletedTrueById(Integer id);

    Integer updateStorageInfo(@Param("stld") StorageListPOJO storageListDTO);

    Integer insertNewStorage(@Param("pojo") StorageListPOJO storageListPOJO);
}
