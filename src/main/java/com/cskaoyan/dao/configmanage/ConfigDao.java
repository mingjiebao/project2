package com.cskaoyan.dao.configmanage;

import com.cskaoyan.model.admin.configmanage.pojo.SystemConfigPojo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ConfigDao {
    //商场配置
    //商场配置 页面数据显示
    List<SystemConfigPojo> selectMallConfig();

    //商场配置 查找表中字段对应的id
    Integer selectIdMallConfig(@Param("poJo") SystemConfigPojo poJo);

    //商场配置 确认并插入配置项
    void insertMallConfigById(@Param("poJo") SystemConfigPojo poJo);

    //商场配置 确认并更新配置项
    void updateMallConfigById(@Param("poJo") SystemConfigPojo poJo);

    //运费配置
    //运费配置 页面数据显示
    List<SystemConfigPojo> selectExpressConfig();

    //运费配置 查找表中字段对应的id
    Integer selectIdExpressConfig(@Param("poJo") SystemConfigPojo poJo);

    //运费配置 确认并插入配置项
    void insertExpressConfigById(@Param("poJo") SystemConfigPojo poJo);

    //运费配置 确认并更新配置项
    void updateExpressConfigById(@Param("poJo") SystemConfigPojo poJo);

    //订单配置
    //订单配置 页面数据显示
    List<SystemConfigPojo> selectOrderConfig();

    //订单配置 查找表中字段对应的id
    Integer selectIdOrderConfig(@Param("poJo") SystemConfigPojo poJo);

    //订单配置 确认并插入配置项
    void insertOrderConfigById(@Param("poJo") SystemConfigPojo poJo);

    //订单配置 确认并更新配置项
    void updateOrderConfigById(@Param("poJo") SystemConfigPojo poJo);

    //小程序配置
    //小程序配置 页面数据显示x
    List<SystemConfigPojo> selectWxConfig();

    //小程序配置 查找表中字段对应的id
    Integer selectIdWxConfig(@Param("poJo") SystemConfigPojo poJo);

    //小程序配置 确认并插入配置项
    void insertWxConfigById(@Param("poJo") SystemConfigPojo poJo);

    //小程序配置 确认并更新配置项
    void updateWxConfigById(@Param("poJo") SystemConfigPojo poJo);
}
