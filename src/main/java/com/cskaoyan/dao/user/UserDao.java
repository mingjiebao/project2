package com.cskaoyan.dao.user;

import com.cskaoyan.model.admin.user.userbeans.*;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @description:
 * @author: XM
 * @date: 2022/1/6 22:00
 */
@Repository
public interface UserDao {

    Integer selectGoodsCount();

    Integer selectUserCount();

    Integer selectProductCount();

    Integer selectOrderCount();

    List<UserListVo> selectUserList(String username, String mobile, String sort, String order);

    UserListVo selectUserListById(String id);

    Integer userUpdate(User user);

    List<UserAddressList> selectUserAddressList(@Param("name") String name, @Param("userId") String userId, @Param("sort") String sort, @Param("order") String order);

    List<UserCollect> selectUserCollect(String userId, String valueId, String sort, String order);

    List<UserFontPrint> selectFootPrint(@Param("userId") String userId, @Param("goodsId") String goodsId, @Param("sort") String sort, @Param("order") String order);

    List<AdminHistory> selectAdminHistory(@Param("userId") String userId, @Param("keyword") String keyword, @Param("sort") String sort, @Param("order") String order);

    List<UserFeekbackList> selectFeedbackList(@Param("username") String username, @Param("id") String id, @Param("sort") String sort, @Param("order") String order);

    Integer getUserId(String username);

}