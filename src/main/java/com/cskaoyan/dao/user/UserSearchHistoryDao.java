package com.cskaoyan.dao.user;

import com.cskaoyan.model.wx.search.Keyword;
import com.cskaoyan.model.wx.user.UserSearchDto;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserSearchHistoryDao {
    List<String> getAllUserSearchInfo(Integer userId);

    void insetNewSearch(@Param("keyword") String keyword, @Param("userId") Integer userId);

    void updateHistorySearch(@Param("keyword") String keyword, @Param("userId") Integer userId);

    void deleteOneHistory(@Param("del") String del, @Param("userId") Integer userId);
}
