package com.cskaoyan.dao.usercoupon;

import com.cskaoyan.model.admin.coupon.bo.CouponUserBo;
import com.cskaoyan.model.admin.coupon.vo.CouponUserVo;
import com.cskaoyan.model.wx.user.UserCouponPojo;
import com.cskaoyan.model.wx.wxcoupon.WxCouponDto;
import com.cskaoyan.model.wx.wxcoupon.vo.WxCouponVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserCouponDao {
    int getSpecialCouponAccount(@Param("userId") Integer userId, @Param("couponId") Integer couponId);

    int receiveNewCoupon(UserCouponPojo pojo);

    List<WxCouponVo> getUserCouponByStatus(@Param("userId") Integer userId, @Param("status") Integer status);

    List<CouponUserVo> getCouponUserList(CouponUserBo bo);

    void deleteUserCoupon(Integer userCouponId);

    void checkAndUpdateUserCoupon(Integer userId);

    List<UserCouponPojo> getUserCouponList(Integer userId);

    List<WxCouponDto> getWxCouponDto(Integer userId);
}
