package com.cskaoyan.dao.feedback;

import com.cskaoyan.model.wx.feedback.bo.FeedBackSubmitBo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserFeedBackDao {
    void submitFeedBack(@Param("bo") FeedBackSubmitBo bo, @Param("username") String username, @Param("id") Integer id);
}
