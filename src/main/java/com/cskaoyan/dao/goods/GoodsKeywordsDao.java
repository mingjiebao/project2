package com.cskaoyan.dao.goods;

import com.cskaoyan.model.admin.goodsbean.KeywordDto;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface GoodsKeywordsDao {
    List<KeywordDto> getGoodsAllKeyword(Integer goodsId);

    void deleteKeyWord(List<KeywordDto> delete);

    void addNewKeyWord(@Param("set") Set<String> set, @Param("goodsId") Integer goodsId);
}
