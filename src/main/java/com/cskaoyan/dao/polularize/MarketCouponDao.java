package com.cskaoyan.dao.polularize;

import com.cskaoyan.model.admin.coupon.bo.CouponCreateInfoBo;
import com.cskaoyan.model.admin.coupon.bo.CouponListBo;
import com.cskaoyan.model.admin.coupon.bo.CouponUpdateBo;
import com.cskaoyan.model.admin.pojo.MarketCouponPojo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MarketCouponDao {
    List<MarketCouponPojo> getMarketCouponInfo(CouponListBo bo);

    MarketCouponPojo getMarketCouponInfoById(int id);

    void addNewCouponInfo(CouponCreateInfoBo bo);

    void updateMarketCouponInfo(CouponUpdateBo bo);

    List<Integer> getUpdatePojoIds(Integer id);

    void updateTimeInfo(@Param("list") List<Integer> list);

    void updateMarketCouponTotal(@Param("id") Integer id, @Param("total") Integer total, @Param("status") Integer status);

    MarketCouponPojo getMarketCouponInfoByCode(String code);

    List<MarketCouponPojo> getMarketCouponInfoByIds(List<Integer> couponIdList);

    List<String> getGoodsIdValues(String[] goodsValue);
}
