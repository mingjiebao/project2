package com.cskaoyan.dao.polularize;

import com.cskaoyan.model.admin.topic.TopicExample;
import com.cskaoyan.model.admin.topic.bo.TopicUpdateBo;
import com.cskaoyan.model.admin.pojo.MarketTopicPojo;
import com.cskaoyan.model.admin.topic.vo.TopicGoodsListVo;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MarketTopicDao {
    List<MarketTopicPojo> getMarketTopicList(TopicExample example);

    void addNewTopicInfo(MarketTopicPojo pojo);

    void updateTopicInfo(TopicUpdateBo bo);

    void deleteTopicInfo(Integer id);

    List<TopicGoodsListVo> getTopicGoodsInfo(Integer[] arr);
}
