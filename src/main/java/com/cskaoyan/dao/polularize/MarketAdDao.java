package com.cskaoyan.dao.polularize;

import com.cskaoyan.model.admin.ad.bo.AdListBO;
import com.cskaoyan.model.admin.ad.bo.AdUpdateBo;
import com.cskaoyan.model.admin.pojo.MarketAdPojo;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MarketAdDao {
    List<MarketAdPojo> getMarketAdInfo(AdListBO list);

    void addNewAdInfo(MarketAdPojo pojo);

    void updateAdInfo(AdUpdateBo bo);
}
