package com.cskaoyan.dao.polularize;

import com.cskaoyan.model.BaseParam;
import com.cskaoyan.model.admin.marketbeans.bo.*;
import com.cskaoyan.model.admin.marketbeans.pojo.AddCategory;
import com.cskaoyan.model.admin.marketbeans.bo.MarketAftersaleListBo;
import com.cskaoyan.model.admin.marketbeans.bo.MarketKeywordListBo;
import com.cskaoyan.model.admin.marketbeans.bo.MarketOrderListBo;
import com.cskaoyan.model.admin.marketbeans.pojo.MarketAftersale;
import com.cskaoyan.model.admin.marketbeans.pojo.UpdateBrand;
import com.cskaoyan.model.admin.marketbeans.vo.*;
import com.cskaoyan.model.admin.marketbeans.pojo.MarketIssue;
import com.cskaoyan.model.admin.marketbeans.pojo.MarketKeyword;
import com.cskaoyan.model.admin.marketbeans.pojo.MarketOrder;
import com.cskaoyan.model.admin.marketbeans.vo.Regionvo.ProvinceDto;
import com.cskaoyan.model.admin.marketbeans.vo.BrandAddVo;
import com.cskaoyan.model.admin.marketbeans.vo.brandmsgvo.BrandMsgDto;
import com.cskaoyan.model.admin.marketbeans.bo.OrderShipBo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @package: com.cskaoyan.dao
 * @Description: 商场管理
 * @author: 北青
 * @date: 2022/1/6 21:46
 */
@Repository
public interface MarketDao {

    /**
     * 售后模块获取售后信息
     *
     * @param marketAftersaleListBO
     * @return
     */
    List<MarketAftersale> getMarketAftersaleListInfo(@Param("aftersale") MarketAftersaleListBo marketAftersaleListBO);

    /**
     * 根据提供的售后信息id和状态码修改售后信息状态
     *
     * @param ids:    要修改的售后信息id数组
     * @param status: 修改之后的状态码
     */
    void changeAftersaleStatus(@Param("ids") Integer[] ids, @Param("status") Integer status);

    /**
     * 从数据库中获取通用问题信息
     *
     * @param sort
     * @param order
     * @return
     */
    List<MarketIssue> getIssueListInfo(@Param("question") String question, @Param("sort") String sort, @Param("order") String order);

    /**
     * 通用问题模块——新增问题
     *
     * @param marketIssue
     */
    void createIssue(@Param("issue") MarketIssue marketIssue);

    /**
     * 通用问题模块——编辑修改问题
     *
     * @param marketIssue
     */
    void updateIssue(@Param("issue") MarketIssue marketIssue);


    /**
     * @createTime: 2022/1/7 20:53
     * @author: Dragon
     * @description: 商城管理模块-行政区域查询全部信息Dao接口
     */
    List<ProvinceDto> getAllRegionList();

    /**
     * @createTime: 2022/1/7 20:53
     * @author: Dragon
     * @description: 商城管理模块-品牌制造商查询全部信息Dao接口
     */
    List<BrandMsgDto> getAllBrandMsgList(@Param("BrandMsg") BaseParam baseParam, @Param("id") String id, @Param("name") String name);


    void updateBrand(@Param("updateBrand") UpdateBrand updateBrand);

    int deleteBrand(Integer id);

    List<CategoryListDto> selectAllCategory();

    int selectCountCategory();

    List<GetLevelFirstChirldCategoryDto> selectLevel1Category();

    int createCategory(@Param("addCategory") AddCategory addCategory);

    int deleteCategory(Integer id);


    /**
     * @createTime: 2022/1/7 20:53
     * @author: Dragon
     * @description: 商城管理模块-品牌制造商-添加品牌Dao接口-创建品牌
     */
    int createNewBrand(@Param("addBrand") AddBrandBo addBrandBo);

    /**
     * @createTime: 2022/1/7 20:53
     * @author: Dragon
     * @description: 商城管理模块-品牌制造商-添加品牌Dao接口-创建品牌后查询所有信息
     */
    BrandAddVo selectBrandAfterCreate(Integer id);


    /**
     * 商城——关键词——关键词信息显示
     *
     * @param marketKeywordListBo
     * @return
     */
    List<MarketKeyword> getMarketKeywordInfo(@Param("keywordBo") MarketKeywordListBo marketKeywordListBo);

    /**
     * createKeyword
     *
     * @param marketKeyword
     */
    void createKeyword(@Param("keyword") MarketKeyword marketKeyword);

    /**
     * 商城——关键词——修改关键词信息
     *
     * @param marketKeyword
     */
    void updateKeyword(@Param("keyword") MarketKeyword marketKeyword);

    void updateOutCategory(@Param("updateCategory") UpdateCategoryBo updateCategoryBo);

    /*
        void updateInnerCaregory(@Param("list") List<Integer> list, @Param("pId") Integer pId);
    */
    void updateInnerCaregory(@Param("list") List<UpdateCategoryChilrenDto> list, @Param("pid") Integer pid);

    /**
     * 商城——订单管理——订单信息显示
     *
     * @param marketOrderListBo
     * @return
     */
    List<MarketOrder> getOrderListInfo(@Param("order") MarketOrderListBo marketOrderListBo);

    /**
     * 获取快递公司信息
     *
     * @return
     */
    List<MarketChannelVo> getChannel();

    void shipOrderMsg(@Param("orderShipBo") OrderShipBo orderShipBo);

    /**
     * 订单详情——查询订单信息
     *
     * @param orderId
     * @return
     */
    MarketOrderVo getOrderDetailByOrderId(@Param("id") Integer orderId);

    /**
     * 订单详情——查询订单中商品详情
     *
     * @param orderId
     * @return
     */
    List<MarketOrderGoodsD> getOrderDetailGoods(@Param("id") Integer orderId);

    /**
     * 订单详情——查询订单中用户信息
     *
     * @param userId
     * @return
     */
    MarketOrderUserD getOrderDetailUser(@Param("id") Integer userId);

    /**
     * 订单管理——删除订单
     *
     * @param orderId
     */
    void deleteOrder(@Param("orderId") Integer orderId);

    /**
     * 售后管理——退款——修改订单状态
     *
     * @param orderId
     * @param orderStatus
     */
    void changeOrderStatus(@Param("orderId") Integer orderId, @Param("orderStatus") Integer orderStatus);

    /**
     * 管理员通过售后请求之后修改订单售后信息
     *
     * @param status
     * @param orderId
     */
    void updateOrderAfterasleStatus(@Param("status") Integer status, @Param("orderId") Integer orderId);

    /**
     * 根据售后ID获取订单ID
     *
     * @param ids
     * @return
     */
    List<Integer> getOrderIdsByAftersaleId(@Param("ids") Integer[] ids);
}
