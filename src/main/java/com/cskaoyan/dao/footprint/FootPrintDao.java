package com.cskaoyan.dao.footprint;

import com.cskaoyan.model.wx.footprint.vo.FootPrintListDataVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FootPrintDao {

    List<FootPrintListDataVo> getGoodsInfo(int id);

    int updateFootPrintInfo(@Param("userId") Integer userId, @Param("goodsId") Integer goodsId);

    void addNewFootPrintInfo(@Param("userId") Integer userId, @Param("goodsId") Integer goodsId);

    void deleteFootPrint(Integer footId);

}
