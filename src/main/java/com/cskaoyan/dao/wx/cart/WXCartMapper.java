package com.cskaoyan.dao.wx.cart;

import com.cskaoyan.model.wx.cart.*;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

/**
 * @Author:
 * @description:
 * @keypoint:
 * @tags:
 * @related:
 */
@Repository
public interface WXCartMapper {

    Integer selectCountGoodsNumByUserID(Integer id);

    Cart selectGoodsProductInfoByGoodsIdAndProductId(@Param("param") GoodsAndProductAndNumber goodsAndProductAndNumber);

    Integer insertCart(Cart goodsProductInfo);

    /**
     * 如果有userId或者cartId那么筛选出是这个userId或cartId的cart信息
     *
     * @param userId
     * @param cartId
     * @return
     */
    List<Cart> selectAllCartInfo(@Param("userId") Integer userId, @Param("cartId") Integer cartId);

    Integer updateCartInfoById(CartUpdateBO cartUpdateBO);

    Integer updateCartCheckedByProductIds(ProductIdsAndChecked productIdsAndChecked);

    Integer deleteCartByProductIds(@Param("productIds") List<Integer> productIds, @Param("userId") Integer userId);

    BigDecimal selectDiscountFromMarketCouponByCouponId(Integer couponId);

    Integer updateCouponStatusByUserCouponId(Integer userCouponId);

    Integer selectCountCouponByUserId(Integer userId);

    BigDecimal selectDiscountFromMarketGrouponRulesByGroupRulesId(Integer grouponRulesId);

    Integer updateCartAfterCheckOut(@Param("userId") Integer userId, @Param("cartId") Integer cartId);

    Integer tryUpdateProductNumber(@Param("productId") Integer productId, @Param("number") Integer number, @Param("userId") Integer userId);

    String selectFreightPrice();

    String selectMinNeedFreightPrice();
}
