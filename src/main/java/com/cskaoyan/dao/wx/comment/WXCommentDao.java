package com.cskaoyan.dao.wx.comment;

import com.cskaoyan.model.wx.comment.bo.CommentCountBO;
import com.cskaoyan.model.wx.comment.vo.CommentListContentUserVO;
import com.cskaoyan.model.wx.comment.vo.CommentListContentVO;
import com.cskaoyan.model.wx.comment.vo.CommentPostVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface WXCommentDao {

    //查找评论的总数量
    Integer selectCommentListTotal(Integer valueId);

    //查询详细的数据信息
    List<CommentListContentVO> selectCommentListContent(Integer valueId);

    //查询user表中的信息
    CommentListContentUserVO selectCommentListUserContent(Integer userId);

    //查询评论的总数量
    Integer selectCommentAllCount(@Param("count") CommentCountBO commentCountBO);

    //查询有图片的评论数量
    Integer selectCommentHasPicCount(@Param("count") CommentCountBO commentCountBO);

    Integer insertCommentPost(@Param("post") CommentPostVO commentPostVO);
}
