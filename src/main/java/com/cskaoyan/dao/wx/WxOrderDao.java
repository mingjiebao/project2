package com.cskaoyan.dao.wx;

import com.cskaoyan.model.admin.goodsbean.Comment;
import com.cskaoyan.model.admin.marketbeans.pojo.MarketAftersale;
import com.cskaoyan.model.admin.goodsbean.OrderGoods;
import com.cskaoyan.model.admin.marketbeans.pojo.MarketOrder;
import com.cskaoyan.model.wx.order.vo.*;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @package: com.cskaoyan.dao.wx
 * @Description: 微信小程序订单模块
 * @author: 北青
 * @date: 2022/1/10 15:12
 */
public interface WxOrderDao {
    /**
     * 根据用户id 查询用户所有订单信息
     *
     * @param userId
     * @return
     */
    List<WxOrderVo> getWxOrderList(@Param("userId") Integer userId, @Param("orderStatus") Integer orderStatus);

    /**
     * 根据订单id获得订单商品详情
     *
     * @param orderId
     * @return
     */
    List<WxOrderGoodsVo> getWxOrderGoodsList(@Param("orderId") Integer orderId);

    /**
     * 根据订单状态获取订单可以操作选项
     *
     * @param orderStatus
     * @return
     */
    HandleOptionVo getHandleOption(@Param("orderStatus") Integer orderStatus);

    /**
     * 获取订单详细信息通过订单ID
     *
     * @param orderId
     * @return
     */
    WxOrderDetailVo getOrderDetailByOrderId(@Param("orderId") Integer orderId);

    /**
     * 订单详情中商品信息获取
     *
     * @param orderId
     * @return
     */
    List<WxOrderDetailGoodsVo> getOrderDetaiGoodsByOrderId(@Param("orderId") Integer orderId);

    /**
     * 插入 market_order表
     *
     * @param orderInsertBean
     * @return
     */
    Integer insertNewOrder(MarketOrder orderInsertBean);

    /**
     * 插入market_order_goods表，单个插入
     *
     * @param orderGoods
     * @return
     */
    Integer insertMarketOrderGoodsSingle(OrderGoods orderGoods);

    /**
     * 插入market_order_goods表 批量插入
     *
     * @param orderGoodsList
     * @return
     */
    Integer insertMarketOrderGoodsBatch(@Param("orderGoodsList") List<OrderGoods> orderGoodsList);

    /**
     * 取消订单:将订单状态码修改成102
     *
     * @param orderStatus
     */
    void changeOrderStatus(@Param("orderStatus") Integer orderStatus, @Param("orderId") Integer orderId);

    /**
     * 删除订单
     *
     * @param orderId
     */
    void deleteOrder(@Param("orderId") Integer orderId);

    /**
     * 取消订单或者退款等操作之后,将订单中商品返回到库存
     *
     * @param productId
     * @param number
     */
    void returnGoods(@Param("productId") Integer productId, @Param("number") Integer number);

    /**
     * 新增售后信息
     *
     * @param marketAftersale
     */
    void insertAftersale(MarketAftersale marketAftersale);

    /**
     * 新增商品评价
     *
     * @param comment
     */
    void insertComment(Comment comment);

    /**
     * 将评论id插入orderGoods表中
     *
     * @param id
     * @param goodsId
     */
    void insertCommentIdIntoOrderGoods(@Param("id") Integer id, @Param("goodsId") Integer goodsId);

    /**
     * 退款是修改订单状态和订单售后状态
     *
     * @param orderStatus
     * @param aftersaleStatus
     * @param orderId
     */
    void changeOrderStatusAndAftersaleStatus(@Param("orderStatus") Integer orderStatus, @Param("aftersaleStatus") Integer aftersaleStatus, @Param("orderId") Integer orderId);

    /**
     * 修改订单待评价商品数
     *
     * @param orderId
     */
    void setComments(@Param("orderId") Integer orderId);

    /**
     * 查询订单待评价数
     *
     * @param orderId
     */
    int getComments(Integer orderId);

    /**
     * 订单参数更新
     *
     * @param marketOrder
     */
    void updateOrderValues(MarketOrder marketOrder);

    /**
     * 根据用户id 修改所有过期未付款订单
     *
     * @param orderStatus
     * @param time
     * @param userId
     */
    void changeOrderStatusByUserId(@Param("orderStatus") Integer orderStatus, @Param("time") String time, @Param("userId") Integer userId);

    /**
     * 修改所有未评价的商品中更新时间超过7天的商品可评价状态为-1
     *
     * @param endtime
     */
    void updateOrderGoodsComment(@Param("endtime") String endtime);

    /**
     * 小程序个人中心获取售后信息显示
     *
     * @param userId
     * @param status
     * @return
     */
    List<Integer> getAftersaleOrders(@Param("userId") Integer userId, @Param("status") Integer status);

    /**
     * 小程序个人中心获取售后信息显示
     *
     * @param orderId
     * @param status
     * @return
     */
    MarketAftersale getAftersale(@Param("orderId") Integer orderId, @Param("status") Integer status);
}
