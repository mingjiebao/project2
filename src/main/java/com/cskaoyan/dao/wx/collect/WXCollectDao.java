package com.cskaoyan.dao.wx.collect;

import com.cskaoyan.model.wx.wxcollect.bo.UpdateDeleted;
import com.cskaoyan.model.wx.wxcollect.vo.WXCollectListVo;
import com.cskaoyan.model.wx.wxcollect.bo.CollectAddorDeleteBo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @description:
 * @author: XM
 * @date: 2022/1/10 15:02
 */
public interface WXCollectDao {


    Integer selectUserId(String username);

    List<WXCollectListVo.ListBean> selectCollectList(@Param("type") Integer type, @Param("id") Integer id);


    UpdateDeleted selectDelectedStatus(@Param("AddorDelete") CollectAddorDeleteBo collectAddorDeleteBo, @Param("id") Integer id);


    Integer updateDeleted(@Param("Deleted") UpdateDeleted updateDeleted);


    Integer insertCollect(@Param("collectAddorDeleteBo") CollectAddorDeleteBo collectAddorDeleteBo, @Param("id") Integer id);

}
