package com.cskaoyan.dao.wx.adress;

import com.cskaoyan.model.wx.wxadressbean.bo.WXAddressSaveBo;
import com.cskaoyan.model.wx.wxadressbean.vo.WXAddressDetailVo;
import com.cskaoyan.model.wx.wxadressbean.vo.WXAdressList;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @description:
 * @author: XM
 * @date: 2022/1/10 20:17
 */

@Repository
public interface WXAddressDao {
    Integer selectCount();

    List<WXAdressList.ListBean> selectAddressAll();

    WXAddressDetailVo selectAddressDetail(Integer id);

    Integer updateAdress(@Param("addressSave") WXAddressSaveBo addressSaveBo);

    Integer deleteAdress(Integer id);

    Integer selectAddressData(Integer id);


    Integer insertAddress(@Param("addressSaveBo") WXAddressSaveBo addressSaveBo, @Param("userId") Integer userId);

    void updateAddressDefault();

}