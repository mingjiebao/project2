package com.cskaoyan.dao.wx.homemanage;


import com.cskaoyan.model.wx.wxhomeindexbean.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @description:
 * @author: XM
 * @date: 2022/1/11 4:00
 */
@Repository
public interface WXHomeIndexDao {
    List<WXActivityVo> selectActivity();

    List<WXBrandListVo> selectBrand(Integer limit);

    List<WXChannelVo> selectChannel();

    List<WXCouponListVo> selectCoupon();

    List<WXHotGoodsListVo> selectHotGoods(Integer limit);

    List<WXNewGoodsListVo> selectWXNewGoodsList(Integer limit);

    List<WXFloorGoodsListVo> selectWXFloorGoodsList(Integer limit);

    List<WXTopicListVo> selectTopic(Integer limit);

}