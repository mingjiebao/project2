package com.cskaoyan.dao.wx.homemanage;

import com.cskaoyan.model.wx.wxindexbean.LoginIndexDto;
import com.cskaoyan.model.wx.wxindexbean.LoginIndexVo;
import com.cskaoyan.model.wx.wxuserdetailbean.*;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface WXUserDao {
    LoginIndexDto getIndexMsg();


    Integer selectUserId(String username);

    Integer getUnrecv(Integer id);

    Integer getUncomment(Integer id);

    Integer getunpaid(Integer id);

    Integer getunship(Integer id);

    List<SpecificationListVo> getSpecificationList(Integer id);

    List<WXDetailAttrubuteVo> getDetailAttribute(Integer id);

    WXDetailGoodsVo getDetailGoods(Integer id);

    List<WXDetailIssueVo> getDetailIssue();

    List<WXDetailProductVo> getDetailProduct(Integer id);

    List<WXUserCommentsVo.DataBean> getDetailComments(Integer id);

    Integer getDetailCommentsCount(Integer id);

    WXUserDetailBrandVo getDetailBrand(Integer id);


    Integer getHasCollect(@Param("id") Integer id, @Param("userId") Integer userId);

}
