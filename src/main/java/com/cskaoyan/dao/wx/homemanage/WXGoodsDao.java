package com.cskaoyan.dao.wx.homemanage;

import com.cskaoyan.model.wx.wxgoodsbean.bo.WxGoodsListBo;
import com.cskaoyan.model.wx.wxgoodsbean.vo.WXGoodsCategoryVo;
import com.cskaoyan.model.wx.wxgoodsbean.vo.WXGoodsListVo;
import com.cskaoyan.model.wx.wxgoodsbean.vo.WXGoodsRelatedVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WXGoodsDao {
    WXGoodsCategoryVo.CurrentCategoryBean selectCurrentCategory(Integer id);

    WXGoodsCategoryVo.ParentCategoryBean selectParentCategory(Integer pid);

    List<WXGoodsCategoryVo.BrotherCategoryBean> selectBrotherCategory(Integer pid);

    List<WXGoodsListVo.ListBean> selectGoodsByCategoryId(@Param("brandId") Integer brandId, @Param("categoryId") Integer categoryId);

    List<WXGoodsListVo.FilterCategoryListBean> selectCategoryAll();

    List<WXGoodsRelatedVo.ListBean> selectGoodsRelated(Integer id);

    Integer selectRelatedCount(Integer id);

    Integer selectGoodsCount();

    List<Integer> getKeywordGoodsId(String keyword);

    List<WXGoodsListVo.ListBean> selectGoodsByKeword(List<Integer> list);

    List<WXGoodsListVo.ListBean> selectHotOrNew(WxGoodsListBo bo);
}
