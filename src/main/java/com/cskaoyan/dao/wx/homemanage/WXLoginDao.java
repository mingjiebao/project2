package com.cskaoyan.dao.wx.homemanage;

import com.cskaoyan.model.wx.wxhomeindexbean.WXRegisterBo;
import com.cskaoyan.model.wx.wxloginbean.UserInfo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface WXLoginDao {
    UserInfo selectUserExist(@Param("username") String username, @Param("password") String password);

    Integer selectId(@Param("username2") String username, @Param("password2") String password);

    String selectPassword(@Param("principal") String principal);

    Integer selectUserIdByUsername(String principal);

    int insertUser(@Param("wxRegister") WXRegisterBo wxRegisterBo);
}
