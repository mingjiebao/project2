package com.cskaoyan.dao.wx.special;

import com.cskaoyan.model.wx.special.vo.SpecialDetailGoodsVO;
import com.cskaoyan.model.wx.special.vo.SpecialDetailTopicVO;
import com.cskaoyan.model.wx.special.vo.SpecialListContentVO;
import com.cskaoyan.model.wx.special.vo.SpecialRelatedContentVO;

import java.util.List;

public interface WXSpecialDao {

    //list接口
    //查询数据库表中List需要字段的信息
    List<SpecialListContentVO> selectSpecialListContent();

    //查询数据库表中数据的数量
    Integer selectSpecialListTotal();

    //detail接口
    //查询market_topic表中需要数据
    SpecialDetailTopicVO selectSpecialDetailTopic(Integer id);

    //查询market_goods表中需要数据
    SpecialDetailGoodsVO selectSpecialDetailGoods(String goodsSn);

    //related接口
    //查询market_topic表中相关商品goods
    String selectSpecialRelatedGoodsId(Integer id);

    //通过id查询market_topic表中需要数据
    SpecialRelatedContentVO selectSpecialRelatedContent(Integer relatedId);
}
