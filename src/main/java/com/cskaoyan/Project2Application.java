package com.cskaoyan;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@MapperScan("com.cskaoyan.dao")
@SpringBootApplication
@MapperScan(basePackages = {"com.cskaoyan.dao"})
public class Project2Application {

    public static void main(String[] args) {

        SpringApplication.run(Project2Application.class, args);
    }

}
