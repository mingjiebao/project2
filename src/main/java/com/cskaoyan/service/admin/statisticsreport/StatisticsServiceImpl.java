package com.cskaoyan.service.admin.statisticsreport;

import com.cskaoyan.dao.statisticsreport.StatisticsDao;
import com.cskaoyan.model.admin.statisticsreport.vo.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

@Service
@Transactional
public class StatisticsServiceImpl implements StatisticsService {

    @Autowired
    StatisticsDao statisticsDao;

    @Override
    //用户统计
    public UserStatisticsVO userStatistics() {
        //新建javabean，用以封装数据
        UserStatisticsVO userStatisticsVO = new UserStatisticsVO();
        List<UserStatisticsRowVO> rows = new ArrayList<>();
        //封装columns
        List<String> columns = new ArrayList<>();
        columns.add("day");
        columns.add("users");
        userStatisticsVO.setColumns(columns);
        //查询用户加入的日期
        List<String> dates = statisticsDao.selectUserAddTime();
        //处理返回的数据，变为需要的格式
        ListIterator<String> iterator = dates.listIterator();
        while (iterator.hasNext()) {
            String str = iterator.next();
            String date = str.substring(0, 10);
            //通过时间查询出人数
            Integer count = statisticsDao.selectUserCountByAddTime(date);
            //封装数据到javabean
            UserStatisticsRowVO userStatisticsRowVO = new UserStatisticsRowVO();
            userStatisticsRowVO.setDay(date);
            userStatisticsRowVO.setUsers(count);
            rows.add(userStatisticsRowVO);
        }
        //封装返回的数据
        userStatisticsVO.setRows(rows);
        return userStatisticsVO;
    }

    @Override
    //订单统计
    public OrderStatisticsVO orderStatistics() {
        //新建javabean，用以封装数据
        OrderStatisticsVO orderStatisticsVO = new OrderStatisticsVO();
        List<OrderStatisticsRowVO> rows = new ArrayList<>();
        //封装columns
        List<String> columns = new ArrayList<>();
        columns.add("day");
        columns.add("orders");
        columns.add("customers");
        columns.add("amount");
        columns.add("pcr");
        orderStatisticsVO.setColumns(columns);
        //查询用户加入的日期
        List<String> dates = statisticsDao.selectOrderAddTime();
        //处理返回的数据，变为需要的格式
        ListIterator<String> iterator = dates.listIterator();
        while (iterator.hasNext()) {
            String str = iterator.next();
            String date = str.substring(0, 10);
            //通过时间查询出其余字段
            OrderStatisticsRowVO orderStatisticsRowVO = statisticsDao.selectOrderByAddTime(date);
            //计算客单价并封装数据到javabean
            BigDecimal pcr = orderStatisticsRowVO.getAmount().divide(BigDecimal.valueOf(orderStatisticsRowVO.getCustomers()));
            orderStatisticsRowVO.setPcr(pcr);
            orderStatisticsRowVO.setDay(date);
            rows.add(orderStatisticsRowVO);
        }
        //封装返回的数据
        orderStatisticsVO.setRows(rows);
        return orderStatisticsVO;
    }

    @Override
    //商品统计
    public GoodsStatisticsVO goodsStatistics() {
        //新建javabean，用以封装数据
        GoodsStatisticsVO goodsStatisticsVO = new GoodsStatisticsVO();
        List<GoodsStatisticsRowVO> rows = new ArrayList<>();
        //封装columns
        List<String> columns = new ArrayList<>();
        columns.add("day");
        columns.add("orders");
        columns.add("products");
        columns.add("amount");
        goodsStatisticsVO.setColumns(columns);
        //查询用户加入的日期
        List<String> dates = statisticsDao.selectGoodsAddTime();
        //处理返回的数据，变为需要的格式
        ListIterator<String> iterator = dates.listIterator();
        while (iterator.hasNext()) {
            String str = iterator.next();
            String date = str.substring(0, 10);
            //通过时间查询出其余字段
            GoodsStatisticsRowVO goodsStatisticsRowVO = statisticsDao.selectGoodsByAddTime(date);
            goodsStatisticsRowVO.setDay(date);
            rows.add(goodsStatisticsRowVO);
        }
        //封装返回的数据
        goodsStatisticsVO.setRows(rows);
        return goodsStatisticsVO;
    }
}
