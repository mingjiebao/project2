package com.cskaoyan.service.admin.statisticsreport;

import com.cskaoyan.model.admin.statisticsreport.vo.GoodsStatisticsVO;
import com.cskaoyan.model.admin.statisticsreport.vo.OrderStatisticsVO;
import com.cskaoyan.model.admin.statisticsreport.vo.UserStatisticsVO;

public interface StatisticsService {

    //用户统计
    UserStatisticsVO userStatistics();

    //订单统计
    OrderStatisticsVO orderStatistics();

    //商品统计
    GoodsStatisticsVO goodsStatistics();
}
