package com.cskaoyan.service.admin.usermanage;

import com.cskaoyan.dao.user.UserDao;
import com.cskaoyan.model.admin.user.param.BaseParam;
import com.cskaoyan.model.admin.user.userbeans.*;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @description:首页显示
 * @author: XM
 * @date: 2022/1/6 22:00
 */
@Component
public class UserSereviceImpl implements UserService {
    @Autowired
    UserDao userDao;
    @Override
    public DashHoardVo dashboard() {
        /*
         * @createTime  2022/1/7 15:12
         * @description:查询用户表、订单表、商品表、货品表个数，返回DashHoardVO类型
         */
        Integer goodsCount=userDao.selectGoodsCount();
        Integer userCount=userDao.selectUserCount();
        Integer productCount=userDao.selectProductCount();
        Integer orderCount=userDao.selectOrderCount();
        DashHoardVo dashHoardVO = new DashHoardVo(goodsCount, userCount,productCount,orderCount);
        return dashHoardVO;
    }


    /*
     * @createTime  2022/1/7 15:36
     * @description:会员管理(搜索都是用的模糊查找)
     * @param
     * @return */
    @Override
   public UserData adminUserList(String username, String mobile, BaseParam param) {
        Integer page = param.getPage();
        Integer limit = param.getLimit();
        String sort = param.getSort();
        String order = param.getOrder();
        PageHelper.startPage(page,limit);
        List<UserListVo> list =userDao.selectUserList(username,mobile,sort,order);
        PageInfo<UserListVo> pageInfo = new PageInfo<>(list);
        long total=pageInfo.getTotal();
        int pages = pageInfo.getPages();
        UserData data = UserData.data(total, pages, limit, page, list);
        return data;
    }



    /*
     * @createTime  2022/1/7 20:19
     * @description:按id查找
     * @param
     * @return */
    @Override
    public UserListVo userDetail(String id) {
       UserListVo userListVO= userDao.selectUserListById(id);
        return userListVO;
    }


    /*未完成，修改用户信息*/
    @Override
    public Integer userUpdate(User user) {
        Integer number=userDao.userUpdate(user);
        return number;
    }


    /*
     * @createTime  2022/1/7 20:20
     * @description:收货地址
     * @param
     * @return */
    @Override
    public UserData addressList(String name, String  userId, BaseParam param) {
        Integer page = param.getPage();
        Integer limit = param.getLimit();
        String sort = param.getSort();
        String order = param.getOrder();
        PageHelper.startPage(page,limit);

        List<UserAddressList> lists=userDao.selectUserAddressList(name,userId,sort,order);

        PageInfo<UserAddressList> pageInfo = new PageInfo<>(lists);


        long total = pageInfo.getTotal();
        int pages = pageInfo.getPages();
        UserData data = UserData.data(total, pages, limit, page, lists);
        return data;
    }


    /*
     * @createTime  2022/1/7 21:07
     * @description:会员收藏（按userId、valueId模糊查询）
     * @param
     * @return */
    @Override
    public UserData collectList(String userId, String valueId, BaseParam param) {
        Integer page = param.getPage();
        Integer limit = param.getLimit();
        String sort = param.getSort();
        String order = param.getOrder();
        PageHelper.startPage(page,limit);
        List<UserCollect> lists=userDao.selectUserCollect(userId,valueId,sort,order);
        PageInfo<UserCollect> pageInfo = new PageInfo<>(lists);
        long total = pageInfo.getTotal();
        int pages = pageInfo.getPages();
        UserData data = UserData.data(total, pages, limit, page, lists);
        return data;
    }


    /*
     * @createTime  2022/1/7 22:41
     * @description:会员足迹（按userId、goodsid查找）
     * @param
     * @return */
    @Override
    public UserData footPrintList(String userId, String goodsId, BaseParam param) {
        Integer page = param.getPage();
        Integer limit = param.getLimit();
        String sort = param.getSort();
        String order = param.getOrder();
        PageHelper.startPage(page,limit);
        List<UserFontPrint> lists= userDao.selectFootPrint(userId,goodsId,sort,order);
        PageInfo<UserFontPrint> pageInfo = new PageInfo<>(lists);
        long total = pageInfo.getTotal();
        int pages = pageInfo.getPages();
        UserData data = UserData.data(total, pages, limit, page, lists);
        return data;
    }


    /*
     * @createTime  2022/1/8 9:21
     * @description:搜索历史（可以按userid、keyWord查询）
     * @param
     * @return */
    @Override
    public UserData adminHistory(String userId, String keyword, BaseParam param) {
        Integer page = param.getPage();
        Integer limit = param.getLimit();
        String sort = param.getSort();
        String order = param.getOrder();
        PageHelper.startPage(page,limit);
        List<AdminHistory> lists=userDao.selectAdminHistory(userId,keyword,sort,order);
        PageInfo<AdminHistory> pageInfo = new PageInfo<>(lists);
        int pages = pageInfo.getPages();
        long total = pageInfo.getTotal();
        UserData data = UserData.data(total, pages, limit, page, lists);
        return data;
    }

    @Override
    public UserData feedbackList(String username, String id, BaseParam param) {
        Integer page = param.getPage();
        Integer limit = param.getLimit();
        String sort = param.getSort();
        String order = param.getOrder();
        PageHelper.startPage(page,limit);
        List<UserFeekbackList> lists=userDao.selectFeedbackList(username,id,sort,order);

        PageInfo<UserFeekbackList> pageInfo = new PageInfo<>(lists);
        long total = pageInfo.getTotal();
        int pages = pageInfo.getPages();
        UserData data = UserData.data(total, pages, limit, page, lists);
        return data;
    }


}