package com.cskaoyan.service.admin.usermanage;

import com.cskaoyan.model.admin.user.param.BaseParam;
import com.cskaoyan.model.admin.user.userbeans.DashHoardVo;
import com.cskaoyan.model.admin.user.userbeans.User;
import com.cskaoyan.model.admin.user.userbeans.UserData;
import com.cskaoyan.model.admin.user.userbeans.UserListVo;

/**
 * @description:
 * @author: XM
 * @date: 2022/1/6 22:00
 */
public interface UserService {

    DashHoardVo dashboard();

    UserData adminUserList(String username, String mobile, BaseParam param);

    UserListVo userDetail(String id);

    Integer userUpdate(User user);

    UserData addressList(String name, String userId, BaseParam param);

    UserData collectList(String userId, String valueId, BaseParam param);

    UserData footPrintList(String userId, String goodsId, BaseParam param);

    UserData adminHistory(String userId, String keyword, BaseParam param);

    UserData feedbackList(String username, String id, BaseParam param);
}