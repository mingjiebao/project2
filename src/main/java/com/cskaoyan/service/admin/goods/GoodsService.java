package com.cskaoyan.service.admin.goods;


import com.cskaoyan.model.admin.goodsbean.Goods;
import com.cskaoyan.model.admin.goodsbean.bo.GoodsCreateBo;
import com.cskaoyan.model.admin.goodsbean.bo.GoodsUpdateBo;
import com.cskaoyan.model.admin.goodsbean.bo.list.GoodsListBO;
import com.cskaoyan.model.admin.goodsbean.vo.CatAndBrandVo;
import com.cskaoyan.model.admin.goodsbean.vo.GoodsDataVO;
import com.cskaoyan.model.admin.goodsbean.vo.GoodsDetailVo;

/**
 * @Author AhaNg
 * @Date 2022/1/7 17:32
 * @description:
 * @return:
 */
public interface GoodsService {

    //商品模块查询所有商品信息分页显示
     GoodsDataVO allGoodsList(GoodsListBO goodsListBO);
     //商品品牌和类目的查询
    CatAndBrandVo CatAndBrand();
    //创建商品
    void create(GoodsCreateBo goodsCreateBo);
    //显示商品细节
    GoodsDetailVo detail(Integer id);
    //更新商品信息
    void update(GoodsUpdateBo goodsUpdateBo);
    //删除商品
    void delete(Goods goods);

}
