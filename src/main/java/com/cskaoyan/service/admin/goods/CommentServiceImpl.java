package com.cskaoyan.service.admin.goods;

import com.cskaoyan.dao.goods.CommentMapper;
import com.cskaoyan.model.BaseData;
import com.cskaoyan.model.BaseParam;
import com.cskaoyan.model.admin.goodsbean.Comment;
import com.cskaoyan.model.admin.goodsbean.CommentExample;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Author AhaNg
 * @Date 2022/1/8 23:41
 * @description:
 * @return:
 */
@Service
@Transactional
public class CommentServiceImpl implements CommentService {

    @Autowired
    CommentMapper commentMapper;
    /**
     * @Author: AhaNg
     * @description: 商品管理中商品评论页面的显示
     * @return:
     */
    @Override
    public BaseData list(Integer userId, Integer valueId, BaseParam param) {
        Integer page = param.getPage();
        Integer limit = param.getLimit();
        PageHelper.startPage(page, limit);

        CommentExample commentExample = new CommentExample();
        CommentExample.Criteria criteria = commentExample.createCriteria();
        if (param.getOrder() != null && param.getSort() != null) {
            commentExample.setOrderByClause(param.getSort() + " " + param.getOrder());
        }
        if (userId != null) {
            criteria.andUserIdEqualTo(userId);
        }
        if (valueId != null) {
            criteria.andValueIdEqualTo(valueId);
        }
        List<Comment> comments = commentMapper.selectByExample(commentExample);

        PageInfo info =new PageInfo(comments);
        long total = info.getTotal();
        int pages = info.getPages();
        return BaseData.list(comments,total,pages,limit,page);
    }

    /**
     * @Author: AhaNg
     * @description: 商品管理中商品评论的删除
     * @return:
     */
    @Override
    public void delete(Comment comment) {
        commentMapper.deleteByPrimaryKey(comment.getId());
    }
}
