package com.cskaoyan.service.admin.goods;

import com.cskaoyan.model.BaseData;
import com.cskaoyan.model.BaseParam;
import com.cskaoyan.model.admin.goodsbean.Comment;

/**
 * @Author AhaNg
 * @Date 2022/1/8 23:39
 * @description:
 * @return:
 */
public interface CommentService {

    BaseData list(Integer userId, Integer valueId, BaseParam param);

    void delete(Comment comment);
}
