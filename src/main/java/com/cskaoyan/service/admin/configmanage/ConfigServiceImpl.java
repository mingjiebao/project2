package com.cskaoyan.service.admin.configmanage;

import com.cskaoyan.dao.configmanage.ConfigDao;
import com.cskaoyan.model.admin.configmanage.bo.ConfigExpressBO;
import com.cskaoyan.model.admin.configmanage.bo.ConfigOrderBO;
import com.cskaoyan.model.admin.configmanage.bo.ConfigWxBO;
import com.cskaoyan.model.admin.configmanage.pojo.SystemConfigPojo;
import com.cskaoyan.model.admin.configmanage.bo.ConfigMallBO;
import com.cskaoyan.model.admin.configmanage.vo.ConfigExpressVO;
import com.cskaoyan.model.admin.configmanage.vo.ConfigMallVO;
import com.cskaoyan.model.admin.configmanage.vo.ConfigOrderVO;
import com.cskaoyan.model.admin.configmanage.vo.ConfigWxVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@Transactional
public class ConfigServiceImpl implements ConfigService {

    @Autowired
    ConfigDao configDao;

    //商场配置
    @Override
    //商场配置 页面数据显示
    public ConfigMallVO configMallInfo() {
        //新建configMallVO作为返回的数据容器
        ConfigMallVO configMallVO = new ConfigMallVO();
        //新建list用以接收返回的数据
        List<SystemConfigPojo> list = configDao.selectMallConfig();
        ListIterator<SystemConfigPojo> iterator = list.listIterator();
        //读取出list中的数据
        while (iterator.hasNext()) {
            SystemConfigPojo systemConfig = iterator.next();
            String key = systemConfig.getConfigName();
            String value = systemConfig.getConfigValue();
            //匹配数据，赋值到configMallVO中
            if ("market_mall_address".equals(key)) {
                configMallVO.setMarket_mall_address(value);
            } else if ("market_mall_latitude".equals(key)) {
                configMallVO.setMarket_mall_latitude(value);
            } else if ("market_mall_longitude".equals(key)) {
                configMallVO.setMarket_mall_longitude(value);
            } else if ("market_mall_name".equals(key)) {
                configMallVO.setMarket_mall_name(value);
            } else if ("market_mall_phone".equals(key)) {
                configMallVO.setMarket_mall_phone(value);
            } else if ("market_mall_qq".equals(key)) {
                configMallVO.setMarket_mall_qq(value);
            }
        }
        return configMallVO;
    }

    @Override
    //商场配置 确认插入或更新配置项
    public void confirmConfigMall(ConfigMallBO configMallBO) {
        //新建list作为容器，取出configMallBO中的数据对应放入容器
        List<SystemConfigPojo> list = new ArrayList<>();
        SystemConfigPojo systemConfigPoJo1 = new SystemConfigPojo();
        SystemConfigPojo systemConfigPoJo2 = new SystemConfigPojo();
        SystemConfigPojo systemConfigPoJo3 = new SystemConfigPojo();
        SystemConfigPojo systemConfigPoJo4 = new SystemConfigPojo();
        SystemConfigPojo systemConfigPoJo5 = new SystemConfigPojo();
        SystemConfigPojo systemConfigPoJo6 = new SystemConfigPojo();
        systemConfigPoJo1.setConfigName("market_mall_name");
        systemConfigPoJo1.setConfigValue(configMallBO.getMarket_mall_name());
        systemConfigPoJo2.setConfigName("market_mall_address");
        systemConfigPoJo2.setConfigValue(configMallBO.getMarket_mall_address());
        systemConfigPoJo3.setConfigName("market_mall_latitude");
        //获得的参数小数点不足6位，补0变成6位小数精度再传给数据库回显
        StringBuilder latitude = new StringBuilder(configMallBO.getMarket_mall_latitude());
        if (latitude.toString().contains(".")) {
            String[] latitudeTemp = latitude.toString().split("\\.");
            if (latitudeTemp[1].length() < 6) {
                for (int i = 0; i < 6 - latitudeTemp[1].length(); i++) {
                    latitude.append("0");
                }
            }
        } else {
            latitude.append(".000000");
        }
        systemConfigPoJo3.setConfigValue(latitude.toString());
        systemConfigPoJo4.setConfigName("market_mall_longitude");
        //获得的参数小数点不足6位，补0变成6位小数精度再传给数据库回显
        StringBuilder longitude = new StringBuilder(configMallBO.getMarket_mall_longitude());
        if (longitude.toString().contains(".")) {
            String[] longitudeTemp = latitude.toString().split("\\.");
            if (longitudeTemp[1].length() < 6) {
                for (int i = 0; i < 6 - longitudeTemp[1].length(); i++) {
                    longitude.append("0");
                }
            }
        } else {
            longitude.append(".000000");
        }
        systemConfigPoJo4.setConfigValue(longitude.toString());
        systemConfigPoJo5.setConfigName("market_mall_phone");
        systemConfigPoJo5.setConfigValue(configMallBO.getMarket_mall_phone());
        systemConfigPoJo6.setConfigName("market_mall_qq");
        systemConfigPoJo6.setConfigValue(configMallBO.getMarket_mall_qq());
        list.add(systemConfigPoJo1);
        list.add(systemConfigPoJo2);
        list.add(systemConfigPoJo3);
        list.add(systemConfigPoJo4);
        list.add(systemConfigPoJo5);
        list.add(systemConfigPoJo6);
        //通过iterator，传入到configDao
        ListIterator<SystemConfigPojo> listIterator = list.listIterator();
        while (listIterator.hasNext()) {
            SystemConfigPojo poJo = listIterator.next();
            //查询表中对应字段的id
            Integer id = configDao.selectIdMallConfig(poJo);
            poJo.setId(id);
            if (id == null) {
                //表中没有这个数据，执行第一次插入数据操作
                Date date = new Date();
                poJo.setAddTime(date);
                poJo.setUpdateTime(date);
                poJo.setDeletedFlag(false);
                configDao.insertMallConfigById(poJo);
            } else {
                //表中已有数据，直接修改
                Date date = new Date();
                poJo.setUpdateTime(date);
                configDao.updateMallConfigById(poJo);
            }
        }
    }

    //运费配置
    @Override
    //运费配置 页面数据显示
    public ConfigExpressVO configExpressInfo() {
        //新建configExpressVO作为返回的数据容器
        ConfigExpressVO configExpressVO = new ConfigExpressVO();
        //新建list用以接收返回的数据
        List<SystemConfigPojo> list = configDao.selectExpressConfig();
        ListIterator<SystemConfigPojo> iterator = list.listIterator();
        //读取出list中的数据
        while (iterator.hasNext()) {
            SystemConfigPojo systemConfig = iterator.next();
            String key = systemConfig.getConfigName();
            String value = systemConfig.getConfigValue();
            //匹配数据，赋值到configExpressVO中
            if ("market_express_freight_min".equals(key)) {
                configExpressVO.setMarket_express_freight_min(value);
            } else if ("market_express_freight_value".equals(key)) {
                configExpressVO.setMarket_express_freight_value(value);
            }
        }
        return configExpressVO;
    }

    @Override
    //运费配置 确认插入或更新配置项
    public void confirmConfigExpress(ConfigExpressBO configExpressBO) {
        //新建list作为容器，取出configExpressBO中的数据对应放入容器
        List<SystemConfigPojo> list = new ArrayList<>();
        SystemConfigPojo systemConfigPoJo1 = new SystemConfigPojo();
        SystemConfigPojo systemConfigPoJo2 = new SystemConfigPojo();
        systemConfigPoJo1.setConfigName("market_express_freight_min");
        systemConfigPoJo1.setConfigValue(configExpressBO.getMarket_express_freight_min());
        systemConfigPoJo2.setConfigName("market_express_freight_value");
        systemConfigPoJo2.setConfigValue(configExpressBO.getMarket_express_freight_value());
        list.add(systemConfigPoJo1);
        list.add(systemConfigPoJo2);
        //通过iterator，传入到configDao
        ListIterator<SystemConfigPojo> listIterator = list.listIterator();
        while (listIterator.hasNext()) {
            SystemConfigPojo poJo = listIterator.next();
            //查询表中对应字段的id
            Integer id = configDao.selectIdExpressConfig(poJo);
            poJo.setId(id);
            if (id == null) {
                //表中没有这个数据，执行第一次插入数据操作
                Date date = new Date();
                poJo.setAddTime(date);
                poJo.setUpdateTime(date);
                poJo.setDeletedFlag(false);
                configDao.insertExpressConfigById(poJo);
            } else {
                //表中已有数据，直接修改
                Date date = new Date();
                poJo.setUpdateTime(date);
                configDao.updateExpressConfigById(poJo);
            }
        }
    }

    //订单配置
    @Override
    //订单配置 页面数据显示
    public ConfigOrderVO configOrderInfo() {
        //新建configOrderVO作为返回的数据容器
        ConfigOrderVO configOrderVO = new ConfigOrderVO();
        //新建list用以接收返回的数据
        List<SystemConfigPojo> list = configDao.selectOrderConfig();
        ListIterator<SystemConfigPojo> iterator = list.listIterator();
        //读取出list中的数据
        while (iterator.hasNext()) {
            SystemConfigPojo systemConfig = iterator.next();
            String key = systemConfig.getConfigName();
            String value = systemConfig.getConfigValue();
            //匹配数据，赋值到configOrderVO中
            if ("market_order_comment".equals(key)) {
                configOrderVO.setMarket_order_comment(value);
            } else if ("market_order_unconfirm".equals(key)) {
                configOrderVO.setMarket_order_unconfirm(value);
            } else if ("market_order_unpaid".equals(key)) {
                configOrderVO.setMarket_order_unpaid(value);
            }
        }
        return configOrderVO;
    }

    @Override
    //订单配置 确认插入或更新配置项
    public void confirmConfigOrder(ConfigOrderBO configOrderBO) {
        //新建list作为容器，取出configOrderBO中的数据对应放入容器
        List<SystemConfigPojo> list = new ArrayList<>();
        SystemConfigPojo systemConfigPoJo1 = new SystemConfigPojo();
        SystemConfigPojo systemConfigPoJo2 = new SystemConfigPojo();
        SystemConfigPojo systemConfigPoJo3 = new SystemConfigPojo();
        systemConfigPoJo1.setConfigName("market_order_comment");
        systemConfigPoJo1.setConfigValue(configOrderBO.getMarket_order_comment());
        systemConfigPoJo2.setConfigName("market_order_unconfirm");
        systemConfigPoJo2.setConfigValue(configOrderBO.getMarket_order_unconfirm());
        systemConfigPoJo3.setConfigName("market_order_unpaid");
        systemConfigPoJo3.setConfigValue(configOrderBO.getMarket_order_unpaid());
        list.add(systemConfigPoJo1);
        list.add(systemConfigPoJo2);
        list.add(systemConfigPoJo3);
        //通过iterator，传入到configDao
        ListIterator<SystemConfigPojo> listIterator = list.listIterator();
        while (listIterator.hasNext()) {
            SystemConfigPojo poJo = listIterator.next();
            //查询表中对应字段的id
            Integer id = configDao.selectIdOrderConfig(poJo);
            poJo.setId(id);
            if (id == null) {
                //表中没有这个数据，执行第一次插入数据操作
                Date date = new Date();
                poJo.setAddTime(date);
                poJo.setUpdateTime(date);
                poJo.setDeletedFlag(false);
                configDao.insertOrderConfigById(poJo);
            } else {
                //表中已有数据，直接修改
                Date date = new Date();
                poJo.setUpdateTime(date);
                configDao.updateOrderConfigById(poJo);
            }
        }
    }

    //小程序配置
    @Override
    //小程序配置 页面数据显示
    public ConfigWxVO configWxInfo() {
        //新建confiWxVO作为返回的数据容器
        ConfigWxVO configWxVO = new ConfigWxVO();
        //新建list用以接收返回的数据
        List<SystemConfigPojo> list = configDao.selectWxConfig();
        ListIterator<SystemConfigPojo> iterator = list.listIterator();
        //读取出list中的数据
        while (iterator.hasNext()) {
            SystemConfigPojo systemConfig = iterator.next();
            String key = systemConfig.getConfigName();
            String value = systemConfig.getConfigValue();
            //匹配数据，赋值到configWxVO中
            if ("market_wx_catlog_goods".equals(key)) {
                configWxVO.setMarket_wx_catlog_goods(value);
            } else if ("market_wx_catlog_list".equals(key)) {
                configWxVO.setMarket_wx_catlog_list(value);
            } else if ("market_wx_index_hot".equals(key)) {
                configWxVO.setMarket_wx_index_hot(value);
            } else if ("market_wx_index_brand".equals(key)) {
                configWxVO.setMarket_wx_index_brand(value);
            } else if ("market_wx_index_new".equals(key)) {
                configWxVO.setMarket_wx_index_new(value);
            } else if ("market_wx_index_topic".equals(key)) {
                configWxVO.setMarket_wx_index_topic(value);
            } else if ("market_wx_share".equals(key)) {
                configWxVO.setMarket_wx_share(value);
            }
        }
        return configWxVO;
    }

    @Override
    //小程序配置 确认插入或更新配置项
    public void confirmConfigWx(ConfigWxBO configWxBO) {
//新建list作为容器，取出configWxBO中的数据对应放入容器
        List<SystemConfigPojo> list = new ArrayList<>();
        SystemConfigPojo systemConfigPoJo1 = new SystemConfigPojo();
        SystemConfigPojo systemConfigPoJo2 = new SystemConfigPojo();
        SystemConfigPojo systemConfigPoJo3 = new SystemConfigPojo();
        SystemConfigPojo systemConfigPoJo4 = new SystemConfigPojo();
        SystemConfigPojo systemConfigPoJo5 = new SystemConfigPojo();
        SystemConfigPojo systemConfigPoJo6 = new SystemConfigPojo();
        SystemConfigPojo systemConfigPoJo7 = new SystemConfigPojo();
        systemConfigPoJo1.setConfigName("market_wx_catlog_goods");
        systemConfigPoJo1.setConfigValue(configWxBO.getMarket_wx_catlog_goods());
        systemConfigPoJo2.setConfigName("market_wx_catlog_list");
        systemConfigPoJo2.setConfigValue(configWxBO.getMarket_wx_catlog_list());
        systemConfigPoJo3.setConfigName("market_wx_index_brand");
        systemConfigPoJo3.setConfigValue(configWxBO.getMarket_wx_index_brand());
        systemConfigPoJo4.setConfigName("market_wx_index_hot");
        systemConfigPoJo4.setConfigValue(configWxBO.getMarket_wx_index_hot());
        systemConfigPoJo5.setConfigName("market_wx_index_new");
        systemConfigPoJo5.setConfigValue(configWxBO.getMarket_wx_index_new());
        systemConfigPoJo6.setConfigName("market_wx_index_topic");
        systemConfigPoJo6.setConfigValue(configWxBO.getMarket_wx_index_topic());
        systemConfigPoJo7.setConfigName("market_wx_share");
        systemConfigPoJo7.setConfigValue(configWxBO.getMarket_wx_share());
        list.add(systemConfigPoJo1);
        list.add(systemConfigPoJo2);
        list.add(systemConfigPoJo3);
        list.add(systemConfigPoJo4);
        list.add(systemConfigPoJo5);
        list.add(systemConfigPoJo6);
        list.add(systemConfigPoJo7);
        //通过iterator，传入到configDao
        ListIterator<SystemConfigPojo> listIterator = list.listIterator();
        while (listIterator.hasNext()) {
            SystemConfigPojo poJo = listIterator.next();
            //查询表中对应字段的id
            Integer id = configDao.selectIdWxConfig(poJo);
            poJo.setId(id);
            if (id == null) {
                //表中没有这个数据，执行第一次插入数据操作
                Date date = new Date();
                poJo.setAddTime(date);
                poJo.setUpdateTime(date);
                poJo.setDeletedFlag(false);
                configDao.insertWxConfigById(poJo);
            } else {
                //表中已有数据，直接修改
                Date date = new Date();
                poJo.setUpdateTime(date);
                configDao.updateWxConfigById(poJo);
            }
        }
    }
}
