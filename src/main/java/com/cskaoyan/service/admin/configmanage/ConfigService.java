package com.cskaoyan.service.admin.configmanage;

import com.cskaoyan.model.admin.configmanage.bo.ConfigExpressBO;
import com.cskaoyan.model.admin.configmanage.bo.ConfigMallBO;
import com.cskaoyan.model.admin.configmanage.bo.ConfigOrderBO;
import com.cskaoyan.model.admin.configmanage.bo.ConfigWxBO;
import com.cskaoyan.model.admin.configmanage.vo.ConfigExpressVO;
import com.cskaoyan.model.admin.configmanage.vo.ConfigMallVO;
import com.cskaoyan.model.admin.configmanage.vo.ConfigOrderVO;
import com.cskaoyan.model.admin.configmanage.vo.ConfigWxVO;

public interface ConfigService {

    //商场配置 页面数据显示
    ConfigMallVO configMallInfo();

    //运费配置 确认插入或更新配置项
    void confirmConfigMall(ConfigMallBO configMallBO);

    //运费配置 页面数据显示
    ConfigExpressVO configExpressInfo();

    //运费配置 确认插入或更新配置项
    void confirmConfigExpress(ConfigExpressBO configExpressBO);

    //订单配置 页面数据显示
    ConfigOrderVO configOrderInfo();

    //订单配置 确认插入或更新配置项
    void confirmConfigOrder(ConfigOrderBO configOrderBO);

    //小程序配置 页面数据显示
    ConfigWxVO configWxInfo();

    //小程序配置 确认插入或更新配置项
    void confirmConfigWx(ConfigWxBO configWxBO);
}
