package com.cskaoyan.service.marketmanage;

import com.cskaoyan.model.BaseParam;
import com.cskaoyan.model.admin.marketbeans.bo.*;
import com.cskaoyan.model.admin.marketbeans.pojo.*;
import com.cskaoyan.model.admin.marketbeans.vo.*;
import com.cskaoyan.model.admin.marketbeans.vo.Regionvo.RegionBeanVo;
import com.cskaoyan.model.admin.marketbeans.vo.brandmsgvo.BrandMsgVo;


import java.util.List;


/**
 * @package: com.cskaoyan.service
 * @Description: 商场管理
 * @author: 北青
 * @date: 2022/1/6 21:47
 */
public interface MarketService {

    /**
     * 售后信息模块获取售后信息的接口
     * @param marketAftersaleListBO
     * @return
     */
    MarketAftersaleListVo getMarketAftersaleListInfo(MarketAftersaleListBo marketAftersaleListBO);

    /**
     * 售后信息处理——通过
     * @param marketAftersale: 选中的售后信息
     */
    void recept(MarketAftersale marketAftersale);

    /**
     * 售后信息处理——拒绝
     * @param marketAftersale: 选中的售后信息
     */
    void reject(MarketAftersale marketAftersale);

    /**
     * 售后信息处理——批量通过
     * @param ids: 要批量处理的售后信息id
     */
    void batchRecept(Integer[] ids);

    /**
     * 售后信息处理——批量拒绝
     * @param ids: 要批量处理的售后信息id
     */
    void batchReject(Integer[] ids);

    /**
     * 售后信息处理——退款
     * @param marketAftersale
     */
    void refund(MarketAftersale marketAftersale);


    /**
     * 通用问题——显示问题信息
     *
     * @param question 问题
     * @param baseParam: 分页排序信息
     * @return
     */
    MarketIssueListVo getIssueListInfo(String question, BaseParam baseParam);

    /**
     * 通用问题模块——新增问题
     * @param marketIssue
     */
    void createIssue(MarketIssue marketIssue);

    /**
     * 通用问题模块——编辑修改问题
     * @param marketIssue
     */
    void updateIssue(MarketIssue marketIssue);

    /**
     * 通用问题模块——删除问题信息
     * @param marketIssue
     */
    void deleteIssue(MarketIssue marketIssue);

    /**
     * 商城——关键词——关键词信息显示
     * @param marketKeywordListBo
     * @return
     */
    MarketKeywordListVo getMarketKeywordInfo(MarketKeywordListBo marketKeywordListBo);


    /**
     * 商城——关键词——添加关键词信息
     * @param marketKeyword
     */
    void createKeyword(MarketKeyword marketKeyword);

    /**
     * 商城——关键词——修改关键词信息
     * @param marketKeyword
     */
    void updateKeyword(MarketKeyword marketKeyword);
    /*
     * @createTime: 2022/1/7 20:53
     * @author: Dragon
     * @description: 商城管理模块-行政区域查询全部信息service接口t
     * */
    RegionBeanVo getRegionList();

    /*
     * @createTime: 2022/1/7 20:53
     * @author: Dragon
     * @description: 商城管理模块-品牌制造商查询全部信息service接口
     * */
    BrandMsgVo getBrandList(BaseParam baseParam, String id, String name);

    BrandAddVo addNewBrand(AddBrandBo addBrandBo);

    /**
     * 商城——订单管理——订单信息显示
     * @param marketOrderListBo
     * @return
     */
    MarketOrderInfoVo getOrderListInfo(MarketOrderListBo marketOrderListBo);

    UpdateBrand renewBrand(UpdateBrand updateBrand);

    int removeBrand(Integer id);

    CategoryListVo getAllCategoryMsg();

    GetLevelFirstCategory getLevel1CategoryMsg();

    AddCategory addCategory(AddCategory addCategory);

    int deleteCategory(Integer id);

    void updateCategory(UpdateCategoryBo updateCategoryBo);
    /**
     * 获取快递公司信息
     * @return
     */
    List<MarketChannelVo> getChannel();

    void shipOrder(OrderShipBo orderShipBo);
    /**
     * 订单模块——获取订单详细信息
     * @param orderId
     * @return
     */
    MarketOrderDetailVo getOrderDetail(Integer orderId);

    /**
     * 订单管理——删除订单
     * @param orderId
     */
    void deleteOrder(Integer orderId);
}
