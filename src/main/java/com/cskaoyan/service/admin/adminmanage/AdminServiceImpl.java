package com.cskaoyan.service.admin.adminmanage;

import com.cskaoyan.dao.adminmanage.AdminMapper;
import com.cskaoyan.exception.adminmanage.AdminAdminException;
import com.cskaoyan.model.BaseParam;
import com.cskaoyan.model.admin.adminmanage.admin.*;
import com.cskaoyan.model.realm.AdminLoginAdminData;
import com.cskaoyan.utils.BeanCreateUtils;
import com.cskaoyan.utils.DateUtils;
import com.cskaoyan.utils.RequestUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @Author:
 * @description:
 * @keypoint:
 * @tags:
 * @related:
 */
@Component
public class AdminServiceImpl implements AdminService{
    
    @Value("${visit.prefix}")
    String httpPrefix;

    @Autowired
    AdminMapper adminMapper;

    /**
     * 获取admin表的信息，username不一定有
     * @param baseParam
     * @param username
     * @return
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    @Override
    public AdminVO getAdminInfo(BaseParam baseParam, String username) throws InstantiationException, IllegalAccessException {
        Integer page = baseParam.getPage();
        Integer limit = baseParam.getLimit();

        PageHelper.startPage(page,limit);
        List<AdminListVO> listDTO = adminMapper.selectAdminInfo(baseParam.getSort(), baseParam.getOrder(),username);
        PageInfo<AdminListVO> pageInfo = new PageInfo<>(listDTO);
        int pages = pageInfo.getPages();
        long total = pageInfo.getTotal();

        AdminVO bean = BeanCreateUtils.createBean(AdminVO.class, Math.toIntExact(total), pages, limit, page, listDTO);

        return bean;
    }

    /**
     * 新增管理员
     * @param adminCreateBO
     * @return
     */
    @Override
    public AdminCreateVO addNewAdmin(AdminCreateBO adminCreateBO) throws InstantiationException, IllegalAccessException, AdminAdminException {
        AdminLoginAdminData principal = (AdminLoginAdminData) SecurityUtils.getSubject().getPrincipal();
        String nowTime = DateUtils.getNowDateTime();

        String ipAddress = principal.getLast_login_ip();
//        String ipAddress = RequestUtils.getIPAddress(req);

        //BO->POJO 创建
        AdminPOJO bean = BeanCreateUtils.createBean(AdminPOJO.class,
                null, adminCreateBO.getUsername(), adminCreateBO.getPassword(), ipAddress, nowTime, adminCreateBO.getAvatar(), nowTime, nowTime, false, adminCreateBO.getRoleIds());

        Integer result = adminMapper.insertNewAdmin(bean);
        if(result !=1){
            throw new AdminAdminException("insert error");
        }

        //POJO->VO
        AdminCreateVO bean1 = BeanCreateUtils.createBean(AdminCreateVO.class,
                bean.getId(), bean.getUsername(), bean.getPassword(), bean.getAvatar(), bean.getAdd_time(), bean.getUpdate_time(), bean.getRole_ids());

        return bean1;

    }

    @Override
    public Integer deleteAdmin(Integer id) throws AdminAdminException {
        //逻辑删除
        Integer result = adminMapper.updateAdminDeletedById(id);
        if(result !=1) throw new AdminAdminException("delete error");
        return result;
    }

    @Override
    public AdminUpdateVO updateAdminInfo(AdminUpdateBO adminUpdateBO) throws InstantiationException, IllegalAccessException, AdminAdminException {
        //todo 名字重复抛异常

        String update_time = DateUtils.getNowDateTime();
        AdminPOJO pojo = BeanCreateUtils.createBean(AdminPOJO.class, adminUpdateBO.getId(), adminUpdateBO.getUsername(), adminUpdateBO.getPassword(), null, null, adminUpdateBO.getAvatar(), null, update_time, null, adminUpdateBO.getRoleIds());
        //前端传过来的包含http:localhost:8083/的前缀，去掉

        String newAvatar = pojo.getAvatar().replace(httpPrefix,"");
        pojo.setAvatar(newAvatar);

        Integer result = adminMapper.updateAdminInfo(pojo);
        if(result !=1) throw new AdminAdminException("update failed");

        AdminUpdateVO bean = BeanCreateUtils.createBean(AdminUpdateVO.class, adminUpdateBO.getId(), adminUpdateBO.getUsername(), adminUpdateBO.getAvatar(), update_time, adminUpdateBO.getRoleIds());
        return bean;
    }
}
