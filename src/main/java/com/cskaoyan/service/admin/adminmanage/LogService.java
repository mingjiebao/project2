package com.cskaoyan.service.admin.adminmanage;

import com.cskaoyan.model.BaseParam;
import com.cskaoyan.model.admin.adminmanage.log.LogListVO;

/**
 * @Author:
 * @description:
 * @keypoint:
 * @tags:
 * @related:
 */

public interface LogService {
    LogListVO showLogInfo(BaseParam baseParam, String name) throws InstantiationException, IllegalAccessException;
}
