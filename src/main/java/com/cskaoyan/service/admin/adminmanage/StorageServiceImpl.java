package com.cskaoyan.service.admin.adminmanage;

import com.cskaoyan.dao.adminmanage.StorageMapper;
import com.cskaoyan.exception.adminmanage.AdminStorageException;
import com.cskaoyan.model.BaseRespVo;
import com.cskaoyan.model.FileUploadDTO;
import com.cskaoyan.model.admin.adminmanage.storage.StorageListPOJO;
import com.cskaoyan.model.admin.adminmanage.storage.StorageListVO;
import com.cskaoyan.model.admin.adminmanage.storage.StorageParam;
import com.cskaoyan.model.admin.adminmanage.storage.StorageVO;
import com.cskaoyan.utils.BeanCreateUtils;
import com.cskaoyan.utils.FileUploadUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * @description:
 * @keypoint:
 * @tags:
 * @related:
 */

@Service
public class StorageServiceImpl implements StorageService{
//    @Value("${upload.macBasePath}")
//    String macBasePath;
//
//    @Value("${upload.windowsBasePath}")
//    String windowsBasePath;

    @Autowired
    HashMap<String,String> uploadPath;

    @Value("${visit.prefix}")
    String visitPrefix;

    @Autowired
    StorageMapper storageMapper;

    /**
     * 从market_storage 数据库里面获取全部的信息
     * @param storageParam
     * @return
     */
    @Override
    public StorageListVO getStorageInfo(StorageParam storageParam) {

        Integer limit = storageParam.getLimit();
        Integer page = storageParam.getPage();

        PageHelper.startPage(page,limit);

        List<StorageVO> listDTO = storageMapper.selectStorageInfoByStorageParam(storageParam);

        PageInfo<StorageVO> pageInfo = new PageInfo<>(listDTO);

        long total = pageInfo.getTotal(); //没有分页情况下的总的数据量
        int pages = pageInfo.getPages(); //总的页码数

        StorageListVO storageListVO = StorageListVO.create(total,pages,limit,page,listDTO);
        return storageListVO;
    }

    /**
     * 根据id 逻辑删除 market_storage里面的信息
     * @param id
     * @return
     * @throws AdminStorageException
     */
    @Override
    public Integer deleteStorageInfo(Integer id) throws AdminStorageException {
        Integer result = storageMapper.setDeletedTrueById(id);
        if(result != 1){
            throw new AdminStorageException("delete failed");
        }
        return result;
    }

    /**
     * market_storage 更新信息，前端这里只修改了name
     * @param storageListDTO
     * @return
     * @throws AdminStorageException
     */
    @Override
    public Integer updateStorageInfo(StorageListPOJO storageListDTO) throws AdminStorageException {
        Integer result = storageMapper.updateStorageInfo(storageListDTO);
        if(result != 1){
            throw new AdminStorageException("update failed");
        }
        return result;

    }

    /**
     * 往market_storage里面插入新条目
     * @param file
     * @return
     * @throws AdminStorageException
     */
    @Override
    public StorageListPOJO addNewStorage(MultipartFile file) throws AdminStorageException {
        //文件上传
        FileUploadDTO fileInfo = null;
        StorageListPOJO storageListPOJO = null;
        try {
            //获取上传的信息
            fileInfo = FileUploadUtils.upload(file, uploadPath,"adminmanage");
            //生成Time
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String add_time = simpleDateFormat.format(new Date(System.currentTimeMillis()));
            String update_time = simpleDateFormat.format(new Date(System.currentTimeMillis()));
            //pojo 用来插入
//            storageListPOJO = StorageListPOJO.create(null, fileInfo.getKey(), fileInfo.getName(), fileInfo.getType(), Math.toIntExact(fileInfo.getSize()), fileInfo.getUrl(), add_time, update_time, false);

            storageListPOJO = BeanCreateUtils.createBean(StorageListPOJO.class, null, fileInfo.getKey(), fileInfo.getName(), fileInfo.getType(), Math.toIntExact(fileInfo.getSize()), fileInfo.getUrl(), add_time, update_time, false);
            // 插入数据库
            storageMapper.insertNewStorage(storageListPOJO);
        } catch (Exception e) {
            e.printStackTrace();
            throw new AdminStorageException("insert failed");
        }

        //前端不需要deleted信息
        storageListPOJO.setDeleted(null);
        storageListPOJO.setUrl(visitPrefix + storageListPOJO.getUrl());

        return storageListPOJO;
    }
}
