package com.cskaoyan.service.admin.adminmanage;

import com.cskaoyan.exception.adminmanage.AdminStorageException;
import com.cskaoyan.model.admin.adminmanage.storage.StorageListPOJO;
import com.cskaoyan.model.admin.adminmanage.storage.StorageListVO;
import com.cskaoyan.model.admin.adminmanage.storage.StorageParam;
import org.springframework.web.multipart.MultipartFile;

/**
 * @description:
 * @keypoint:
 * @tags:
 * @related:
 */

public interface StorageService {
    StorageListVO getStorageInfo(StorageParam storageParam);

    Integer deleteStorageInfo(Integer id) throws AdminStorageException;

    Integer updateStorageInfo(StorageListPOJO storageListDTO) throws AdminStorageException;

    StorageListPOJO addNewStorage(MultipartFile multipartFile) throws AdminStorageException;
}
