package com.cskaoyan.service.admin.adminmanage;

import com.cskaoyan.exception.adminmanage.AdminRoleException;
import com.cskaoyan.model.BaseParam;
import com.cskaoyan.model.admin.adminmanage.role.*;

/**
 * @Author:
 * @description:
 * @keypoint:
 * @tags:
 * @related:
 */

public interface RoleService {
    RoleVO getSimpleRole() throws InstantiationException, IllegalAccessException;

    Integer initDataBase(SystemPermissonVO systemPermissonVO);

    Integer updateRolePermission(PermissionUpdateBO permissionUpdateBO);

    RolePermissionVO getRolePermission(Integer roleId) throws InstantiationException, IllegalAccessException;

    RoleDetailVO getRoleDetail(BaseParam baseParam, String name) throws InstantiationException, IllegalAccessException;

    RoleDetailDTO createNewRole(RoleCreateBO roleCreateBO) throws InstantiationException, IllegalAccessException, AdminRoleException;

    Integer updateRoleInfo(RoleUpdateDeleteBO roleUpdateDeleteBO) throws AdminRoleException;

    Integer deleteRoleInfo(RoleUpdateDeleteBO roleUpdateDeleteBO) throws AdminRoleException;
}
