package com.cskaoyan.service.admin.adminmanage;

import com.cskaoyan.dao.adminmanage.LogMapper;
import com.cskaoyan.model.BaseParam;
import com.cskaoyan.model.admin.adminmanage.log.LogListDTO;
import com.cskaoyan.model.admin.adminmanage.log.LogListVO;
import com.cskaoyan.utils.BeanCreateUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Author:
 * @description:
 * @keypoint:
 * @tags:
 * @related:
 */
@Component
public class LogServiceImpl implements LogService{
    @Autowired
    LogMapper logMapper;
    @Override
    public LogListVO showLogInfo(BaseParam baseParam, String name) throws InstantiationException, IllegalAccessException {
        Integer page = baseParam.getPage();
        Integer limit = baseParam.getLimit();

        PageHelper.startPage(page,limit);
        List<LogListDTO> logListDTO = logMapper.selectLogInfo(baseParam.getSort(),baseParam.getOrder(),name);

        PageInfo<LogListDTO> pageInfo = new PageInfo<>(logListDTO);
        int pages = pageInfo.getPages();
        long total = pageInfo.getTotal();
        LogListVO bean = BeanCreateUtils.createBean(LogListVO.class, Math.toIntExact(total), pages, limit, page, logListDTO);

        return bean;

    }
}
