package com.cskaoyan.service.admin.adminmanage;

import com.cskaoyan.exception.adminmanage.AdminAdminException;
import com.cskaoyan.model.BaseParam;
import com.cskaoyan.model.admin.adminmanage.admin.*;

import javax.servlet.http.HttpServletRequest;

/**
 * @Author:
 * @description:
 * @keypoint:
 * @tags:
 * @related:
 */

public interface AdminService {
    AdminVO getAdminInfo(BaseParam baseParam, String username) throws InstantiationException, IllegalAccessException;

    AdminCreateVO addNewAdmin(AdminCreateBO adminCreateBO) throws InstantiationException, IllegalAccessException, AdminAdminException;

    Integer deleteAdmin(Integer id) throws AdminAdminException;

    AdminUpdateVO updateAdminInfo(AdminUpdateBO adminUpdateBO) throws InstantiationException, IllegalAccessException, AdminAdminException;
}
