package com.cskaoyan.service.admin.adminmanage;

import com.cskaoyan.dao.adminmanage.RoleMapper;
import com.cskaoyan.exception.adminmanage.AdminRoleException;
import com.cskaoyan.model.BaseParam;
import com.cskaoyan.model.admin.adminmanage.role.*;
import com.cskaoyan.utils.BeanCreateUtils;
import com.cskaoyan.utils.DateUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Author:
 * @description:
 * @keypoint:
 * @tags:
 * @related:
 */

@Component
public class RoleServiceImpl implements RoleService {
    @Autowired
    RoleMapper roleMapper;

    /**
     * 获取Role的简单信息
     *
     * @return
     */
    @Override
    public RoleVO getSimpleRole() throws InstantiationException, IllegalAccessException {
        Integer total = roleMapper.getItemNum();
        //数据库查询返回
        List<RoleDTO> roleDTOS = roleMapper.selectSimpleRole();

        RoleVO bean = BeanCreateUtils.createBean(RoleVO.class, total, 1, total, 1, roleDTOS);
        return bean;


    }

    /**
     * todo 使用prepraedstatment 和 batch试一下
     * @param systemPermissonVO
     * @return
     */

    @Override
    public Integer initDataBase(SystemPermissonVO systemPermissonVO) {
        List<ModuleVO> moduleList = systemPermissonVO.getSystemPermissions();
        Integer re = roleMapper.insertToModule(moduleList);

        // 给pid赋值
        int indexModule = 1;
        int indexFunction =1;
        for (ModuleVO systemPermission : moduleList) {
            List<FunctionVO> children = systemPermission.getChildren();


            for (FunctionVO func : children) {
                func.setPid(indexModule);
                List<ApiVO> children1 = func.getChildren();
                for (ApiVO apiVO : children1) {
                    apiVO.setPid(indexFunction);
                }
                indexFunction++ ;
                Integer re2 = roleMapper.insertToApi(children1);
            }
            indexModule++;
            Integer re3 = roleMapper.insertToFunction(children);
        }

        return 1;
    }

    /**
     * 更新角色的管理权限，先删除所有的再更新
     *
     * @param permissionUpdateBO
     * @return
     */
    @Override
    public Integer updateRolePermission(PermissionUpdateBO permissionUpdateBO) {

        //todo 测试
        Integer result = roleMapper.deleteAllPermissionById(permissionUpdateBO.getRoleId());
        Integer result2 = roleMapper.insertRolePersmission(permissionUpdateBO);

        return 1;
    }

    /**
     *
     * @param roleId
     * @return
     */
    @Override
    public RolePermissionVO getRolePermission(Integer roleId) throws InstantiationException, IllegalAccessException {
       List<ModuleVO> systemPermissonVO = roleMapper.getAllSystemPermission();
       List<String> rolePermissions = roleMapper.getAllRolePermissionById(roleId);

        RolePermissionVO bean = BeanCreateUtils.createBean(RolePermissionVO.class, rolePermissions, systemPermissonVO);
        return bean;
    }

    /**
     * 角色管理的页面展示
     * @param baseParam
     * @param name
     * @return
     */
    @Override
    public RoleDetailVO getRoleDetail(BaseParam baseParam, String name) throws InstantiationException, IllegalAccessException {
        Integer page = baseParam.getPage();
        Integer limit = baseParam.getLimit();

        PageHelper.startPage(page,limit);
        String sort = baseParam.getSort();
        String order = baseParam.getOrder();
        List<RoleDetailDTO> roleDetailDTOS =  roleMapper.selectAllRole(sort,order,name);
        PageInfo<RoleDetailDTO> pageInfo = new PageInfo<>(roleDetailDTOS);

        int pages = pageInfo.getPages();
        long total = pageInfo.getTotal();
        RoleDetailVO bean = BeanCreateUtils.createBean(RoleDetailVO.class, Math.toIntExact(total), pages, limit, pages, roleDetailDTOS);
        return bean;
    }

    /**
     * 创建新的角色
     * @param roleCreateBO
     * @return
     */
    @Override
    public RoleDetailDTO createNewRole(RoleCreateBO roleCreateBO) throws InstantiationException, IllegalAccessException, AdminRoleException {
        //生成数据
        String addTime = DateUtils.getNowDateTime();
        String updateTime = addTime;
        RoleDetailDTO bean = BeanCreateUtils.createBean(RoleDetailDTO.class, null, roleCreateBO.getName(), roleCreateBO.getDesc(), null, addTime, updateTime, null);
        try {
            Integer result = roleMapper.insertNewRole(bean);
        }catch (Exception e){
            e.printStackTrace();
            throw new AdminRoleException("名字重复");
        }
        return bean;
    }

    /**
     * 添加角色
     * @param roleUpdateDeleteBO
     * @return
     * @throws AdminRoleException
     */
    @Override
    public Integer updateRoleInfo(RoleUpdateDeleteBO roleUpdateDeleteBO) throws AdminRoleException {
        Integer result = roleMapper.updateRoleInfo(roleUpdateDeleteBO.getId(), roleUpdateDeleteBO.getName(), roleUpdateDeleteBO.getDesc());
        if(result != 1)throw new AdminRoleException("update error");
        return result;
    }

    /**
     * 逻辑删除
     * @param roleUpdateDeleteBO
     * @return
     */
    @Override
    public Integer deleteRoleInfo(RoleUpdateDeleteBO roleUpdateDeleteBO) throws AdminRoleException {
        Integer result = roleMapper.deleteRole(roleUpdateDeleteBO.getId());

        if(result != 1) throw new AdminRoleException("delete error");
        return result;
    }
}
