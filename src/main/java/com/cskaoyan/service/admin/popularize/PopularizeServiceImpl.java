package com.cskaoyan.service.admin.popularize;

import com.cskaoyan.dao.polularize.MarketAdDao;
import com.cskaoyan.model.admin.ad.bo.AdCreateBO;
import com.cskaoyan.model.admin.ad.bo.AdListBO;
import com.cskaoyan.model.admin.ad.bo.AdUpdateBo;
import com.cskaoyan.model.admin.ad.vo.AdListDataVo;
import com.cskaoyan.model.admin.ad.vo.AdListVo;
import com.cskaoyan.model.admin.pojo.MarketAdPojo;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class PopularizeServiceImpl implements PopularizeService {

    @Autowired
    MarketAdDao marketAdDao;

    /***
     *      根据条件查询所有的广告信息，将所有的数据一次性读出，数据处理的过程交付给java，所以创建了pojo，
     *      方便复用
     */
    @Override
    public AdListVo getAdList(AdListBO adList) {
        AdListVo result = new AdListVo(adList);

        PageHelper.startPage(adList.getPage(), adList.getLimit());
        List<MarketAdPojo> list = marketAdDao.getMarketAdInfo(adList);
        // 真正进行分页的是再这里,会进行两次Sql查询，一次count(0) 查询total
        // 一次查询根据条件对list进行裁剪和修饰
        PageInfo<MarketAdPojo> info = new PageInfo<>(list);

        result.setTotal((int) info.getTotal());
        result.setPages(info.getPages());

        result.setList(list.stream().map(AdListDataVo::new).collect(Collectors.toList()));

        return result;
    }


    /***
     *
     *  广告内容的插入
     */
    @Override
    public AdListDataVo addNewAdInfo(AdCreateBO bo) {
        MarketAdPojo pojo = transferAdCreateBo(bo);
        if (bo.getLink() == null) {
            bo.setLink("");
        }
        marketAdDao.addNewAdInfo(pojo);
        return new AdListDataVo(pojo);
    }

    @Override
    public AdListDataVo updateAdInfo(AdUpdateBo bo) {
        marketAdDao.updateAdInfo(bo);
        return transferAdUpdateBo(bo);
    }

    /***
     *  插入数据库的时间，应该按照java的时间来。
     */
    private MarketAdPojo transferAdCreateBo(AdCreateBO bo) {
        MarketAdPojo pojo = new MarketAdPojo();
        pojo.setName(bo.getName());
        pojo.setContent(bo.getContent());
        pojo.setUrl(bo.getUrl());
        pojo.setLink(bo.getLink());
        pojo.setPosition(bo.getPosition());
        pojo.setAddTime(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH-mm-ss")));
        pojo.setUpdateTime(pojo.getAddTime());
        // enable代表是否启用,1为true,0为false
        if (bo.getEnabled()) {
            pojo.setEnabled(1);
        } else {
            pojo.setEnabled(0);
        }
        return pojo;
    }

    private AdListDataVo transferAdUpdateBo(AdUpdateBo bo) {
        AdListDataVo vo = new AdListDataVo();

        vo.setId(bo.getId());
        vo.setName(bo.getName());
        vo.setUrl(bo.getUrl());
        vo.setPosition(bo.getPosition());
        vo.setContent(bo.getContent());
        vo.setLink(bo.getLink());
        vo.setEnabled(bo.getEnabled());
        vo.setAddTime(bo.getAddTime());
        vo.setUpdateTime(bo.getUpdateTime());
        vo.setDeleted(bo.getDeleted());
        return vo;
    }
}
