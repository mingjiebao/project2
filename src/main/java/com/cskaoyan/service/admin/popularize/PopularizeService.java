package com.cskaoyan.service.admin.popularize;

import com.cskaoyan.model.admin.ad.bo.AdCreateBO;
import com.cskaoyan.model.admin.ad.bo.AdListBO;
import com.cskaoyan.model.admin.ad.bo.AdUpdateBo;
import com.cskaoyan.model.admin.ad.vo.AdListDataVo;
import com.cskaoyan.model.admin.ad.vo.AdListVo;

public interface PopularizeService {
    AdListVo getAdList(AdListBO adList);

    AdListDataVo addNewAdInfo(AdCreateBO bo);

    AdListDataVo updateAdInfo(AdUpdateBo bo);
}
