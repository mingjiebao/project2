package com.cskaoyan.service.admin.popularize;

import com.cskaoyan.model.admin.topic.bo.TopicCreateBo;
import com.cskaoyan.model.admin.topic.bo.TopicListBo;
import com.cskaoyan.model.admin.topic.bo.TopicUpdateBo;
import com.cskaoyan.model.admin.topic.vo.TopicBaseRespVo;
import com.cskaoyan.model.admin.topic.vo.TopicReadBaseVo;
import com.cskaoyan.model.admin.pojo.MarketTopicPojo;

import java.util.List;

public interface TopicService {
    TopicBaseRespVo getTopicListInfo(TopicListBo bo);

    MarketTopicPojo createTopicInfo(TopicCreateBo bo);

    TopicReadBaseVo readTopicInfo(Integer id);

    void updateTopicInfo(TopicUpdateBo bo);

    void deleteTopicInfo(Integer bo);

    void batchDeleteTopicInfo(List<Integer> arr);
}
