package com.cskaoyan.service.admin.popularize;

import com.cskaoyan.dao.polularize.MarketTopicDao;
import com.cskaoyan.model.admin.goodsbean.vo.GoodsDetailVo;
import com.cskaoyan.model.admin.topic.TopicExample;
import com.cskaoyan.model.admin.topic.bo.TopicCreateBo;
import com.cskaoyan.model.admin.topic.bo.TopicListBo;
import com.cskaoyan.model.admin.topic.bo.TopicUpdateBo;
import com.cskaoyan.model.admin.topic.vo.TopicBaseRespVo;

import com.cskaoyan.model.admin.topic.vo.TopicListDataVo;
import com.cskaoyan.model.admin.topic.vo.TopicReadBaseVo;
import com.cskaoyan.model.admin.pojo.MarketTopicPojo;
import com.cskaoyan.service.admin.goods.GoodsService;
import com.cskaoyan.utils.StringParseUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class TopicServiceImpl implements TopicService {

    @Autowired
    MarketTopicDao topicDao;

    /***
     *
     * @param bo :需要转换成Case方便topicDao的复用
     * @return ：返回查询的结果
     */
    @Override
    public TopicBaseRespVo getTopicListInfo(TopicListBo bo) {

        final TopicExample topicExample = new TopicExample();
        topicExample.setOrder(bo.getOrder());
        topicExample.setSort(bo.getSort());
        topicExample.setTitle(bo.getTitle());
        topicExample.setSubtitle(bo.getSubtitle());


        PageHelper.startPage(bo.getPage(), bo.getLimit());
        List<MarketTopicPojo> resultList = topicDao.getMarketTopicList(topicExample);
        PageInfo<MarketTopicPojo> info = new PageInfo(resultList);


        // 结果映射
        List<TopicListDataVo> list = resultList.stream().map(TopicListDataVo::new).collect(Collectors.toList());

        TopicBaseRespVo respVo = new TopicBaseRespVo();

        respVo.setList(new ArrayList<>(list));
        respVo.setPages(info.getPages());
        respVo.setTotal((int) info.getTotal());
        respVo.setPage(bo.getPage());
        return respVo;
    }


    /**
     * 创建新的推广条目
     */
    @Override
    public MarketTopicPojo createTopicInfo(TopicCreateBo bo) {
        MarketTopicPojo pojo = transferTopicCreateBo(bo);
        topicDao.addNewTopicInfo(pojo);
        return pojo;
    }

    /**
     * @param id；topic的主键DI
     * @return : 返回相应的视图
     */
    @Override
    public TopicReadBaseVo readTopicInfo(Integer id) {
        TopicExample example = new TopicExample();
        example.setId(id);
        List<MarketTopicPojo> marketTopicList = topicDao.getMarketTopicList(example);

        TopicReadBaseVo result = new TopicReadBaseVo();
        // 异常处理
        if (marketTopicList.size() == 0) {
            return null;
        }


        TopicListDataVo topicListDataVo = new TopicListDataVo(marketTopicList.get(0));
        result.setTopic(topicListDataVo);

        Integer[] goods = StringParseUtils.getIntArrFromString(marketTopicList.get(0).getGoods());
        if(goods!=null){
            result.setGoodsList(topicDao.getTopicGoodsInfo(goods));
        }
        // todo:调用商品接口得到返回值
//        result.setGoodsList(new ArrayList<>());

        return result;
    }

    /**
     * 更新推广信息
     */
    @Override
    public void updateTopicInfo(TopicUpdateBo bo) {
        bo.setTopicValue(Double.parseDouble(bo.getPrice()));
        bo.setGoodsValue(bo.getGoods().toString());
        topicDao.updateTopicInfo(bo);
    }

    @Override
    public void deleteTopicInfo(Integer id) {
        topicDao.deleteTopicInfo(id);
    }

    @Override
    public void batchDeleteTopicInfo(List<Integer> arr) {
        for (Integer integer : arr) {
            topicDao.deleteTopicInfo(integer);
        }
    }

    private MarketTopicPojo transferTopicCreateBo(TopicCreateBo bo) {
        MarketTopicPojo pojo = new MarketTopicPojo();
        pojo.setContent(bo.getContent());
        pojo.setGoods(bo.getGoods().toString());
        pojo.setPicUrl(bo.getPicUrl());
        pojo.setPrice(BigDecimal.valueOf(Double.parseDouble(bo.getPrice())));
        pojo.setSubtitle(bo.getSubtitle());
        pojo.setTitle(bo.getTitle());

        if (bo.getReadCount() != null) {
            pojo.setReadCount(bo.getReadCount());
        }
        pojo.setReadCount("0");

        pojo.setSortOrder(0);
        pojo.setDeleted(0);
        String time = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        pojo.setAddTime(time);
        pojo.setUpdateTime(time);
        return pojo;
    }
}
