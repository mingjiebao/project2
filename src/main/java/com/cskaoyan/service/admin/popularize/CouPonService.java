package com.cskaoyan.service.admin.popularize;

import com.cskaoyan.exception.ParamsException;
import com.cskaoyan.model.admin.coupon.bo.CouponCreateInfoBo;
import com.cskaoyan.model.admin.coupon.bo.CouponListBo;
import com.cskaoyan.model.admin.coupon.bo.CouponUpdateBo;
import com.cskaoyan.model.admin.coupon.bo.CouponUserBo;
import com.cskaoyan.model.admin.coupon.vo.CouponAddDataVo;
import com.cskaoyan.model.admin.coupon.vo.CouponReadVo;
import com.cskaoyan.model.admin.coupon.vo.CouponResponseVo;

public interface CouPonService {
    CouponResponseVo getCouponList(CouponListBo bo);

    CouponAddDataVo addNewCoupon(CouponCreateInfoBo bo);

    CouponAddDataVo updateCouponInfo(CouponUpdateBo bo) throws ParamsException;

    void checkAndUpdateCouponInfo(Integer id);

    CouponReadVo getCouponRead(Integer id);

    CouponResponseVo getUserInfo(CouponUserBo bo);
}
