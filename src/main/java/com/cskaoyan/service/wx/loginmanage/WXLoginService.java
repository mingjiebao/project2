package com.cskaoyan.service.wx.loginmanage;

import com.cskaoyan.model.wx.wxhomeindexbean.WXRegisterBo;
import com.cskaoyan.model.wx.wxindexbean.LoginIndexDto;
import com.cskaoyan.model.wx.wxloginbean.LoginOutVo;
import com.cskaoyan.model.wx.wxloginbean.LoginVo;
import com.cskaoyan.model.wx.wxloginbean.UserInfo;

public interface WXLoginService {

    UserInfo userLogin(String username, String password);

    int userRegister(WXRegisterBo wxRegisterBo);
}
