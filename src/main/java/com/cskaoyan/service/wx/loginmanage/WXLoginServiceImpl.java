package com.cskaoyan.service.wx.loginmanage;

import com.cskaoyan.dao.wx.homemanage.WXLoginDao;
import com.cskaoyan.model.wx.wxhomeindexbean.WXRegisterBo;
import com.cskaoyan.model.wx.wxloginbean.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author : [Dragon]
 * @createTime : [2022/1/10 14:40]
 * @description :
 */

@Service
@Transactional
public class WXLoginServiceImpl implements WXLoginService {

    @Autowired
    WXLoginDao wxLoginDao;

    @Override
    public UserInfo userLogin(String username, String password) {
        UserInfo userInfo = wxLoginDao.selectUserExist(username, password);
        if (userInfo.getAvatarUrl() != null) {
            return userInfo;
        }
        return null;
    }

    @Override
    public int userRegister(WXRegisterBo wxRegisterBo) {
        int i = 0;
        try {
            i = wxLoginDao.insertUser(wxRegisterBo);
        } catch (Exception e) {
            return 404;
        }
        return i;
    }
}
