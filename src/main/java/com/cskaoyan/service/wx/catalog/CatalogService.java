package com.cskaoyan.service.wx.catalog;

import com.cskaoyan.model.wx.catalog.CatalogIndexVO;
import com.cskaoyan.model.wx.catalog.CurrentVO;

/**
 * @Author AhaNg
 * @Date 2022/1/11 12:24
 * @description:
 * @return:
 */
public interface CatalogService {


    /**
     * @Author: AhaNg
     * @description: 获取当前类目信息
     * @return:
     */
    CurrentVO current(Integer id);


    /**
     * @Author: AhaNg
     * @description: 获取所有类目信息
     * @return:
     */

    CatalogIndexVO index();

}
