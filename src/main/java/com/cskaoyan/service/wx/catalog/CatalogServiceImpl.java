package com.cskaoyan.service.wx.catalog;

import com.cskaoyan.dao.goods.CategoryMapper;
import com.cskaoyan.model.admin.goodsbean.Category;
import com.cskaoyan.model.admin.goodsbean.CategoryExample;
import com.cskaoyan.model.wx.catalog.CatalogIndexVO;
import com.cskaoyan.model.wx.catalog.CurrentVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author AhaNg
 * @Date 2022/1/11 12:25
 * @description:
 * @return:
 */
@Service
public class CatalogServiceImpl implements CatalogService{



    @Autowired
    CategoryMapper categoryMapper;

    /**
     * @Author: AhaNg
     * @description: 显示当前类目的信息
     * @return:
     */
    @Override
    public CurrentVO current(Integer id) {
      //当前类目信息---->pid
        Category category=categoryMapper.selectByPrimaryKey(id);
        CategoryExample categoryExample = new CategoryExample();
        categoryExample.createCriteria().andPidEqualTo(id);
        List<Category>categories=categoryMapper.selectByExample(categoryExample);
        CurrentVO currentVO = new CurrentVO();
        currentVO.setCurrentCategory(category);
        currentVO.setCurrentSubCategory(categories);
        return currentVO;
    }


    /**
     * @Author: AhaNg
     * @description: 显示所有类目
     * @return:
     */
    @Override
    public CatalogIndexVO index() {
        CategoryExample fatherExam = new CategoryExample();
        fatherExam.createCriteria().andPidEqualTo(0);
        List<Category>fatherCategoryList=categoryMapper.selectByExample(fatherExam);
        CatalogIndexVO indexVO=new CatalogIndexVO();
        indexVO.setCategoryList(fatherCategoryList);

        //当前的就是第一个
        Category currentCategory=fatherCategoryList.get(0);
        indexVO.setCurrentCategory(currentCategory);

        //当前类目的子类目
        CategoryExample currentSubExam = new CategoryExample();
        fatherExam.createCriteria().andPidEqualTo(currentCategory.getId());
        List<Category>currentSubList=categoryMapper.selectByExample(currentSubExam);
        indexVO.setCurrentSubCategory(currentSubList);
        return indexVO;
    }
}
