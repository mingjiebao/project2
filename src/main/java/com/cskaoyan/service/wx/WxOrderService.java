package com.cskaoyan.service.wx;

import com.cskaoyan.model.BaseParam;
import com.cskaoyan.model.admin.goodsbean.Comment;
import com.cskaoyan.model.admin.marketbeans.pojo.MarketAftersale;
import com.cskaoyan.model.wx.aftersale.WxAftersaleListVo;
import com.cskaoyan.model.wx.order.bo.CommentSession;
import com.cskaoyan.model.wx.order.bo.WxOrderSubmitBo;
import com.cskaoyan.model.wx.order.vo.WxOrderDetailGoodsVo;
import com.cskaoyan.model.wx.order.vo.WxOrderDetailInfoVo;
import com.cskaoyan.model.wx.order.vo.WxOrderListInfoVo;
import com.cskaoyan.model.wx.order.vo.WxOrderSubmitVo;

import java.util.List;

/**
 * @package: com.cskaoyan.service.wx
 * @Description:
 * @author: 北青
 * @date: 2022/1/10 15:10
 */
public interface WxOrderService {
    /**
     * 微信小程序个人中心订单显示
     * @param baseParam
     * @param showType
     * @return
     */
    WxOrderListInfoVo getWxOrderListInfo(BaseParam baseParam, Integer showType);

    /**
     * 小程序显示订单详细信息
     * @param orderId
     * @return
     */
    WxOrderDetailInfoVo getWxOrderDetailInfo(Integer orderId);

    /**
     * 取消订单
     *
     * @param orderId
     */
    void cancelOrder(Integer orderId);

    /**
     * 申请退款
     * @param orderId
     */
    void refund(Integer orderId);

    /**
     * 删除订单
     * @param orderId
     */
    void deleteOrder(Integer orderId);

    /**
     * 确认收货
     * @param orderId
     */
    void confirm(Integer orderId);

    /**
     * 取消订单或者退款等操作之后,将订单中商品返回到库存
     * @param orderGoods
     */
    void returnGoods(List<WxOrderDetailGoodsVo> orderGoods);

    /**
     * 收货后提交订单售后申请
     * @param marketAftersale: 售后信息
     */
    void submitAftersale(MarketAftersale marketAftersale);

    /**
     * 提交评价内容
     * @param comment
     * @param commentValue
     */
    void comment(Comment comment, CommentSession commentValue);

    /**
     * 订单付款
     * @param orderId
     */
    void prepay(Integer orderId);

    /**
     * 提交新增订单表
     * @param wxOrderSubmitBo
     */
    WxOrderSubmitVo createOrder(WxOrderSubmitBo wxOrderSubmitBo);

    /**
     * 小程序个人中心获取售后信息显示
     * @param status: 售后状态
     * @param page: 当前页数
     * @param limit: 单页限制
     * @return
     */
    WxAftersaleListVo getAftersaleListInfo(Integer status, Integer page, Integer limit);
}
