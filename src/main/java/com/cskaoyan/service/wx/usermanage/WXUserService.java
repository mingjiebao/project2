package com.cskaoyan.service.wx.usermanage;

import com.cskaoyan.model.wx.wxindexbean.LoginIndexDto;
import com.cskaoyan.model.wx.wxindexbean.LoginIndexVo;
import com.cskaoyan.model.wx.wxuserdetailbean.WXUserDetailVo;


public interface WXUserService {
    LoginIndexVo getIndex();


    WXUserDetailVo getUserDetail(Integer id);
}
