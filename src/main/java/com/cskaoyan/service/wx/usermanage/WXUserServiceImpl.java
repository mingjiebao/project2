package com.cskaoyan.service.wx.usermanage;


import com.cskaoyan.model.realm.WxLoginUserData;
import com.cskaoyan.model.wx.wxindexbean.LoginIndexDto;

import com.cskaoyan.dao.wx.homemanage.WXUserDao;

import com.cskaoyan.model.wx.wxindexbean.LoginIndexVo;
import com.cskaoyan.model.wx.wxuserdetailbean.*;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author : [Dragon]
 * @createTime : [2022/1/10 15:01]
 * @description :
 */
@Service
@Transactional
public class WXUserServiceImpl implements WXUserService {

    @Autowired
    WXUserDao wxUserDao;

    @Override
    public LoginIndexVo getIndex() {
        //获取id
        WxLoginUserData principal = (WxLoginUserData) SecurityUtils.getSubject().getPrincipal();
        Integer id = principal.getId();
        //待收货（个数）
        Integer unrecv = wxUserDao.getUnrecv(id);

        //待评价
        Integer uncomment = wxUserDao.getUncomment(id);

        //待付款（个数）
        Integer unpaid = wxUserDao.getunpaid(id);

        //待发货（个数）
        Integer unship = wxUserDao.getunship(id);

        LoginIndexDto loginIndexDto = new LoginIndexDto(unrecv, uncomment, unpaid, unship);
        LoginIndexVo loginIndexVo = new LoginIndexVo(loginIndexDto);
        return loginIndexVo;

    }

    @Override
    public WXUserDetailVo getUserDetail(Integer id) {
        WxLoginUserData principal = (WxLoginUserData) SecurityUtils.getSubject().getPrincipal();
        Integer userId = principal.getId();

        List<SpecificationListVo> specificationListVo = wxUserDao.getSpecificationList(id);
        List<WXDetailAttrubuteVo> wxDetailAttrubuteVo = wxUserDao.getDetailAttribute(id);
        WXDetailGoodsVo wxDetailGoodsVo = wxUserDao.getDetailGoods(id);
        List<WXDetailIssueVo> wxDetailIssueVo = wxUserDao.getDetailIssue();
        List<WXDetailProductVo> wxDetailProductVo = wxUserDao.getDetailProduct(id);
        List<WXUserCommentsVo.DataBean> dataBean = wxUserDao.getDetailComments(id);
        Integer number = wxUserDao.getDetailCommentsCount(id);
        WXUserCommentsVo wxUserCommentsVo = new WXUserCommentsVo(number, dataBean);
        WXUserDetailBrandVo wxUserDetailBrandVo=wxUserDao.getDetailBrand(id);
        Integer  status = wxUserDao.getHasCollect(id,userId);

        WXUserDetailVo wxUserDetailVo=new WXUserDetailVo(status,null,wxUserCommentsVo,false,wxUserDetailBrandVo,wxDetailGoodsVo,specificationListVo,wxDetailIssueVo,wxDetailAttrubuteVo,wxDetailProductVo);

        return wxUserDetailVo;
    }
}
