package com.cskaoyan.service.wx.cart;

import com.cskaoyan.exception.wx.cart.WxCartException;
import com.cskaoyan.model.wx.cart.*;

import java.util.List;

/**
 * @Author:
 * @description:
 * @keypoint:
 * @tags:
 * @related:
 */

public interface WxCartService {
    Integer selectGoodsCountInCart() throws WxCartException;

    Integer insertItemToCart(GoodsAndProductAndNumber goodsAndProductAndNumber,Boolean fastMode) throws WxCartException;

    CartIndexVO selectCartInfo() throws InstantiationException, IllegalAccessException, WxCartException;

    Integer updateCartInfo(CartUpdateBO cartUpdateBO) throws WxCartException;

    Integer updateCartChecked(ProductIdsAndChecked productIdsAndChecked) throws WxCartException;

    Integer deleteCart(List<Integer> productIds1) throws WxCartException;

    CheckOutVO getCheckOutInfo(CheckOut checkOut) throws InstantiationException, IllegalAccessException, WxCartException;
}
