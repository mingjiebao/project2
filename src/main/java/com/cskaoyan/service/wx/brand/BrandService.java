package com.cskaoyan.service.wx.brand;

import com.cskaoyan.model.admin.goodsbean.Brand;
import com.cskaoyan.model.wx.brand.WxBaseParam;
import com.cskaoyan.model.wx.brand.WxBrandListVO;

/**
 * @Author AhaNg
 * @Date 2022/1/11 10:40
 * @description:
 * @return:
 */
public interface BrandService {

    /**
     * @Author: AhaNg
     * @description: wx获取全部brand信息
     * @return:
     */
    WxBrandListVO wxList(WxBaseParam baseParam);


/**
 * @Author: AhaNg
 * @description: wx获取指定brand里的详细信息
 * @return:
 */
    Brand queryBrandById(String id);
}
