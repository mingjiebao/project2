package com.cskaoyan.service.wx.brand;

import com.cskaoyan.dao.goods.BrandMapper;
import com.cskaoyan.model.admin.goodsbean.Brand;
import com.cskaoyan.model.admin.goodsbean.BrandExample;
import com.cskaoyan.model.wx.brand.WxBaseParam;
import com.cskaoyan.model.wx.brand.WxBrandListVO;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author AhaNg
 * @Date 2022/1/11 11:10
 * @description:
 * @return:
 */
@Service
public class BrandServiceImpl implements BrandService{
    @Autowired
    BrandMapper brandMapper;

    /**
     * @Author: AhaNg
     * @description: 获取全部brand的信息
     * @return:
     */
    @Override
    public WxBrandListVO wxList(WxBaseParam baseParam) {
        PageHelper.startPage(baseParam.getPage(),baseParam.getLimit());

        BrandExample brandExample = new BrandExample();
        brandExample.createCriteria().andDeletedEqualTo(false);
        List<Brand> brands=brandMapper.selectByExample(brandExample);

        PageInfo pageInfo = new PageInfo(brands);
        long total=pageInfo.getTotal();
        WxBrandListVO wxBrandListVO = new WxBrandListVO();
        wxBrandListVO.setTotal(total);
        wxBrandListVO.setList(brands);
        wxBrandListVO.setPages((int) (total/(baseParam.getLimit()))+1);
        return wxBrandListVO;
    }

    /**
     * @Author: AhaNg
     * @description: 获取指定brand里的详细信息
     * @return:
     */
    @Override
    public Brand queryBrandById(String id) {
        Brand brand=brandMapper.selectByPrimaryKey(Integer.parseInt(id));
        return brand;
    }
}
