package com.cskaoyan.service.wx.footprint;

import com.cskaoyan.model.wx.footprint.vo.FootPrintListVo;

public interface FootPrintService {
    FootPrintListVo getFootPrintList(Integer page, Integer limit);

    void addFootPrint(Integer userId,Integer goodsId);

    void deleteFootPrint(Integer footId);

}
