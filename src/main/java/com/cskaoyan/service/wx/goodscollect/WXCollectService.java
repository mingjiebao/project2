package com.cskaoyan.service.wx.goodscollect;

import com.cskaoyan.model.wx.wxcollect.bo.CollectAddorDeleteBo;
import com.cskaoyan.model.wx.wxcollect.vo.WXCollectListVo;


/**
 * @description:
 * @author: XM
 * @date: 2022/1/10 15:03
 */
public interface WXCollectService {

    WXCollectListVo collectList(Integer type, Integer page, Integer limit);

    Integer collectAddordelete(CollectAddorDeleteBo collectAddorDeleteBo);

}
