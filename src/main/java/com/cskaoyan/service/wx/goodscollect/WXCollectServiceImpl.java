package com.cskaoyan.service.wx.goodscollect;

import com.cskaoyan.dao.wx.collect.WXCollectDao;

import com.cskaoyan.model.wx.wxcollect.bo.CollectAddorDeleteBo;

import com.cskaoyan.model.realm.WxLoginUserData;

import com.cskaoyan.model.wx.wxcollect.bo.UpdateDeleted;


import com.cskaoyan.model.wx.wxcollect.vo.WXCollectListVo;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @description:
 * @author: XM
 * @date: 2022/1/10 15:04
 */
@Component
public class WXCollectServiceImpl implements WXCollectService {

    @Autowired
    WXCollectDao collectDao;

    @Override
    public WXCollectListVo collectList(Integer type, Integer page, Integer limit) {
        WxLoginUserData principal = (WxLoginUserData) SecurityUtils.getSubject().getPrincipal();
        Integer id = principal.getId();

        PageHelper.startPage(page, limit);
        List<WXCollectListVo.ListBean> listBeans = collectDao.selectCollectList(type, id);
        PageInfo<WXCollectListVo.ListBean> pageInfo = new PageInfo<>(listBeans);
        int pages = pageInfo.getPages();
        long total = pageInfo.getTotal();
        return new WXCollectListVo((int) total, pages, limit, page, listBeans);
    }

    @Override
    public Integer collectAddordelete(CollectAddorDeleteBo collectAddorDeleteBo) {
        WxLoginUserData principal = (WxLoginUserData) SecurityUtils.getSubject().getPrincipal();
        Integer id = principal.getId();
/*
        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd : hh:mm:ss");
        String format = dateFormat.format(date);
        updateDeleted.setUpdateTime(format);*/
        //根据商品id查找收藏表中是否有收藏记录
        UpdateDeleted updateDeleted = collectDao.selectDelectedStatus(collectAddorDeleteBo, id);

        //如果id=null说明收藏表没有数据，进行收藏表的添加
        if(updateDeleted==null){
           Integer number=collectDao.insertCollect(collectAddorDeleteBo,id);
           return 1;
        }else {
           Integer result = collectDao.updateDeleted(updateDeleted);
            return 1;
        }

    }
}