package com.cskaoyan.service.wx.Shippingaddress;

import com.cskaoyan.model.wx.wxadressbean.bo.WXAddressSaveBo;
import com.cskaoyan.model.wx.wxadressbean.vo.WXAddressDetailVo;
import com.cskaoyan.model.wx.wxadressbean.vo.WXAdressList;

/**
 * @description:
 * @author: XM
 * @date: 2022/1/10 20:18
 */


public interface AddressService {
    WXAdressList addressList();

    WXAddressDetailVo getAdressDetail(Integer id);

    Integer addressSaveMsg(WXAddressSaveBo addressSaveBo);

    Integer addressDelete(Integer id);
}