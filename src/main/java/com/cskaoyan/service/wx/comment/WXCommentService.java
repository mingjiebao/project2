package com.cskaoyan.service.wx.comment;

import com.cskaoyan.model.wx.comment.bo.CommentCountBO;
import com.cskaoyan.model.wx.comment.bo.CommentListBO;
import com.cskaoyan.model.wx.comment.bo.CommentPostBO;
import com.cskaoyan.model.wx.comment.vo.CommentCountVO;
import com.cskaoyan.model.wx.comment.vo.CommentListVO;
import com.cskaoyan.model.wx.comment.vo.CommentPostVO;

public interface WXCommentService {

    //评论显示
    CommentListVO commentListInfo(CommentListBO commentListBO);

    //上传评论
    CommentPostVO commentPost(CommentPostBO commentPostBO);

    //评论数量
    CommentCountVO commentCount(CommentCountBO commentCountBO);
}
