package com.cskaoyan.service.wx.comment;

import com.cskaoyan.dao.wx.comment.WXCommentDao;
import com.cskaoyan.model.realm.WxLoginUserData;
import com.cskaoyan.model.wx.comment.bo.CommentCountBO;
import com.cskaoyan.model.wx.comment.bo.CommentListBO;
import com.cskaoyan.model.wx.comment.bo.CommentPostBO;
import com.cskaoyan.model.wx.comment.vo.*;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

@Service
@Transactional
public class WXCommentServiceImpl implements WXCommentService {

    @Autowired
    WXCommentDao wxCommentDao;

    @Override
    //评论显示
    public CommentListVO commentListInfo(CommentListBO commentListBO) {
        //新建JavaBean作为数据容器
        CommentListVO commentListVO = new CommentListVO();
        //查询评论总数量
        Integer total = wxCommentDao.selectCommentListTotal(commentListBO.getValueId());
        //查询详细的数据信息
        List<CommentListContentVO> commentListContentVOS = wxCommentDao.selectCommentListContent(commentListBO.getValueId());
        ListIterator<CommentListContentVO> listIterator = commentListContentVOS.listIterator();
        while (listIterator.hasNext()) {
            CommentListContentVO listContentVO = listIterator.next();
            //查找用户信息
            CommentListContentUserVO commentListContentUserVO = wxCommentDao.selectCommentListUserContent(listContentVO.getUserId());
            listContentVO.setUserInfo(commentListContentUserVO);
        }
        //封装数据
        commentListVO.setList(commentListContentVOS);
        commentListVO.setLimit(commentListBO.getLimit());
        commentListVO.setPage(commentListBO.getPage());
        Integer pages = total / commentListVO.getLimit() + 1;
        commentListVO.setPages(pages);
        commentListVO.setTotal(total);
        return commentListVO;
    }

    @Override
    public CommentPostVO commentPost(CommentPostBO commentPostBO) {
        //将接收的数据装入新建的容器
        CommentPostVO commentPostVO = new CommentPostVO();
        //获取用户id
        WxLoginUserData principal = (WxLoginUserData) SecurityUtils.getSubject().getPrincipal();
        Integer userId = principal.getId();
        //封装数据
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = simpleDateFormat.format(new Date());
        commentPostVO.setAddTime(date);
        commentPostVO.setUpdateTime(date);
        commentPostVO.setContent(commentPostBO.getContent());
        commentPostVO.setHasPicture(commentPostBO.getHasPicture());
        commentPostVO.setStar(commentPostBO.getStar());
        commentPostVO.setValueId(Integer.parseInt(commentPostBO.getValueId()));
        commentPostVO.setType(commentPostBO.getType());
        commentPostVO.setUserId(userId);
        //将数据插入数据库
        Integer id = wxCommentDao.insertCommentPost(commentPostVO);
        //封装传回的id
        commentPostVO.setId(id);
        return commentPostVO;
    }

    @Override
    public CommentCountVO commentCount(CommentCountBO commentCountBO) {
        //新建JavaBean作为数据容器
        CommentCountVO commentCountVO = new CommentCountVO();
        //查询总数量
        Integer allCount = wxCommentDao.selectCommentAllCount(commentCountBO);
        Integer hasPicCount = wxCommentDao.selectCommentHasPicCount(commentCountBO);
        //封装数据
        commentCountVO.setAllCount(allCount);
        commentCountVO.setHasPicture(hasPicCount);
        return commentCountVO;
    }
}
