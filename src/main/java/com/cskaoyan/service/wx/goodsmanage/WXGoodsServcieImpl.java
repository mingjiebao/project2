package com.cskaoyan.service.wx.goodsmanage;

import com.cskaoyan.dao.user.UserSearchHistoryDao;
import com.cskaoyan.dao.wx.homemanage.WXGoodsDao;
import com.cskaoyan.model.realm.WxLoginUserData;
import com.cskaoyan.model.wx.wxgoodsbean.bo.WxGoodsListBo;
import com.cskaoyan.model.wx.wxgoodsbean.vo.WXGoodsCategoryVo;
import com.cskaoyan.model.wx.wxgoodsbean.vo.WXGoodsListVo;
import com.cskaoyan.model.wx.wxgoodsbean.vo.WXGoodsRelatedVo;
import com.cskaoyan.model.wx.wxuserdetailbean.*;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author : [Dragon]
 * @createTime : [2022/1/10 14:53]
 * @description :
 */
@Service
public class WXGoodsServcieImpl implements WXGoodsService {

    @Autowired
    WXGoodsDao wxGoodsDao;

    @Autowired
    UserSearchHistoryDao searchHistoryDao;

    @Override
    public WXGoodsCategoryVo getCategoryMsg(Integer id) {
        WXGoodsCategoryVo.CurrentCategoryBean currentCategory = wxGoodsDao.selectCurrentCategory(id);
        Integer pid = currentCategory.getPid();
        WXGoodsCategoryVo.ParentCategoryBean parentCategory = wxGoodsDao.selectParentCategory(pid);
        List<WXGoodsCategoryVo.BrotherCategoryBean> brotherCategorys = wxGoodsDao.selectBrotherCategory(pid);
        return new WXGoodsCategoryVo(currentCategory, parentCategory, brotherCategorys);
    }

    @Override
    public WXGoodsListVo getGoodsList(Integer categoryId, Integer brandId, Integer page, Integer limit) {
        /*Integer page = baseParam.getPage();
        Integer limit = baseParam.getLimit();
        PageHelper.startPage(page, limit);

        List<BrandMsgDto> brandMsgList = marketDao.getAllBrandMsgList(baseParam, id, name);

        PageInfo<BrandMsgDto> pageInfo = new PageInfo<>(brandMsgList);
        int pages = pageInfo.getPages();
        long total = pageInfo.getTotal();*/

        PageHelper.startPage(page, limit);
        List<WXGoodsListVo.ListBean> listBeans = wxGoodsDao.selectGoodsByCategoryId(brandId, categoryId);
        PageInfo<WXGoodsListVo.ListBean> pageInfo = new PageInfo<>(listBeans);
        int pages = pageInfo.getPages();
        int total = (int) pageInfo.getTotal();
        List<WXGoodsListVo.FilterCategoryListBean> filterCategoryListBeans = wxGoodsDao.selectCategoryAll();
        return new WXGoodsListVo(total, pages, limit, page, listBeans, filterCategoryListBeans);
    }

    @Override
    public WXGoodsRelatedVo getGoodsRelated(Integer id) {
        Integer i = wxGoodsDao.selectRelatedCount(id);
        List<WXGoodsRelatedVo.ListBean> listBeans = wxGoodsDao.selectGoodsRelated(id);
        return new WXGoodsRelatedVo(i, 1, 6, 1, listBeans);
    }

    @Override
    public Integer getGoodsCount() {
        Integer i = wxGoodsDao.selectGoodsCount();
        return i;
    }

    /***
     *
     * @param bo:通过keyword拿到对应的商品Id,通过商品ID在拿到对应的GoodList,list查询没有结果时返回的是
     *          空list不是Null
     * @return
     */
    @Override
    public WXGoodsListVo getGoodsListKeyword(WxGoodsListBo bo) {
        List<Integer> goodsId = wxGoodsDao.getKeywordGoodsId(bo.getKeyword());
        List<WXGoodsListVo.ListBean> listBeans = null;
        int pages = 0;
        int total = 0;

        if (goodsId != null && goodsId.size() != 0) {
            PageHelper.startPage(bo.getPage(), bo.getLimit());
            listBeans = wxGoodsDao.selectGoodsByKeword(goodsId);
            PageInfo<WXGoodsListVo.ListBean> pageInfo = new PageInfo<>(listBeans);
            pages = pageInfo.getPages();
            total = (int) pageInfo.getTotal();
        }

        List<WXGoodsListVo.FilterCategoryListBean> filterCategoryListBeans = wxGoodsDao.selectCategoryAll();
        return new WXGoodsListVo(total, pages, bo.getLimit(), bo.getPage(), listBeans, filterCategoryListBeans);

    }

    @Override
    public void addSearchHistory(WxGoodsListBo bo) {
        String keyword = bo.getKeyword();
        WxLoginUserData principal = (WxLoginUserData) SecurityUtils.getSubject().getPrincipal();
        Integer userId = principal.getId();

        List<String> list = searchHistoryDao.getAllUserSearchInfo(userId);
        if (list.contains(keyword)) {
            searchHistoryDao.updateHistorySearch(keyword, userId);
            return;
        }
        if (list.size() >= 10) {
            String del = list.get(list.size() - 1);
            searchHistoryDao.deleteOneHistory(del, userId);
        }
        searchHistoryDao.insetNewSearch(keyword, userId);
    }

    @Override
    public WXGoodsListVo getGoodsListHot(WxGoodsListBo bo) {

        List<WXGoodsListVo.ListBean> listBeans;

        PageHelper.startPage(bo.getPage(), bo.getLimit());
        // 不能有种类为0字段的属性存在
        if(bo.getCategoryId() == 0){
            bo.setCategoryId(null);
        }
        listBeans = wxGoodsDao.selectHotOrNew(bo);
        PageInfo<WXGoodsListVo.ListBean> pageInfo = new PageInfo<>(listBeans);
        int pages = pageInfo.getPages();
        int total = (int) pageInfo.getTotal();


        List<WXGoodsListVo.FilterCategoryListBean> filterCategoryListBeans = wxGoodsDao.selectCategoryAll();
        return new WXGoodsListVo(total, pages, bo.getLimit(), bo.getPage(), listBeans, filterCategoryListBeans);
    }
}
