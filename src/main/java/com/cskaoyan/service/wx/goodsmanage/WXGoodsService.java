package com.cskaoyan.service.wx.goodsmanage;

import com.cskaoyan.model.wx.wxgoodsbean.bo.WxGoodsListBo;
import com.cskaoyan.model.wx.wxgoodsbean.vo.WXGoodsCategoryVo;
import com.cskaoyan.model.wx.wxgoodsbean.vo.WXGoodsListVo;
import com.cskaoyan.model.wx.wxgoodsbean.vo.WXGoodsRelatedVo;
import com.cskaoyan.model.wx.wxuserdetailbean.WXUserDetailVo;

public interface WXGoodsService {
    WXGoodsCategoryVo getCategoryMsg(Integer id);

    WXGoodsListVo getGoodsList(Integer categoryId,Integer brandId, Integer page, Integer limit);

    WXGoodsRelatedVo getGoodsRelated(Integer id);

    Integer getGoodsCount();

    WXGoodsListVo getGoodsListKeyword(WxGoodsListBo bo);

    void addSearchHistory(WxGoodsListBo bo);

    WXGoodsListVo getGoodsListHot(WxGoodsListBo bo);
}
