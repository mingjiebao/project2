package com.cskaoyan.service.wx.helper;

import com.cskaoyan.model.PageLimitRepsVo;

public interface HelperService {
    PageLimitRepsVo getHelperIssueInfo(Integer page, Integer limit);
}
