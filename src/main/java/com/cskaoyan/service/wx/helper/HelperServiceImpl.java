package com.cskaoyan.service.wx.helper;


import com.cskaoyan.dao.helper.MarketIssueDao;
import com.cskaoyan.model.PageLimitRepsVo;
import com.cskaoyan.model.wx.helper.HelperListVo;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HelperServiceImpl implements HelperService {

    @Autowired
    MarketIssueDao issueDao;

    @Override
    public PageLimitRepsVo getHelperIssueInfo(Integer page, Integer limit) {
        PageHelper.startPage(page, limit);
        List<HelperListVo> resultList = issueDao.getIssueListInfo();
        PageInfo<HelperListVo> info = new PageInfo<>(resultList);

        PageLimitRepsVo pageLimitRepsVo = new PageLimitRepsVo();
        pageLimitRepsVo.setLimit(limit);
        pageLimitRepsVo.setPage(page);
        pageLimitRepsVo.setPages(info.getPages());
        pageLimitRepsVo.setTotal((int) info.getTotal());
        pageLimitRepsVo.setList(resultList);
        return pageLimitRepsVo;
    }
}
