package com.cskaoyan.service.wx.feedback;

import com.cskaoyan.model.wx.feedback.bo.FeedBackSubmitBo;

public interface FeedBackService {
    void submitFeedBack(FeedBackSubmitBo bo, String username, Integer id);
}
