package com.cskaoyan.service.wx.feedback;

import com.cskaoyan.dao.feedback.UserFeedBackDao;
import com.cskaoyan.model.wx.feedback.bo.FeedBackSubmitBo;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.SessionAttribute;

@Service
public class FeedBackServiceImpl implements FeedBackService {

    @Autowired
    UserFeedBackDao feedBackDao;


    @Transactional
    @Override
    public void submitFeedBack(FeedBackSubmitBo bo, String username, Integer id) {
        feedBackDao.submitFeedBack(bo, username, id);
    }
}
