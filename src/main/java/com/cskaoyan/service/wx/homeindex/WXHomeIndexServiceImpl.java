package com.cskaoyan.service.wx.homeindex;


import com.cskaoyan.dao.configmanage.ConfigDao;
import com.cskaoyan.dao.wx.homemanage.WXHomeIndexDao;
import com.cskaoyan.model.admin.configmanage.pojo.SystemConfigPojo;
import com.cskaoyan.model.wx.wxcoupon.vo.WxCouponVo;
import com.cskaoyan.model.wx.wxhomeindexbean.*;
import com.cskaoyan.service.wx.coupon.WXCouponService;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @description:
 * @author: XM
 * @date: 2022/1/11 4:11
 */
@RestController
@Transactional
public class WXHomeIndexServiceImpl implements WXHomeIndexservice {
    @Autowired
    WXHomeIndexDao wxHomeIndexDao;

    @Autowired
    WXCouponService service;

    @Autowired
    ConfigDao configDao;

    /***
     *      newGoodsList  -->  market_wx_index_new
     *      hotGoodsList  -->  market_wx_index_hot
     *      brandList     -->  market_wx_index_brand
     *      topicList     -->  market_wx_index_topic
     *      floorGoodsList --> market_wx_catlog_list
     *      floorGoodsList中的goodsList  ->market_wx_catlog_goods
     * @return
     */
    @Override
    public WXHomeIndexVo homeIndex() {

        List<SystemConfigPojo> systemConfig = configDao.selectWxConfig();
        Integer newGoodsListLimit = null,
                hotGoodsListLimit = null,
                brandListLimit = null,
                topicListLimit = null,
                floorGoodsListLimit = null,
                goodsListLimit = null;
        int limit;
        for (SystemConfigPojo item : systemConfig) {
            if (item.getConfigName().trim().equals("market_wx_index_new")) {
                limit = Integer.parseInt(item.getConfigValue());
                newGoodsListLimit = limit == 0 ? null : limit;
            }
            if (item.getConfigName().trim().equals("market_wx_index_hot")) {
                limit = Integer.parseInt(item.getConfigValue());
                hotGoodsListLimit = limit == 0 ? null : limit;
            }
            if (item.getConfigName().trim().equals("market_wx_index_brand")) {
                limit = Integer.parseInt(item.getConfigValue());
                brandListLimit = limit == 0 ? null : limit;
            }
            if (item.getConfigName().trim().equals("market_wx_index_topic")) {
                limit = Integer.parseInt(item.getConfigValue());
                topicListLimit = limit == 0 ? null : limit;
            }
            if (item.getConfigName().trim().equals("market_wx_catlog_list")) {
                limit = Integer.parseInt(item.getConfigValue());
                floorGoodsListLimit = limit == 0 ? null : limit;
            }
            if (item.getConfigName().trim().equals("market_wx_catlog_goods")) {
                limit = Integer.parseInt(item.getConfigValue());
                goodsListLimit = limit == 0 ? null : limit;
            }
        }


        List<WXActivityVo> activityVos = wxHomeIndexDao.selectActivity();



        List<WXBrandListVo> brandListVos = wxHomeIndexDao.selectBrand(brandListLimit);

        PageHelper.startPage(1, 10);
        List<WXChannelVo> channelVos = wxHomeIndexDao.selectChannel();
        List<WxCouponVo> CouponListVos = service.getHomeCouponList();


        List<WXHotGoodsListVo> hotGoodsListVos = wxHomeIndexDao.selectHotGoods(hotGoodsListLimit);
        List<WXNewGoodsListVo> newGoodsListVos = wxHomeIndexDao.selectWXNewGoodsList(newGoodsListLimit);
        List<WXFloorGoodsListVo> floorGoodsListVos = wxHomeIndexDao.selectWXFloorGoodsList(floorGoodsListLimit);

        // floorGoodsList。goodsList  --> goodsListLimit  限制floorGoodsListVos中每一个
        //  GoodsList的数量
        Integer finalGoodsListLimit = goodsListLimit == null ? -1 : goodsListLimit;
        if (floorGoodsListVos != null) {
            floorGoodsListVos.forEach(x -> {
                if (x.getGoodsList() != null) {
                    if (x.getGoodsList().size() <= finalGoodsListLimit) {
                        x.setGoodsList(x.getGoodsList().subList(0, x.getGoodsList().size()));
                    } else {
                        x.setGoodsList(x.getGoodsList().subList(0, finalGoodsListLimit));
                    }
                }
            });
        }

        List<WXTopicListVo> topicListVos = wxHomeIndexDao.selectTopic(topicListLimit);


        WXHomeIndexVo wxHomeIndexVo = new WXHomeIndexVo(newGoodsListVos, CouponListVos, channelVos, activityVos, brandListVos, hotGoodsListVos, topicListVos, floorGoodsListVos);
        return wxHomeIndexVo;
    }
}