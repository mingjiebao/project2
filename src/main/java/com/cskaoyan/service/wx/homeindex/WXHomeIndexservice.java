package com.cskaoyan.service.wx.homeindex;

import com.cskaoyan.model.wx.wxhomeindexbean.WXHomeIndexVo;

/**
 * @description:
 * @author: XM
 * @date: 2022/1/11 4:11
 */
public interface WXHomeIndexservice {
    WXHomeIndexVo homeIndex();
}
