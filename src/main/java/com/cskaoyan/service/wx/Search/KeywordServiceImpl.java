package com.cskaoyan.service.wx.Search;

import com.cskaoyan.dao.user.UserSearchHistoryDao;
import com.cskaoyan.dao.wx.search.KeywordMapper;
import com.cskaoyan.dao.wx.search.SearchHistoryMapper;
import com.cskaoyan.model.BaseParam;
import com.cskaoyan.model.wx.search.Keyword;
import com.cskaoyan.model.wx.search.KeywordExample;
import com.cskaoyan.model.wx.search.SearchHistory;
import com.cskaoyan.model.wx.search.SearchHistoryExample;
import com.cskaoyan.model.wx.search.vo.SearchIndexVO;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author AhaNg
 * @Date 2022/1/11 22:58
 * @description:
 * @return:
 */
@Service
public class KeywordServiceImpl implements KeywordService {
    @Autowired
    KeywordMapper keywordMapper;

    @Autowired
    SearchHistoryMapper searchHistoryMapper;



    /**
     * @Author: bao
     * @description: 首页推荐重构
     * @return:
     */
    @Override
    public SearchIndexVO searchIndex(Integer userId) {
        //获取默认关键字
        Keyword defaultKey=keywordMapper.selectDefaultKeyWord();


        //获取热点关键字
        List<Keyword> hotKeys = keywordMapper.selectHotKeyWords(6);

        //获取个人搜索历史
        //先判断userid是不是null
        List<SearchHistory> searchHistories = null;
        if (userId != null) {
            searchHistories = searchHistoryMapper.selectUserHistory(userId,8);
        }

        SearchIndexVO searchIndexVO = new SearchIndexVO();
        searchIndexVO.setDefaultKeyword(defaultKey);
        searchIndexVO.setHotKeywordList(hotKeys);
        searchIndexVO.setHistoryKeywordList(searchHistories);
        return searchIndexVO;
    }

    /**
     * @Author: AhaNg
     * @description: wx搜索栏关键字提示
     * @return:
     */
    @Override
    public List<String> helper(String keyword) {
        KeywordExample keywordExample = new KeywordExample();
        keywordExample.createCriteria().andKeywordLike("%" + keyword + "%")
                .andDeletedEqualTo(false);

        List<Keyword> keywords = keywordMapper.selectByExample(keywordExample);
        List<String> keywordList = new ArrayList<>();
        for (Keyword keyword1 : keywords) {
            keywordList.add(keyword1.getKeyword());
        }
        return keywordList;
    }

    /**
     * @Author: AhaNg
     * @description: 逻辑删除用户的搜索历史
     * @return:
     */
    @Transactional
    @Override
    public void clearHistory(Integer userId) {
        searchHistoryMapper.deleteByUserId(userId);
    }
}
