package com.cskaoyan.service.wx.Search;

import com.cskaoyan.model.BaseParam;
import com.cskaoyan.model.wx.search.vo.SearchIndexVO;

import java.util.List;

/**
 * @Author AhaNg
 * @Date 2022/1/11 22:48
 * @description:
 * @return:
 */
public interface KeywordService {

    /**
     * @Author: AhaNg
     * @description: wx搜索首页显示
     * @return:
     */
    SearchIndexVO searchIndex(Integer userId);

    /**
     * @Author: AhaNg
     * @description: wx搜索栏关键字提示
     * @return:
     */
    List<String> helper(String keyword);

    /**
     * @Author: AhaNg
     * @description: 逻辑删除用户的搜索信息
     * @return:
     */
    void clearHistory(Integer id);
}
