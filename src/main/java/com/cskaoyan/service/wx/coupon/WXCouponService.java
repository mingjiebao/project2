package com.cskaoyan.service.wx.coupon;

import com.cskaoyan.model.BaseData;
import com.cskaoyan.model.wx.wxcoupon.bo.CouponBaseBo;
import com.cskaoyan.model.wx.wxcoupon.vo.CouponBaseVo;
import com.cskaoyan.model.wx.wxcoupon.vo.WxCouponVo;

import java.util.List;

public interface WXCouponService {
    BaseData getCouponListInfo(Integer limit, Integer page);

    List<WxCouponVo> getHomeCouponList();


    boolean receiveCoupon(int couponId);

    CouponBaseVo getCouponMyList(CouponBaseBo info);

    boolean exchangeCoupon(String code, Integer userId);

    BaseData getCouponSelectList(Integer cartId, Integer grouponRulesId);
}
