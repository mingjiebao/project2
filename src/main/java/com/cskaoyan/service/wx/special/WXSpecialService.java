package com.cskaoyan.service.wx.special;

import com.cskaoyan.model.wx.special.bo.SpecialBO;
import com.cskaoyan.model.wx.special.vo.SpecialDetailVO;
import com.cskaoyan.model.wx.special.vo.SpecialListVO;
import com.cskaoyan.model.wx.special.vo.SpecialRelatedVO;

public interface WXSpecialService {

    //专题精选 页面数据显示
    SpecialListVO specialListInfo(SpecialBO specialBO);

    //专题精选 专题详情
    SpecialDetailVO specialDetail(Integer id);

    //专题精选 专题相关推荐
    SpecialRelatedVO specialRelated(Integer id);
}
