package com.cskaoyan.service.wx.special;

import com.cskaoyan.dao.wx.special.WXSpecialDao;
import com.cskaoyan.model.wx.special.bo.SpecialBO;
import com.cskaoyan.model.wx.special.vo.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class WXSpecialServiceImpl implements WXSpecialService {

    @Autowired
    WXSpecialDao wxSpecialDao;

    @Override
    //专题精选 页面数据显示
    public SpecialListVO specialListInfo(SpecialBO specialBO) {
        //新建Javabean作为数据容器
        SpecialListVO specialListVO = new SpecialListVO();
        //新建List接收查表后返回的数据
        List<SpecialListContentVO> specialListContentVOS = wxSpecialDao.selectSpecialListContent();
        //查询数据库表中数据的数量
        Integer total = wxSpecialDao.selectSpecialListTotal();
        //计算总页面数
        Integer pages = total / specialBO.getLimit() + 1;
        //封装数据
        specialListVO.setLimit(specialBO.getLimit());
        specialListVO.setPage(specialBO.getPage());
        specialListVO.setPages(pages);
        specialListVO.setTotal(total);
        specialListVO.setList(specialListContentVOS);
        return specialListVO;
    }

    @Override
    //专题精选 专题详情
    public SpecialDetailVO specialDetail(Integer id) {
        //新建Javabean作为数据容器
        SpecialDetailVO specialDetailVO = new SpecialDetailVO();
        List<SpecialDetailGoodsVO> specialDetailGoodsVOS = new ArrayList<>();
        //查询数据返回给VO对象
        SpecialDetailTopicVO specialDetailTopicVO = wxSpecialDao.selectSpecialDetailTopic(id);
        //处理market_topic表中返回的goods数据
        String temp1 = specialDetailTopicVO.getGoods();
        //去掉[],并用","分割字符串层单个goodsSn，进行维持查表
        String temp2 = temp1.substring(1, temp1.length() - 1);
        if (!temp2.equals("")){
            String[] goodsSn = temp2.split(",");
            for (String s : goodsSn) {
                SpecialDetailGoodsVO specialDetailGoodsVO = wxSpecialDao.selectSpecialDetailGoods(s);
                specialDetailGoodsVOS.add(specialDetailGoodsVO);
            }
        }
        //封装数据
        specialDetailVO.setGoods(specialDetailGoodsVOS);
        specialDetailVO.setTopic(specialDetailTopicVO);
        return specialDetailVO;
    }

    @Override
    //专题精选 专题相关推荐
    public SpecialRelatedVO specialRelated(Integer id) {
        //新建Javabean作为数据容器
        SpecialRelatedVO specialRelatedVO = new SpecialRelatedVO();
        List<SpecialRelatedContentVO> specialRelatedContentVOS = new ArrayList<>();
        //先查询相关商品id
        String tempGoodsId = wxSpecialDao.selectSpecialRelatedGoodsId(id);
        if (!tempGoodsId.equals("[]")){
            //处理goods字符串
            String[] goodsId = tempGoodsId.substring(1, tempGoodsId.length() - 1).split(",");
            for (String i : goodsId) {
                Integer relatedId = Integer.parseInt(i);
                //新建List接收查表后返回的数据
                SpecialRelatedContentVO specialRelatedContentVO = wxSpecialDao.selectSpecialRelatedContent(relatedId);
                specialRelatedContentVOS.add(specialRelatedContentVO);
            }
        }
        //查询数据库表中数据的数量
        Integer total = wxSpecialDao.selectSpecialListTotal();
        //计算总页面数
        Integer limit = 4;
        Integer pages = total / limit + 1;
        //封装数据
        specialRelatedVO.setLimit(limit);
        specialRelatedVO.setPage(1);
        specialRelatedVO.setPages(pages);
        specialRelatedVO.setTotal(total);
        specialRelatedVO.setList(specialRelatedContentVOS);
        return specialRelatedVO;
    }
}
