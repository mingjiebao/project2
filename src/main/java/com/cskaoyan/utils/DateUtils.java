package com.cskaoyan.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @Author:
 * @description:
 * @keypoint:
 * @tags:
 * @related:
 */

public class DateUtils {

    /**
     * 返回当前的时间
     * @Author Yeah
     * @return
     */
    public static String getNowDateTime(){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return simpleDateFormat.format(new Date(System.currentTimeMillis()));
    }

    /**
     *
     * @return Date 类型
     */
    public static Date getNowDate(){
        return new Date(System.currentTimeMillis());
    }
    public static Date getFutureDate(long time){
        return new Date(System.currentTimeMillis()+time);
    }
    public static String getFutureDateTime(long time){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return simpleDateFormat.format(new Date(System.currentTimeMillis()+time));
    }
}
