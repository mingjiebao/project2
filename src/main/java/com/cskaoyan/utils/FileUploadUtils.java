package com.cskaoyan.utils;

import com.cskaoyan.model.FileUploadDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.UUID;

/**
 * @Author: cybertheye
 * @description:
 * @keypoint:
 * @tags:
 * @related:
 */
//@Component
public class FileUploadUtils {


    /**
     * 文件上传的工具类
     * 保存总目录在static下面，自己模块的文件上传就新建一个自己模块的目录，比如系统管理模块中有文件上传的功能在static下新建一个adminmanage
     * /static/adminmanage
     *
     * @param mf          MultipartFile对象
     * @param module_path 例如系统管理模块，就传入"adminmanage"
     * @return FileUploadDTO {@link FileUploadDTO} 返回FileUpload类，包含key,name，type，size，url
     * ！！！如果有其他需求，自己解决！！！
     * 注意： 返回 例如 /adminmanage/.../xxxx.jpg ， yml中没有做静态资源映射，默认访问static下
     * @pram uploadpath hash表， 通过获取当前的系统，来决定使用哪个路径
     */

//    @Value("${upload.absolutePath}")
//    static String absolutePath;
    public static FileUploadDTO upload(MultipartFile mf, HashMap<String, String> uploadPath, String module_path) throws IOException {


        String fileNameAndType = mf.getOriginalFilename(); //这个获取的是原来的文件名 yellow.jpg
        Long fileSize = mf.getSize();
        String[] split = fileNameAndType.split("\\."); //["yello","jpg"]
        String fileName = split[0]; //"yello"
        String fileType = split[1]; //"jpg"


        //通过UUID 解决文件不重名问题
//        UUID uuid = new UUID();
        String key = UUID.randomUUID().toString();
        fileName = UUID.randomUUID().toString() + "-" + fileName;

        //通过hash解决文件散列问题，均匀
        int hashCode = fileName.hashCode();
        String hexString = Integer.toHexString(hashCode);
        char[] chars = hexString.toCharArray();
//        String basePath="upload/files/"; //通过配置文件

        String relativePath = module_path + "/";

        for (char aChar : chars) { //将每一位的16进制当作一个目录
            relativePath = relativePath + aChar + "/";
        }

        String os = System.getProperty("os.name").toLowerCase();
        String basePath = null;
        if (os.contains("mac")) {
            basePath = uploadPath.get("mac");
        } else {
            basePath = uploadPath.get("windows");
        }

        fileNameAndType = fileName + "." + fileType;

        /*
        basePath: 不同系统的保存根路径
        relativePath： 在根路径下，文件散列的路径
        fileNameAndType： UUID+文件
         */

        String fileRealUrl = basePath + relativePath + fileNameAndType;

        File file = new File(fileRealUrl);

        //目录不存在要创建
        if (!file.getParentFile().exists()) {
            file.getParentFile().mkdirs();
        }


        mf.transferTo(file);

        FileUploadDTO fileUploadDTO = FileUploadDTO.create(key, fileName, fileType, fileSize, relativePath + fileNameAndType);
        return fileUploadDTO;
    }
}
