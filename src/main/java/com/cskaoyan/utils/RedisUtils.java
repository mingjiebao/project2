package com.cskaoyan.utils;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * 首先需要打开redis服务，用户名和密码不用设置，一定要有RedisConfig文件，Object类型的要序列化，序列化使用的
 * 是jackSon,不然的话需要每一个bo,vo等实现序列化接口
 * <p>
 * >>>>>>>>   key 默认是String类型
 * >>>>>>>>   host: localhost或127.0.0.1
 * >>>>>>>>   port: 6379
 */

@Component
public class RedisUtils {

    final RedisTemplate<String, Object> redisTemplate;

    public RedisUtils(RedisTemplate<String, Object> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    /**
     * 检查redis是否存在key
     */
    public boolean hasKey(String key) {
        return redisTemplate.hasKey(key);
    }

    /**
     * 设置key值对应的缓存失效时间
     *
     * @param time:秒数
     */
    public boolean setExpire(String key, long time) {
        if (!hasKey(key) || time < 0) {
            return false;
        }

        try {
            redisTemplate.expire(key, time, TimeUnit.SECONDS);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * @param key: KEY-VALUE形式的根据key拿到对应的value
     * @return :返回对应的序列化后的object
     */
    public Object getStringStructOfValue(String key) {
        return redisTemplate.opsForValue().get(key);
    }

    /**
     * @param :key
     * @param :value
     */
    public void setStringStructOfValue(String key, Object value) {
        redisTemplate.opsForValue().set(key, value);
    }

    /**
     * 其他等用到再补充，目前只有基本的String数据类型。  key-value 类型。
     */
}
