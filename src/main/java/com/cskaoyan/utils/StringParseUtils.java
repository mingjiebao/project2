package com.cskaoyan.utils;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class StringParseUtils {
    private StringParseUtils() {
    }

    public static Integer[] getIntArrFromString(String s) {
        if (s == null || s.length() <= 2) {
            return null;
        }


        List<Integer> list = new ArrayList<>();
        for (String integer : s.substring(1, s.length() - 1).split(",")) {
            list.add(Integer.parseInt(integer.trim()));
        }
        return list.toArray(new Integer[0]);
    }

    public static String[] getStringArr(String s) {
        if (s == null || s.length() <= 2) {
            return new String[0];
        }

        String[] temp = s.substring(1, s.length() - 1).split(",");
        return temp;
    }

    public static String getStringFromArr(String[] arr) {
        if (arr == null || arr.length == 0) {
            return null;
        }
        return Arrays.toString(arr);
    }

    public static String getStringEmpty(String[] arr) {
        if (arr == null || arr.length == 0) {
            return "";
        }
        return Arrays.toString(arr);
    }
}
