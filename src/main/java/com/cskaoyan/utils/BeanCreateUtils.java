package com.cskaoyan.utils;

import java.lang.reflect.Field;

/**
 * @Author: cybertheye
 * @description:
 * @keypoint:
 * @tags:
 * @related:
 */

public class BeanCreateUtils {

    /**
     * 创建一个Bean实例
     * ！！！如果有继承，拿不到爸爸的成员变量！！！自己想办法
     * @param clazz
     * @param args 给的参数要和成员变量一一对应
     * @param <T>
     * @return
     * @throws InstantiationException
     * @throws IllegalAccessException
     */

    public static <T> T createBean(Class<T>clazz,Object...args) throws InstantiationException, IllegalAccessException {
        T t =clazz.newInstance();
        Field[] declaredFields =clazz.getDeclaredFields();

        int index = 0;
        for (Field declaredField : declaredFields) {
            declaredField.setAccessible(true);
            declaredField.set(t,args[index]);
            index++;
        }

        return t;
    }
}
