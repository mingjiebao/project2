package com.cskaoyan.utils;

import javax.servlet.http.HttpServletRequest;

/**
 * @Author:
 * @description: 关于request的工具类
 * @keypoint:
 * @tags:
 * @related:
 */

public class RequestUtils {

    /**
     * 得到ip地址
     * @param req
     * @return
     */
    public static String getIPAddress(HttpServletRequest req){
        //这个有一个问题，获得的不是真实的地址，是gateway的地址
        //todo ip地址完善一下
        return req.getRemoteHost();
    }
}
