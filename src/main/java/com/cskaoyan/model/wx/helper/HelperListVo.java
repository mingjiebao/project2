package com.cskaoyan.model.wx.helper;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class HelperListVo {
    private Integer id;
    private String question;
    private String answer;
    private String addTime;
    private String updateTime;
    private Boolean deleted;
}
