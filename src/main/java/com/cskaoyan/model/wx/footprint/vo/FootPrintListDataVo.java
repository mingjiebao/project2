package com.cskaoyan.model.wx.footprint.vo;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class FootPrintListDataVo {
    private String brief;
    private String picUrl;
    private String addTime;
    private Integer goodsId;
    private String name;
    private Integer id;
    private Integer retailPrice;
}
