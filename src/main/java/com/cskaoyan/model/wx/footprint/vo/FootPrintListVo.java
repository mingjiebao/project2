package com.cskaoyan.model.wx.footprint.vo;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@Data
public class FootPrintListVo {

    private Integer total;
    private Integer pages;
    private Integer limit;
    private Integer page;
    private List<FootPrintListDataVo> list;

}
