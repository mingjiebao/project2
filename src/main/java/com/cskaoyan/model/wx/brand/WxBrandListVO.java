package com.cskaoyan.model.wx.brand;

import com.cskaoyan.model.admin.goodsbean.Brand;
import lombok.Data;

import java.util.List;

/**
 * @Author AhaNg
 * @Date 2022/1/11 10:59
 * @description:
 * @return:
 */
@Data
public class WxBrandListVO {

    long total;
    Integer pages;
    Integer limit;
    Integer page;
    List<Brand> list;
}
