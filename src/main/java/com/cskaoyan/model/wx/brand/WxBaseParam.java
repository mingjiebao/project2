package com.cskaoyan.model.wx.brand;

import lombok.Data;

/**
 * @Author AhaNg
 * @Date 2022/1/11 10:52
 * @description:
 * @return:
 */
@Data
public class WxBaseParam {
    Integer page;
    Integer limit;
}
