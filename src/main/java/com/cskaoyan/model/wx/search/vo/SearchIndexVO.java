package com.cskaoyan.model.wx.search.vo;

import com.cskaoyan.model.wx.search.Keyword;
import com.cskaoyan.model.wx.search.SearchHistory;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @Author AhaNg
 * @Date 2022/1/11 22:37
 * @description:
 * @return:
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SearchIndexVO {
    Keyword defaultKeyword;
    List<Keyword> hotKeywordList;
    List<SearchHistory> historyKeywordList;
}
