package com.cskaoyan.model.wx.wxuserdetailbean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @description:
 * @author: XM
 * @date: 2022/1/11 17:08
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class WXUserCommentsVo {
        /**
         * data : [{"addTime":"2022-01-11 15:20:22","picList":[],"adminContent":"11","nickname":"userfasdfasdsdf","id":1082,"avatar":"","content":"很好"},{"addTime":"2018-02-01 00:00:00","picList":["https://yanxuan.nosdn.127.net/556cb7799b8218db00c3a65241e0f92b.jpg"],"adminContent":"多谢","nickname":"userfasdfasdsdf","id":903,"avatar":"","content":"这个床垫现在垫上去之后舒服多了，软软的，弹性不错，你们睡硬床板的考虑一下呗？"}]
         * count : 92
         */

        private Integer count;
        private List<DataBean> data;

        @NoArgsConstructor
        @Data
        public static class DataBean {
            /**market_user、market_comment
             * addTime : 2022-01-11 15:20:22
             * picList : []
             * adminContent : 11
             * nickname : userfasdfasdsdf
             * id : 1082
             * avatar :
             * content : 很好
             */
            private String addTime;
            private String adminContent;
            private String nickname;
            private Integer id;
            private String avatar;
            private String content;
            private String[] picList;
        }

}