package com.cskaoyan.model.wx.wxuserdetailbean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description:
 * @author: XM
 * @date: 2022/1/11 17:32
 */
@AllArgsConstructor
@NoArgsConstructor

public class WXUserDetailBrandVo {
        /**(品牌商表)
         * id : 1001020
         * name : Ralph Lauren制造商
         * desc : 我们与Ralph Lauren Home的制造商成功接洽，掌握先进的生产设备，传承品牌工艺和工序。追求生活品质的你，值得拥有。
         * picUrl : http://yanxuan.nosdn.127.net/9df78eb751eae2546bd3ee7e61c9b854.png
         * sortOrder : 20
         * floorPrice : 29.0
         * addTime : 2018-02-01 00:00:00
         * updateTime : 2018-02-01 00:00:00
         * deleted : false
         */

        private Integer id;
        private String name;
        private String desc;
        private String picUrl;
        private Integer sortOrder;
        private Double floorPrice;
        private String addTime;
        private String updateTime;
        private Boolean deleted;

        public Integer getId() {
                return id;
        }

        public void setId(Integer id) {
                this.id = id;
        }

        public String getName() {
                return name;
        }

        public void setName(String name) {
                this.name = name;
        }

        public String getDesc() {
                return desc;
        }

        public void setDesc(String desc) {
                this.desc = desc;
        }

        public String getPicUrl() {
                return picUrl;
        }

        public void setPicUrl(String picUrl) {
                this.picUrl = picUrl;
        }

        public Integer getSortOrder() {
                return sortOrder;
        }

        public void setSortOrder(Integer sortOrder) {
                this.sortOrder = sortOrder;
        }

        public Double getFloorPrice() {
                return floorPrice;
        }

        public void setFloorPrice(Double floorPrice) {
                this.floorPrice = floorPrice;
        }

        public String getAddTime() {
                return addTime;
        }

        public void setAddTime(String addTime) {
                this.addTime = addTime;
        }

        public String getUpdateTime() {
                return updateTime;
        }

        public void setUpdateTime(String updateTime) {
                this.updateTime = updateTime;
        }

        public Boolean getDeleted() {
                return deleted;
        }

        public void setDeleted(Integer deleted) {
                if(deleted==0){
                        this.deleted = false;
                }
                else if(deleted==1){
                        this.deleted = true;
                }
        }
}