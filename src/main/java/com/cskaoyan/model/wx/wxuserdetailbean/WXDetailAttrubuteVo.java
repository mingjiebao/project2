package com.cskaoyan.model.wx.wxuserdetailbean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description:
 * @author: XM
 * @date: 2022/1/11 19:49
 */
@AllArgsConstructor
@NoArgsConstructor

public class WXDetailAttrubuteVo {
        /**market_goods_attribute
         * id : 870
         * goodsId : 1181000
         * attribute : 规格
         * value : 组合一：AB面独立弹簧床垫 进口乳胶150*200cm*1+可水洗抗菌防螨丝羽绒枕*2。
         组合二：AB面独立弹簧床垫 进口乳胶180*200cm*1+可水洗抗菌防螨丝羽绒枕*2
         * addTime : 2018-02-01 00:00:00
         * updateTime : 2018-02-01 00:00:00
         * deleted : false
         */

        private Integer id;
        private Integer goodsId;
        private String attribute;
        private String value;
        private String addTime;
        private String updateTime;
        private Boolean deleted;

        public Integer getId() {
                return id;
        }

        public void setId(Integer id) {
                this.id = id;
        }

        public Integer getGoodsId() {
                return goodsId;
        }

        public void setGoodsId(Integer goodsId) {
                this.goodsId = goodsId;
        }

        public String getAttribute() {
                return attribute;
        }

        public void setAttribute(String attribute) {
                this.attribute = attribute;
        }

        public String getValue() {
                return value;
        }

        public void setValue(String value) {
                this.value = value;
        }

        public String getAddTime() {
                return addTime;
        }

        public void setAddTime(String addTime) {
                this.addTime = addTime;
        }

        public String getUpdateTime() {
                return updateTime;
        }

        public void setUpdateTime(String updateTime) {
                this.updateTime = updateTime;
        }

        public Boolean getDeleted() {
                return deleted;
        }

        public void setDeleted(Integer deleted) {
                if(deleted==0){
                        this.deleted = false;
                }
                else if(deleted==1){
                        this.deleted = true;
                }
        }
}