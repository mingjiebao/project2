package com.cskaoyan.model.wx.wxuserdetailbean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description:
 * @author: XM
 * @date: 2022/1/11 19:58
 */
@AllArgsConstructor
@NoArgsConstructor

public class WXDetailProductVo {
        /**
         * id : 1
         * goodsId : 1181000
         * specifications : ["1.5m床垫*1+枕头*2","浅杏粉"]
         * price : 999.0
         * number : 100
         * url : http://yanxuan.nosdn.127.net/1f67b1970ee20fd572b7202da0ff705d.png
         * addTime : 2018-02-01 00:00:00
         * updateTime : 2022-01-11 16:47:14
         * deleted : false
         */

        private Integer id;
        private Integer goodsId;
        private Double price;
        private Integer number;
        private String url;
        private String addTime;
        private String updateTime;
        private Boolean deleted;
        private String[] specifications;

        public Integer getId() {
                return id;
        }

        public void setId(Integer id) {
                this.id = id;
        }

        public Integer getGoodsId() {
                return goodsId;
        }

        public void setGoodsId(Integer goodsId) {
                this.goodsId = goodsId;
        }

        public Double getPrice() {
                return price;
        }

        public void setPrice(Double price) {
                this.price = price;
        }

        public Integer getNumber() {
                return number;
        }

        public void setNumber(Integer number) {
                this.number = number;
        }

        public String getUrl() {
                return url;
        }

        public void setUrl(String url) {
                this.url = url;
        }

        public String getAddTime() {
                return addTime;
        }

        public void setAddTime(String addTime) {
                this.addTime = addTime;
        }

        public String getUpdateTime() {
                return updateTime;
        }

        public void setUpdateTime(String updateTime) {
                this.updateTime = updateTime;
        }

        public Boolean getDeleted() {
                return deleted;
        }

        public void setDeleted(Integer deleted) {
                if(deleted==0){
                        this.deleted = false;
                }
                else if(deleted==1){
                        this.deleted = true;
                }
        }

        public String[] getSpecifications() {
                return specifications;
        }

        public void setSpecifications(String[] specifications) {
                this.specifications = specifications;
        }
}