package com.cskaoyan.model.wx.wxuserdetailbean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description:
 * @author: XM
 * @date: 2022/1/11 19:40
 */
@AllArgsConstructor
@NoArgsConstructor

public class WXDetailIssueVo {


        /**market_issue
         * id : 1
         * question : 购买运费如何收取？
         * answer : 单笔订单金额（不含运费）满88元免邮费；不满88元，每单收取10元运费。
         (港澳台地区需满
         * addTime : 2018-02-01 00:00:00
         * updateTime : 2022-01-06 21:37:58
         * deleted : false
         */

        private Integer id;
        private String question;
        private String answer;
        private String addTime;
        private String updateTime;
        private Boolean deleted;

        public Integer getId() {
                return id;
        }

        public void setId(Integer id) {
                this.id = id;
        }

        public String getQuestion() {
                return question;
        }

        public void setQuestion(String question) {
                this.question = question;
        }

        public String getAnswer() {
                return answer;
        }

        public void setAnswer(String answer) {
                this.answer = answer;
        }

        public String getAddTime() {
                return addTime;
        }

        public void setAddTime(String addTime) {
                this.addTime = addTime;
        }

        public String getUpdateTime() {
                return updateTime;
        }

        public void setUpdateTime(String updateTime) {
                this.updateTime = updateTime;
        }

        public boolean isDeleted() {
                return deleted;
        }

        public void setDeleted(Integer deleted) {
                if(deleted==0){
                        this.deleted = false;
                }
                else if(deleted==1){
                        this.deleted = true;
                }
        }
}