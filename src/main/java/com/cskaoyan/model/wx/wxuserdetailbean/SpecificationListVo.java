package com.cskaoyan.model.wx.wxuserdetailbean;

import com.cskaoyan.model.BaseRespVo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @description:
 * @author: XM
 * @date: 2022/1/11 17:41
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class SpecificationListVo {

        /**
         * name : 规格
         * valueList : [{"id":1,"goodsId":1181000,"specification":"规格","value":"1.5m床垫*1+枕头*2","picUrl":"","addTime":"2018-02-01 00:00:00","updateTime":"2018-02-01 00:00:00","deleted":false},{"id":2,"goodsId":1181000,"specification":"规格","value":"1.8m床垫*1+枕头*2","picUrl":"","addTime":"2018-02-01 00:00:00","updateTime":"2018-02-01 00:00:00","deleted":false}]
         */

        private String name;
        private List<ValueListBean> valueList;

        @NoArgsConstructor

        public static class ValueListBean {
            /**
             * id : 1
             * goodsId : 1181000
             * specification : 规格
             * value : 1.5m床垫*1+枕头*2
             * picUrl :
             * addTime : 2018-02-01 00:00:00
             * updateTime : 2018-02-01 00:00:00
             * deleted : false
             */

            private Integer id;
            private Integer goodsId;
            private String specification;
            private String value;
            private String picUrl;
            private String addTime;
            private String updateTime;
            private Boolean deleted;

            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

            public Integer getGoodsId() {
                return goodsId;
            }

            public void setGoodsId(Integer goodsId) {
                this.goodsId = goodsId;
            }

            public String getSpecification() {
                return specification;
            }

            public void setSpecification(String specification) {
                this.specification = specification;
            }

            public String getValue() {
                return value;
            }

            public void setValue(String value) {
                this.value = value;
            }

            public String getPicUrl() {
                return picUrl;
            }

            public void setPicUrl(String picUrl) {
                this.picUrl = picUrl;
            }

            public String getAddTime() {
                return addTime;
            }

            public void setAddTime(String addTime) {
                this.addTime = addTime;
            }

            public String getUpdateTime() {
                return updateTime;
            }

            public void setUpdateTime(String updateTime) {
                this.updateTime = updateTime;
            }

            public Boolean getDeleted() {
                return deleted;
            }

            public void setDeleted(Integer deleted) {
                if(deleted==0){
                    this.deleted = false;
                }
                else if(deleted==1){
                    this.deleted = true;
                }

            }
        }

}