package com.cskaoyan.model.wx.comment.vo;

import lombok.Data;

import java.util.List;

@Data
public class CommentListContentVO {
    private Integer userId;
    private String addTime;
    private String adminContent;
    private String content;
    private List<String> picList;
    private CommentListContentUserVO userInfo;
}
