package com.cskaoyan.model.wx.comment.bo;

import lombok.Data;

import java.util.List;

@Data
public class CommentPostBO {
    private String content;
    private Boolean hasPicture;
    private String valueId;
    private Short star;
    private Integer type;
    private String[] picUrls;
}
