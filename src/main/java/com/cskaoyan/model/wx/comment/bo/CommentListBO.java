package com.cskaoyan.model.wx.comment.bo;

import lombok.Data;

@Data
public class CommentListBO {
    private Integer valueId;
    private Boolean type;
    private Integer showType;
    private Integer page;
    private Integer limit;
}
