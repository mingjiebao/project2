package com.cskaoyan.model.wx.comment.vo;

import lombok.Data;

import java.util.List;
@Data
public class CommentPostVO {
    private String addTime;
    private String content;
    private Boolean hasPicture;
    private Integer id;
    private String[] picUrls;
    private Short star;
    private Integer type;
    private String updateTime;
    private Integer userId;
    private Integer valueId;
}
