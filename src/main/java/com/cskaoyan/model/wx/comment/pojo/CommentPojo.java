package com.cskaoyan.model.wx.comment.pojo;

import lombok.Data;

import java.util.Date;

@Data
public class CommentPojo {
    private Integer id;
    private Integer valueId;
    private Boolean type;
    private String content;
    private String adminContent;
    private Integer userId;
    private Boolean hasPicture;
    private String picUrls;
    private Short star;
    private Date addTime;
    private Date updateTime;
    private Boolean deleted;
}
