package com.cskaoyan.model.wx.comment.vo;

import lombok.Data;

import java.util.List;

@Data
public class CommentListVO {
    private Integer limit;
    private List<CommentListContentVO> list;
    private Integer page;
    private Integer pages;
    private Integer total;
}
