package com.cskaoyan.model.wx.comment.vo;

import lombok.Data;

@Data
public class CommentListContentUserVO {
    private String nickName;
    private String avatarUrl;
}
