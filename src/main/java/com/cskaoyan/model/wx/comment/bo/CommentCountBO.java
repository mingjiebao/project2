package com.cskaoyan.model.wx.comment.bo;

import lombok.Data;

@Data
public class CommentCountBO {
    private Integer valueId;
    private Integer type;
}
