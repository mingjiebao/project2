package com.cskaoyan.model.wx.comment.vo;

import lombok.Data;

@Data
public class CommentCountVO {
    private Integer allCount;
    private Integer hasPicture;
}
