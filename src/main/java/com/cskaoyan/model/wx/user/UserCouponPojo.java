package com.cskaoyan.model.wx.user;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class UserCouponPojo {
    private Integer id;
    private Integer userId;
    private Integer couponId;
    private Integer status;
    private String usedTime;
    private String startTime;
    private String endTime;
    private Integer orderId;
    private String addTime;
    private String updateTime;
    private Boolean deleted;
    private String name;
}
