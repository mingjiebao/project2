package com.cskaoyan.model.wx.wxadressbean.bo;

import com.cskaoyan.annotation.TelValidate;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * @author : [Dragon]
 * @createTime : [2022/1/11 16:36]
 * @description :
 */
@NoArgsConstructor

public class WXAddressSaveBo {

    private Integer id;
    @NotNull
    @NotEmpty
    private String name;
    @TelValidate(message = "手机号必须为11位正整数")
    private String tel;
    @NotNull
    @NotEmpty
    private String province;
    @NotNull
    @NotEmpty
    private String city;
    @NotNull
    @NotEmpty
    private String county;
    private String areaCode;
    @NotNull
    @NotEmpty
    private String addressDetail;
    private Integer isDefault;
    private String updateTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getAddressDetail() {
        return addressDetail;
    }

    public void setAddressDetail(String addressDetail) {
        this.addressDetail = addressDetail;
    }

    public Integer getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(Boolean isDefault) {
        if(isDefault==true){
            this.isDefault = 1;
        }
        else {
            this.isDefault = 0;
        }
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "WXAddressSaveBo{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", tel='" + tel + '\'' +
                ", province='" + province + '\'' +
                ", city='" + city + '\'' +
                ", county='" + county + '\'' +
                ", areaCode='" + areaCode + '\'' +
                ", addressDetail='" + addressDetail + '\'' +
                ", isDefault=" + isDefault +
                ", updateTime='" + updateTime + '\'' +
                '}';
    }
}
