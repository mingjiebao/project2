package com.cskaoyan.model.wx.wxadressbean.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author : [Dragon]
 * @createTime : [2022/1/11 15:25]
 * @description :
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class WXAdressList {
    private Integer total;
    private Integer pages;
    private Integer limit;
    private Integer page;
    private List<ListBean> list;

    @NoArgsConstructor

    public static class ListBean {

        private Integer id;
        private String name;
        private Integer userId;
        private String province;
        private String city;
        private String county;
        private String addressDetail;
        private String areaCode;
        private String tel;
        private Boolean isDefault;
        private String addTime;
        private String updateTime;
        private Boolean deleted;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getUserId() {
            return userId;
        }

        public void setUserId(Integer userId) {
            this.userId = userId;
        }

        public String getProvince() {
            return province;
        }

        public void setProvince(String province) {
            this.province = province;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getCounty() {
            return county;
        }

        public void setCounty(String county) {
            this.county = county;
        }

        public String getAddressDetail() {
            return addressDetail;
        }

        public void setAddressDetail(String addressDetail) {
            this.addressDetail = addressDetail;
        }

        public String getAreaCode() {
            return areaCode;
        }

        public void setAreaCode(String areaCode) {
            this.areaCode = areaCode;
        }

        public String getTel() {
            return tel;
        }

        public void setTel(String tel) {
            this.tel = tel;
        }

        public Boolean getisDefault() {
            return isDefault;
        }

        public void setDefault(Integer isdefault) {
            if(isdefault==0){
                this.isDefault = false;
            }else if(isdefault==1){
                this.isDefault = true;
            }
        }

        public String getAddTime() {
            return addTime;
        }

        public void setAddTime(String addTime) {
            this.addTime = addTime;
        }

        public String getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(String updateTime) {
            this.updateTime = updateTime;
        }

        public boolean isDeleted() {
            return deleted;
        }

        public void setDeleted(Integer deleted) {
           if(deleted==0){
               this.deleted=false;
           }else if(deleted==1)
            this.deleted = true;
        }
    }

}
