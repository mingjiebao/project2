package com.cskaoyan.model.wx.catalog;

import com.cskaoyan.model.admin.goodsbean.Category;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * @Author AhaNg
 * @Date 2022/1/11 14:11
 * @description:
 * @return:
 */
@Data
public class CatalogIndexVO {
    private Category currentCategory;
    private List<Category> categoryList;
    private List<Category> currentSubCategory;


    @Data
    public static class CurrentCategory{
        private long id;
        private String name;
        private String keywords;
        private String desc;
        private int pid;
        private String iconUrl;
        private String picUrl;
        private String level;
        private int sortOrder;
        private Date addTime;
        private Date updateTime;
        private boolean deleted;

    }
}
