package com.cskaoyan.model.wx.catalog;

import com.cskaoyan.model.admin.goodsbean.Category;
import lombok.Data;

import java.util.List;

/**
 * @Author AhaNg
 * @Date 2022/1/11 12:34
 * @description:
 * @return:
 */
@Data
public class CurrentVO {
    private Category currentCategory;
    private List<Category> currentSubCategory;

}
