package com.cskaoyan.model.wx.special.vo;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class SpecialListContentVO {
    private Integer id;
    private String picUrl;
    private BigDecimal price;
    private String readCount;
    private String subtitle;
    private String title;
}
