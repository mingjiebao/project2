package com.cskaoyan.model.wx.special.vo;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class SpecialDetailGoodsVO {
    private String brief;
    private BigDecimal counterPrice;
    private Integer id;
    private Boolean isNew;
    private Boolean isHot;
    private String name;
    private String picUrl;
    private BigDecimal retailPrice;
}
