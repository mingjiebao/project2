package com.cskaoyan.model.wx.special.bo;

import lombok.Data;

@Data
public class SpecialBO {
    private Integer page;
    private Integer limit;
}
