package com.cskaoyan.model.wx.special.vo;

import lombok.Data;

import java.util.List;

@Data
public class SpecialRelatedVO {
    private Integer limit;
    private List<SpecialRelatedContentVO> list;
    private Integer page;
    private Integer pages;
    private Integer total;
}
