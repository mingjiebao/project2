package com.cskaoyan.model.wx.special.vo;

import lombok.Data;

import java.util.List;

@Data
public class SpecialDetailVO {
    List<SpecialDetailGoodsVO> goods;
    SpecialDetailTopicVO topic;
}
