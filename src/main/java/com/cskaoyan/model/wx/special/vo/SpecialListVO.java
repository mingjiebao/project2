package com.cskaoyan.model.wx.special.vo;

import lombok.Data;

import java.util.List;

@Data
public class SpecialListVO {
    private Integer limit;
    private List<SpecialListContentVO> list;
    private Integer page;
    private Integer pages;
    private Integer total;
}
