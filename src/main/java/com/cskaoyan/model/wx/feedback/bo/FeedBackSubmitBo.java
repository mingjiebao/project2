package com.cskaoyan.model.wx.feedback.bo;

import com.cskaoyan.annotation.IntValidate;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import java.util.List;

@NoArgsConstructor
@Data
public class FeedBackSubmitBo {

    @Length(min = 11,max = 11,message = "手机号格式不正确")
    private String mobile;
    private String feedType;
    private String content;
    private Boolean hasPicture;
    private String[] picUrls;
}
