package com.cskaoyan.model.wx.order.vo;

import lombok.Data;

/**
 * @package: com.cskaoyan.model.wx.order.vo
 * @Description: 小程序订单详情中参数
 *                  注:暂时不知道是什么参数
 * @author: 北青
 * @date: 2022/1/11 15:20
 */
@Data
public class ExpressInfoVo {
}
