package com.cskaoyan.model.wx.order.vo;

import lombok.Data;

import java.util.List;

/**
 * @package: com.cskaoyan.model.wx.order.vo
 * @Description: 微信小城序订单模块订单元素内容封装
 * @author: 北青
 * @date: 2022/1/11 9:22
 */
@Data
public class WxOrderVo {
    private Integer orderStatus; //订单状态
    private String orderStatusText; //订单状态描述
    private Integer aftersaleStatus; //售后状态
    private Boolean isGroupin; //是否是团购
    private String orderSn; //订单编号
    private Integer actualPrice; //实付款
    private Integer id; //订单id
    private List<WxOrderGoodsVo> goodsList; //商品列表
    private HandleOptionVo handleOption; //操作选项
}
