package com.cskaoyan.model.wx.order.vo;

import lombok.Data;

import java.util.List;

/**
 * @package: com.cskaoyan.model.wx.order.vo
 * @Description: 小程序订单详情中商品信息
 * @author: 北青
 * @date: 2022/1/11 15:27
 */
@Data
public class WxOrderDetailGoodsVo {
    private Integer productId; //商品获评表的货品ID
    private String addTime; //创建时间
    private Integer orderId; //订单表订单ID
    private Integer goodsId; //商品表商品ID
    private String goodsSn; //商品编号
    private String updateTime; //更新时间
    private List<String> specifications; //商品规格列表
    private Integer number; //购买数量
    private String picUrl; //商品图片
    private Boolean deleted; //逻辑删除
    private Double price; //商品售价
    private Integer comment; //是否可以评价,如果是-1，则超期不能评价；如果是0，则可以评价；如果其他值，则是comment表里面的评论ID。
    private Integer id;
    private String goodsName; //商品名称
}
