package com.cskaoyan.model.wx.order.pojo;

import lombok.Data;

/**
 * @package: com.cskaoyan.model.wx.order.pojo
 * @Description: 数据库——微信小程序——订单——订单可操作选项和订单状态描述
 * @author: 北青
 * @date: 2022/1/11 11:02
 */
@Data
public class WxOrderHandleOptionAndText {
    private Integer orderStatus;
    private String orderStatusText;
    private Boolean cancel; //取消
    private Boolean confirm; //确认收货
    private Boolean rebuy; //重新购买
    private Boolean pay; //付款
    private Boolean comment; //评价
    private Boolean delete; //删除
    private Boolean aftersale; //售后
    private Boolean refund; //退款
}
