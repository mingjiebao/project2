package com.cskaoyan.model.wx.order.bo;

import lombok.Data;

/**
 * @package: com.cskaoyan.model.wx.order.bo
 * @Description: 购物车下单提交信息
 * @author: 北青
 * @date: 2022/1/12 19:26
 */
@Data
public class WxOrderSubmitBo {

    /**
     * grouponRulesId : 团购规则ID
     * cartId : 购物车ID
     * grouponLinkId : 0
     * userCouponId : 用户的优惠券
     * couponId : 优惠券ID
     * message : 信息
     * addressId : 地址ID
     */
    private Integer grouponRulesId;
    private Integer cartId;
    private Integer grouponLinkId;
    private Integer userCouponId;
    private Integer couponId;
    private String message;
    private Integer addressId;
}
