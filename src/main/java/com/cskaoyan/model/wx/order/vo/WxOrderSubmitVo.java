package com.cskaoyan.model.wx.order.vo;

import lombok.Data;

/**
 * @package: com.cskaoyan.model.wx.order.vo
 * @Description:
 * @author: 北青
 * @date: 2022/1/12 19:30
 */
@Data
public class WxOrderSubmitVo {
    private Integer orderId;
    private Integer grouponLinkId;
}
