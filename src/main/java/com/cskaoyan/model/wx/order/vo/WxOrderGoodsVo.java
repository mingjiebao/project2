package com.cskaoyan.model.wx.order.vo;

import lombok.Data;

import java.util.List;

/**
 * @package: com.cskaoyan.model.wx.order.vo
 * @Description: 微信小程序订单模块订单商品元素封装
 * @author: 北青
 * @date: 2022/1/11 9:42
 */
@Data
public class WxOrderGoodsVo {

    /**
     * number : 1
     * picUrl : http://182.92.235.201:8083/wx/storage/fetch/0nw67u4zcldeklycg14z.png
     * price : 99999
     * id : 111
     * goodsName : 商品名称
     * specifications : ["啊啊啊啊","嗯嗯"]
     */
    private Integer number; //购买数量
    private String picUrl; //商品货品图片或商品图片
    private Double price; //商品售价
    private Integer id; //订单商品id
    private String goodsName; //商品名称
    private List<String> specifications; //商品规格列表
}
