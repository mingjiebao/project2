package com.cskaoyan.model.wx.order.vo;

import lombok.Data;

/**
 * @package: com.cskaoyan.model.wx.order.vo
 * @Description: 小程序订单详情中订单信息
 * @author: 北青
 * @date: 2022/1/11 15:15
 */
@Data
public class WxOrderDetailVo {

    /**
     * consignee : 杰宝
     * address : 湖北省黄冈市浠水县 龙哥同坐
     * addTime : 2022-01-11 15:01:58
     * orderSn : 20220111954829
     * actualPrice : 236.9
     * mobile : 13888888888
     * message : 一定一定
     * orderStatusText : 未付款
     * aftersaleStatus : 0
     * goodsPrice : 4.9
     * couponPrice : 0
     * id : 126
     * freightPrice : 232
     */
    private String consignee; //收货人名
    private String address; //收货地址
    private String addTime; //添加时间
    private String orderSn; //订单编号
    private Double actualPrice; //实付款
    private String mobile; //联系方式
    private String message; //订单说明
    private String orderStatusText; //订单状态说明
    private Integer aftersaleStatus; //售后
    private Double goodsPrice; //商品总费用
    private Double couponPrice; //优惠券减免
    private Integer id; //订单id
    private Double freightPrice; //配送费用
    private Integer orderStatus; //订单状态
    private HandleOptionVo handleOption;
}
