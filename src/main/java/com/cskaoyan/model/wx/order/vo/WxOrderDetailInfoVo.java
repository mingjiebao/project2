package com.cskaoyan.model.wx.order.vo;

import lombok.Data;

import java.util.List;

/**
 * @package: com.cskaoyan.model.wx.order.vo
 * @Description: 小程序订单详情响应参数
 * @author: 北青
 * @date: 2022/1/11 15:10
 */
@Data
public class WxOrderDetailInfoVo {
    List<ExpressInfoVo> expressInfo;
    WxOrderDetailVo orderInfo;
    List<WxOrderDetailGoodsVo> orderGoods;
}
