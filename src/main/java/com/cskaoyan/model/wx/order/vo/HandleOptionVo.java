package com.cskaoyan.model.wx.order.vo;

import lombok.Data;

/**
 * @package: com.cskaoyan.model.wx.order.vo
 * @Description: 订单可操作选项
 * @author: 北青
 * @date: 2022/1/11 10:23
 */
@Data
public class HandleOptionVo {

    /**
     * cancel : false
     * confirm : false
     * rebuy : false
     * pay : false
     * comment : false
     * delete : true
     * aftersale : false
     * refund : false
     */
    private Boolean cancel; //取消
    private Boolean confirm; //确认收货
    private Boolean rebuy; //重新购买
    private Boolean pay; //付款
    private Boolean comment; //评价
    private Boolean delete; //删除
    private Boolean aftersale; //售后
    private Boolean refund; //退款
}
