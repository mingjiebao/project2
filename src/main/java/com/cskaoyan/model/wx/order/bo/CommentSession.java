package com.cskaoyan.model.wx.order.bo;

import lombok.Data;

/**
 * @package: com.cskaoyan.model.wx.order.bo
 * @Description: 进行商品评价时用于传递评价对应的orderId和goodsId内容的封装,用于存入session
 * @author: 北青
 * @date: 2022/1/12 9:55
 */
@Data
public class CommentSession {
    Integer orderId;
    Integer goodsId;

    public static CommentSession getComment(Integer orderId, Integer goodsId){
        CommentSession commentSession = new CommentSession();
        commentSession.setOrderId(orderId);
        commentSession.setGoodsId(goodsId);
        return commentSession;
    }
}
