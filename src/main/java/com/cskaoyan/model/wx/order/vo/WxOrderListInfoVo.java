package com.cskaoyan.model.wx.order.vo;

import com.cskaoyan.model.BaseRespParam;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @package: com.cskaoyan.model.wx.order.vo
 * @Description: 微信小程序我的订单显示返回参数封装
 * @author: 北青
 * @date: 2022/1/11 9:20
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class WxOrderListInfoVo extends BaseRespParam {
    List<WxOrderVo> list;
}
