package com.cskaoyan.model.wx.wxgoodsbean.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author : [Dragon]
 * @createTime : [2022/1/11 4:03]
 * @description :
 */

@NoArgsConstructor
@AllArgsConstructor
@Data
public class WXGoodsCategoryVo {

    private CurrentCategoryBean currentCategory;
    private ParentCategoryBean parentCategory;
    private List<BrotherCategoryBean> brotherCategory;

    @NoArgsConstructor
    @Data
    public static class CurrentCategoryBean {


        private Integer id;
        private String name;
        private String keywords;
        private String desc;
        private Integer pid;
        private String iconUrl;
        private String picUrl;
        private String level;
        private Integer sortOrder;
        private String addTime;
        private String updateTime;
        private boolean deleted;
    }

    @NoArgsConstructor
    @Data
    public static class ParentCategoryBean {
        private Integer id;
        private String name;
        private String keywords;
        private String desc;
        private Integer pid;
        private String iconUrl;
        private String picUrl;
        private String level;
        private Integer sortOrder;
        private String addTime;
        private String updateTime;
        private boolean deleted;
    }

    @NoArgsConstructor
    @Data
    public static class BrotherCategoryBean {

        private Integer id;
        private String name;
        private String keywords;
        private String desc;
        private Integer pid;
        private String iconUrl;
        private String picUrl;
        private String level;
        private Integer sortOrder;
        private String addTime;
        private String updateTime;
        private boolean deleted;
    }
}
