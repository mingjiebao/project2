package com.cskaoyan.model.wx.wxgoodsbean.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author : [Dragon]
 * @createTime : [2022/1/11 6:09]
 * @description :
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class WXGoodsRelatedVo {

    private Integer total;
    private Integer pages;
    private Integer limit;
    private Integer page;
    private List<ListBean> list;

    @NoArgsConstructor
    @Data
    public static class ListBean {

        private Integer id;
        private String name;
        private String brief;
        private String picUrl;
        private boolean isNew;
        private boolean isHot;
        private Integer counterPrice;
        private Integer retailPrice;
    }
}
