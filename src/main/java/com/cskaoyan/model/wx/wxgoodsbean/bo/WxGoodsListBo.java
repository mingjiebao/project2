package com.cskaoyan.model.wx.wxgoodsbean.bo;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class WxGoodsListBo {
    // 1. brandID和keyword场景冲突
    private Integer brandId;
    // 关键词查询的场景中
    private String keyword;
    private Boolean isHot;
    private Boolean isNew;
    private Integer page;
    private Integer limit;
    private String sort;
    private String order;

    // 用不到的团购
    private Integer categoryId;
}
