package com.cskaoyan.model.wx.wxgoodsbean.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author : [Dragon]
 * @createTime : [2022/1/11 4:47]
 * @description :
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class WXGoodsListVo {

    private Integer total;
    private Integer pages;
    private Integer limit;
    private Integer page;
    private List<ListBean> list;
    private List<FilterCategoryListBean> filterCategoryList;

    @NoArgsConstructor
    @Data
    public static class ListBean {


        private Integer id;
        private String name;
        private String brief;
        private String picUrl;
        private boolean isNew;
        private boolean isHot;
        private Integer counterPrice;
        private Integer retailPrice;
    }

    @NoArgsConstructor
    @Data
    public static class FilterCategoryListBean {


        private Integer id;
        private String name;
        private String keywords;
        private String desc;
        private Integer pid;
        private String iconUrl;
        private String picUrl;
        private String level;
        private Integer sortOrder;
        private String addTime;
        private String updateTime;
        private boolean deleted;
    }
}
