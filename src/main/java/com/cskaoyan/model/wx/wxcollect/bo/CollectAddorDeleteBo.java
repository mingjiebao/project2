package com.cskaoyan.model.wx.wxcollect.bo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description:
 * @author: XM
 * @date: 2022/1/10 17:11
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CollectAddorDeleteBo {
    Integer type;
    Integer valueId;
}