package com.cskaoyan.model.wx.wxcollect.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author : [Dragon]
 * @createTime : [2022/1/11 19:40]
 * @description :
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class WXCollectListVo {

    private Integer total;
    private Integer pages;
    private Integer limit;
    private Integer page;
    private List<ListBean> list;

    @NoArgsConstructor
    @Data
    public static class ListBean {
       
        private String brief;
        private String picUrl;
        private Integer valueId;
        private String name;
        private Integer id;
        private Integer type;
        private Integer retailPrice;
    }
}
