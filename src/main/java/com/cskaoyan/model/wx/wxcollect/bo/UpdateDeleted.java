package com.cskaoyan.model.wx.wxcollect.bo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author : [Dragon]
 * @createTime : [2022/1/11 21:56]
 * @description :
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UpdateDeleted {
    private Integer id;
    private Integer deleted;
    private String updateTime;
}
