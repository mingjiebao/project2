package com.cskaoyan.model.wx.wxcoupon.vo;

import com.cskaoyan.model.wx.wxcoupon.WxCouponDto;
import com.sun.org.apache.bcel.internal.generic.DDIV;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class CouponMyListVo {
    private Integer id;
    private Integer cid;
    private String name;
    private String desc;
    private String tag;
    private Double min;
    private Double discount;
    private String startTime;
    private String endTime;
    private Boolean available;

    public CouponMyListVo(WxCouponDto dto) {
        this.id = dto.getId();
        this.cid = dto.getCouponId();
        this.name = dto.getName();
        this.desc = dto.getDesc();
        this.tag = dto.getTag();
        this.min = dto.getMin();
        this.discount = dto.getDiscount();
        this.startTime = dto.getStartTime();
        this.endTime = dto.getEndTime();
        this.available = dto.getAvailable();
    }
}
