package com.cskaoyan.model.wx.wxcoupon.vo;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class CouponBaseVo {
    private Integer limit;
    private Integer page;
    private Integer pages;
    private Integer total;
    private Object list;
}
