package com.cskaoyan.model.wx.wxcoupon;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class WxCouponDto {
    private Integer id;
    private Integer couponId;
    private String name;
    private String desc;
    private String tag;
    private Double min;
    private Double discount;
    private String startTime;
    private String endTime;

    private Integer goodsType;
    private String[] goodsValue;
    private Boolean available;
}
