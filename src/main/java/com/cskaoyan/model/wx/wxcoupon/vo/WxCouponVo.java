package com.cskaoyan.model.wx.wxcoupon.vo;

import com.cskaoyan.model.wx.user.UserCouponPojo;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class WxCouponVo {
    private Integer id;
    private String name;
    private String desc;
    private String tag;
    private Double discount;
    private Double min;
    private Integer days;
    private String startTime;
    private String endTime;
}
