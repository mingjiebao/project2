package com.cskaoyan.model.wx.wxcoupon.bo;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CouponBaseBo {
    private Integer limit;
    private Integer status;
    private Integer page;
}
