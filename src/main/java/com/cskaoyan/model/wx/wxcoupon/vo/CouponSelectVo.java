package com.cskaoyan.model.wx.wxcoupon.vo;

import com.cskaoyan.model.admin.pojo.MarketCouponPojo;
import com.cskaoyan.model.wx.user.UserCouponPojo;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@Data
public class CouponSelectVo {

    private Integer id;
    private Integer cid;
    private String name;
    private String desc;
    private String tag;
    private Double min;
    private Double discount;
    private String startTime;
    private String endTime;
    private Boolean available = true;

}
