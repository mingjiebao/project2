package com.cskaoyan.model.wx.wxhomeindexbean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description:goods表
 * @author: XM
 * @date: 2022/1/11 4:35
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class WXNewGoodsListVo {

        /**goods
         * id : 1181041
         * name : MINJIENA - 笑脸图案运动长裤
         * brief : MINJIENA - 笑脸图案运动长裤
         * picUrl : http://182.92.235.201:8083/wx/storage/fetch/yqimmsqzjk4htn9pzdgs.jpg
         * isNew : true
         * isHot : false
         * counterPrice : 534.99
         * retailPrice : 535.0
         */
        private Integer id;
        private String name;
        private String brief;
        private String picUrl;
        private boolean isNew;
        private boolean isHot;
        private Double counterPrice;
        private Double retailPrice;
}
