package com.cskaoyan.model.wx.wxhomeindexbean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description:
 * @author: XM
 * @date: 2022/1/11 4:37
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class WXChannelVo {

        /**market_category（类目显示）
         * id : 1036040
         * name : 玩儿法规
         * iconUrl :
         */

        private Integer id;
        private String name;
        private String iconUrl;

}