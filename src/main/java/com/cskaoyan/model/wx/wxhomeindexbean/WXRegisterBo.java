package com.cskaoyan.model.wx.wxhomeindexbean;

import com.cskaoyan.annotation.TelValidate;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * @author : [Dragon]
 * @createTime : [2022/1/13 0:18]
 * @description :
 */
@NoArgsConstructor
@Data
public class WXRegisterBo {


    @NotEmpty
    @NotNull
    private String username;
    @NotEmpty
    @NotNull
    private String password;
    @TelValidate(message = "手机号必须为11位整数")
    private String mobile;
    private String code;
    private String wxCode;
}
