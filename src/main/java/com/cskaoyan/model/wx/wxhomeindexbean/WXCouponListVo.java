package com.cskaoyan.model.wx.wxhomeindexbean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description:
 * @author: XM
 * @date: 2022/1/11 4:36
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class WXCouponListVo {

    /**
     * 优惠卷显示
     * id : 70
     * name : adsgjasvdbjh
     * desc :
     * tag :
     * discount : 0.0
     * min : 0.0
     * days : 0
     */

    private Integer id;
    private String name;
    private String desc;
    private String tag;
    private Double discount;
    private Double min;
    private String days;
    private int status = 1;
}
