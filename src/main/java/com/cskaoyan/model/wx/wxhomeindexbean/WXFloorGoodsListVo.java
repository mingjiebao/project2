package com.cskaoyan.model.wx.wxhomeindexbean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @description:
 * @author: XM
 * @date: 2022/1/11 4:47
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class WXFloorGoodsListVo {
        /**
         * name : 玩儿法规
         * goodsList : [{"id":1056002,"name":"男式玩色内裤","brief":"德国工艺，多色随搭","picUrl":"http://yanxuan.nosdn.127.net/922fdbe007033f7a88f7ebc57c3d1e75.png","isNew":false,"isHot":false,"counterPrice":79,"retailPrice":59.5},{"id":1181000,"name":"母亲节礼物-舒适安睡组合","brief":"安心舒适是最好的礼物","picUrl":"http://yanxuan.nosdn.127.net/1f67b1970ee20fd572b7202da0ff705d.png","isNew":true,"isHot":false,"counterPrice":2618,"retailPrice":999},{"id":1020000,"name":"升级款记忆绵护椎腰靠","brief":"人体工学设计，缓解腰背疼痛","picUrl":"http://yanxuan.nosdn.127.net/819fdf1f635a694166bcfdd426416e8c.png","isNew":false,"isHot":false,"counterPrice":99,"retailPrice":79}]
         * id : 1036040
         */

        private String name;
        private Integer id;
        private List<WXNewGoodsListVo> goodsList;

}