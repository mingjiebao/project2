package com.cskaoyan.model.wx.wxhomeindexbean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description:
 * @author: XM
 * @date: 2022/1/11 4:44
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class WXTopicListVo {
        /**market_topic
         * id : 315
         * title : 新疆美食
         * subtitle : 烤包子
         * price : 33333.0
         * readCount : 1000000000
         * picUrl : http://182.92.235.201:8083/wx/storage/fetch/5wok7hodx58lih00h5mq.jpg
         */

        private Integer id;
        private String title;
        private String subtitle;
        private Double price;
        private String readCount;
        private String picUrl;

}