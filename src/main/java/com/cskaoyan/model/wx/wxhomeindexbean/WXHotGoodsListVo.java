package com.cskaoyan.model.wx.wxhomeindexbean;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 * @description:
 * @author: XM
 * @date: 2022/1/11 4:43
 */
@AllArgsConstructor
@NoArgsConstructor
public class WXHotGoodsListVo {


        /**
         * id : 1181026
         * name : 商品名称
         * brief : 这是一个简介
         * picUrl : http://182.92.235.201:8083/wx/storage/fetch/9pwsrswn7y2z0kjo9fy8.png
         * isNew : true
         * isHot : true
         * counterPrice : 99999.0
         * retailPrice : 99999.0
         */

        private Integer id;
        private String name;
        private String brief;
        private String picUrl;
        private boolean isNew;
        private boolean isHot;
        private Double counterPrice;
        private Double retailPrice;

        public Integer getId() {
                return id;
        }

        public void setId(Integer id) {
                this.id = id;
        }

        public String getName() {
                return name;
        }

        public void setName(String name) {
                this.name = name;
        }

        public String getBrief() {
                return brief;
        }

        public void setBrief(String brief) {
                this.brief = brief;
        }

        public String getPicUrl() {
                return picUrl;
        }

        public void setPicUrl(String picUrl) {
                this.picUrl = picUrl;
        }

        public boolean isNew() {
                return isNew;
        }

        public void setNew(Integer aNew) {
             if(aNew==0)
                this.isNew = false;
             else if(aNew==1)
                this.isNew = true;

        }

        public boolean isHot() {
                return isHot;
        }

        public void setHot(Integer hot) {
                if(hot==0)
                    this.isHot = false;
                else if(hot==1)
                    this.isHot = true;
        }

        public Double getCounterPrice() {
                return counterPrice;
        }

        public void setCounterPrice(Double counterPrice) {
                this.counterPrice = counterPrice;
        }

        public Double getRetailPrice() {
                return retailPrice;
        }

        public void setRetailPrice(Double retailPrice) {
                this.retailPrice = retailPrice;
        }
}