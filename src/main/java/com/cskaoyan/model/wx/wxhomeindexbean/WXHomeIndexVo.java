package com.cskaoyan.model.wx.wxhomeindexbean;

import com.cskaoyan.model.wx.wxcoupon.vo.WxCouponVo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @description:
 * @author: XM
 * @date: 2022/1/11 4:16
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class WXHomeIndexVo {

    private List<WXNewGoodsListVo> newGoodsList;
    private List<WxCouponVo> couponList;
    private List<WXChannelVo> channel;
    private List<WXActivityVo> banner;
    private List<WXBrandListVo> brandList;
    private List<WXHotGoodsListVo> hotGoodsList;
    private List<WXTopicListVo> topicList;
    private List<WXFloorGoodsListVo> floorGoodsList;

}