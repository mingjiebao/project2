package com.cskaoyan.model.wx.wxhomeindexbean;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 * @description:
 * @author: XM
 * @date: 2022/1/11 4:52
 */
@AllArgsConstructor
@NoArgsConstructor

public class WXActivityVo {

        /**market_ad
         * id : 2
         * name : 活动 美食节
         * link :
         * url : http://yanxuan.nosdn.127.net/bff2e49136fcef1fd829f5036e07f116.jpg
         * position : 1
         * content : 活动 美食节
         * enabled : true
         * addTime : 2018-02-01 00:00:00
         * updateTime : 2018-02-01 00:00:00
         * deleted : false
         */

        private Integer id;
        private String name;
        private String link;
        private String url;
        private Integer position;
        private String content;
        private boolean enabled;
        private String addTime;
        private String updateTime;
        private boolean deleted;

        public Integer getId() {
                return id;
        }

        public void setId(Integer id) {
                this.id = id;
        }

        public String getName() {
                return name;
        }

        public void setName(String name) {
                this.name = name;
        }

        public String getLink() {
                return link;
        }

        public void setLink(String link) {
                this.link = link;
        }

        public String getUrl() {
                return url;
        }

        public void setUrl(String url) {
                this.url = url;
        }

        public Integer getPosition() {
                return position;
        }

        public void setPosition(Integer position) {
                this.position = position;
        }

        public String getContent() {
                return content;
        }

        public void setContent(String content) {
                this.content = content;
        }

        public boolean isEnabled() {
                return enabled;
        }

        public void setEnabled(boolean enabled) {
                this.enabled = enabled;
        }

        public String getAddTime() {
                return addTime;
        }

        public void setAddTime(String addTime) {
                this.addTime = addTime;
        }

        public String getUpdateTime() {
                return updateTime;
        }

        public void setUpdateTime(String updateTime) {
                this.updateTime = updateTime;
        }

        public boolean isDeleted() {
                return deleted;
        }

        public void setDeleted(Integer deleted) {
                if(deleted==0)
                   this.deleted = false;
                else if(deleted==1)
                   this.deleted = true;
        }
}