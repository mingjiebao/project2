package com.cskaoyan.model.wx.wxhomeindexbean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description:
 * @author: XM
 * @date: 2022/1/11 4:40
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class WXBrandListVo {


        /**market_brand（查找）
         * id : 1001000
         * name : MUJI制造商
         * desc : 严选精选了MUJI制造商和生产原料，
         用几乎零利润的价格，剔除品牌溢价，
         让用户享受原品牌的品质生活。
         * picUrl : http://yanxuan.nosdn.127.net/1541445967645114dd75f6b0edc4762d.png
         * floorPrice : 12.9
         */

        private Integer id;
        private String name;
        private String desc;
        private String picUrl;
        private Double floorPrice;

}