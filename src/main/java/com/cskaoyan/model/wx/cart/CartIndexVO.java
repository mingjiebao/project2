package com.cskaoyan.model.wx.cart;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

/**
 * @Author:
 * @description:
 * @keypoint:
 * @tags:
 * @related:
 */


@NoArgsConstructor
@Data
public class CartIndexVO {

    @JsonProperty("cartTotal")
    private CartTotalDTO cartTotal;
    @JsonProperty("cartList")
    private List<Cart> cartList;

    @NoArgsConstructor
    @Data
    public static class CartTotalDTO {
        @JsonProperty("goodsCount")
        private Integer goodsCount;
        @JsonProperty("checkedGoodsCount")
        private Integer checkedGoodsCount;
        @JsonProperty("goodsAmount")
        private BigDecimal goodsAmount;
        @JsonProperty("checkedGoodsAmount")
        private BigDecimal checkedGoodsAmount;
    }
}
