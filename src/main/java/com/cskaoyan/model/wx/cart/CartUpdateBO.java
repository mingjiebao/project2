package com.cskaoyan.model.wx.cart;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author:
 * @description:
 * @keypoint:
 * @tags:
 * @related:
 */

@NoArgsConstructor
@Data
public class CartUpdateBO {

    @JsonProperty("productId")
    private Integer productId;
    @JsonProperty("goodsId")
    private Integer goodsId;
    @JsonProperty("number")
    private Integer number;
    @JsonProperty("id")
    private Integer id;
}
