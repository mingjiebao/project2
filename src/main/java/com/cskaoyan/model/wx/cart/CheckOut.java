package com.cskaoyan.model.wx.cart;

import lombok.Data;

/**
 * @Author:
 * @description:
 * @keypoint:
 * @tags:
 * @related:
 */
@Data
public class CheckOut {
    Integer cartId;
    Integer addressId;
    Integer couponId;
    Integer userCouponId;
    Integer grouponRulesId;
}
