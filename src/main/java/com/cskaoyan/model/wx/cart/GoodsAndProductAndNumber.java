package com.cskaoyan.model.wx.cart;

import lombok.Data;

/**
 * @Author:
 * @description:
 * @keypoint:
 * @tags:
 * @related:
 */

@Data
public class GoodsAndProductAndNumber {
    Integer goodsId;
    Integer number;
    Integer productId;
}
