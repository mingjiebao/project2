package com.cskaoyan.model.wx.cart;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @Author:
 * @description:
 * @keypoint:
 * @tags:
 * @related:
 */

@NoArgsConstructor
@Data
public class ProductIdsAndChecked {

    @JsonProperty("productIds")
    private List<Integer> productIds;
    @JsonProperty("isChecked")
    private Integer isChecked;
}
