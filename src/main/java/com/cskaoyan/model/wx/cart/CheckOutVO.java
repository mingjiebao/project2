package com.cskaoyan.model.wx.cart;

import com.cskaoyan.model.wx.wxadressbean.vo.WXAddressDetailVo;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

/**
 * @Author:
 * @description:
 * @keypoint:
 * @tags:
 * @related:
 */

@NoArgsConstructor
@Data
public class CheckOutVO {

    @JsonProperty("grouponRulesId")
    private Integer grouponRulesId;
    @JsonProperty("actualPrice")
    private BigDecimal actualPrice;
    @JsonProperty("orderTotalPrice")
    private BigDecimal orderTotalPrice;
    @JsonProperty("cartId")
    private Integer cartId;
    @JsonProperty("userCouponId")
    private Integer userCouponId;
    @JsonProperty("couponId")
    private Integer couponId;
    @JsonProperty("goodsTotalPrice")
    private BigDecimal goodsTotalPrice;
    @JsonProperty("addressId")
    private Integer addressId;
    @JsonProperty("grouponPrice")
    private BigDecimal grouponPrice;
    @JsonProperty("checkedAddress")
    private WXAddressDetailVo checkedAddress;
    @JsonProperty("couponPrice")
    private BigDecimal couponPrice;
    @JsonProperty("availableCouponLength")
    private Integer availableCouponLength;
    @JsonProperty("freightPrice")
    private BigDecimal freightPrice;
    @JsonProperty("checkedGoodsList")
    private List<Cart> checkedGoodsList;


}
