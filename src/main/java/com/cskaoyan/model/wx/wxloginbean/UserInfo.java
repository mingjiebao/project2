package com.cskaoyan.model.wx.wxloginbean;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author : [Dragon]
 * @createTime : [2022/1/10 15:32]
 * @description :登录返回信息类的中间类
 */
@Data
@NoArgsConstructor
public class UserInfo {
    private String nickName;
    private String avatarUrl;
}
