package com.cskaoyan.model.wx.wxloginbean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author : [Dragon]
 * @createTime : [2022/1/10 18:49]
 * @description :
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoginOutVo {

    private LoginVo loginVo;
    private Integer id;
}
