package com.cskaoyan.model.wx.wxloginbean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author : [Dragon]
 * @createTime : [2022/1/10 15:28]
 * @description :登录返回信息类
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoginVo {
    private UserInfo UserInfo;
    private String token;
}
