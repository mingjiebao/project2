package com.cskaoyan.model.wx.aftersale;

import com.cskaoyan.model.BaseRespParam;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @package: com.cskaoyan.model.wx.aftersale
 * @Description: 小程序售后信息显示
 * @author: 北青
 * @date: 2022/1/12 23:18
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class WxAftersaleListVo extends BaseRespParam {
    List<WxAftersaleVo> list;
}
