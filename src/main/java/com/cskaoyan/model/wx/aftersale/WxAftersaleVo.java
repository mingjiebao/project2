package com.cskaoyan.model.wx.aftersale;


import com.cskaoyan.model.admin.marketbeans.pojo.MarketAftersale;
import com.cskaoyan.model.wx.order.vo.WxOrderDetailGoodsVo;
import lombok.Data;

import java.util.List;

/**
 * @package: com.cskaoyan.model.wx.aftersale
 * @Description: 小程序售后信息列表显示中售后信息封装
 * @author: 北青
 * @date: 2022/1/12 23:20
 */
@Data
public class WxAftersaleVo {
    List<WxOrderDetailGoodsVo> goodsList;
    MarketAftersale aftersale;
}
