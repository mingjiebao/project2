package com.cskaoyan.model.wx.wxindexbean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author : [Dragon]
 * @createTime : [2022/1/10 17:18]
 * @description :
 */
@AllArgsConstructor
@Data
@NoArgsConstructor
public class LoginIndexDto {
    private Integer unrecv;
    private Integer uncomment;
    private Integer unpaid;
    private Integer unship;
}
