package com.cskaoyan.model.wx.wxindexbean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author : [Dragon]
 * @createTime : [2022/1/10 17:14]
 * @description :
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class LoginIndexVo {

    private LoginIndexDto order;


}
