package com.cskaoyan.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PageLimitRepsVo {
    private Integer limit;
    private Integer page;
    private Integer pages;
    private Integer total;
    private Object list;
}
