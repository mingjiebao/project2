package com.cskaoyan.model;

import lombok.Data;

/**
 * @description:
 * @keypoint:
 * @tags:
 * @related:
 */

@Data
public class FileUploadDTO {
    String key; //UUID的唯一值
    String name;//文件名
    String type;//文件对象
    Long size;//文件大小
    String url;//文件的相对路径

    public static FileUploadDTO create(String key, String name, String type,Long size,String url){
        FileUploadDTO fileUploadDTO = new FileUploadDTO();

        fileUploadDTO.setKey(key);
        fileUploadDTO.setName(name);
        fileUploadDTO.setType(type);
        fileUploadDTO.setSize(size);
        fileUploadDTO.setUrl(url);
        return fileUploadDTO;
    }
}
