package com.cskaoyan.model;

import lombok.Data;

/**
 * @description:
 * @keypoint:
 * @tags:
 * @related:
 */

@Data
public class BaseRespVo<T> {
    T data;
    String errmsg;
    Integer errno;

    public static BaseRespVo ok() {
        BaseRespVo<Object> baseRespVo = new BaseRespVo<>();
        baseRespVo.setErrno(0);
        baseRespVo.setErrmsg("成功");
        return baseRespVo;
    }

    //    @JsonFormat(pattern = "yyyy/MM/dd")
    public static BaseRespVo ok(Object data) {
        BaseRespVo<Object> baseRespVo = new BaseRespVo<>();
        baseRespVo.setData(data);
        baseRespVo.setErrno(0);
        baseRespVo.setErrmsg("成功");
        return baseRespVo;
    }

    public static BaseRespVo ok(Object data, String msg) {
        BaseRespVo<Object> baseRespVo = new BaseRespVo<>();
        baseRespVo.setData(data);
        baseRespVo.setErrno(0);
        baseRespVo.setErrmsg(msg);
        return baseRespVo;
    }

    public static BaseRespVo fail() {
        BaseRespVo<Object> baseRespVo = new BaseRespVo<>();
        baseRespVo.setErrno(500);
        baseRespVo.setErrmsg("失败");
        return baseRespVo;
    }

    public static BaseRespVo fail(String msg) {
        BaseRespVo<Object> baseRespVo = new BaseRespVo<>();
        baseRespVo.setErrno(500);
        baseRespVo.setErrmsg(msg);
        return baseRespVo;
    }
}
