package com.cskaoyan.model;

import lombok.Data;

/**
 * @package: com.cskaoyan.model
 * @Description: 返回参数——查询信息显示时,关于分页信息的封装
 * @author: 北青
 * @date: 2022/1/7 9:20
 */
@Data
public class BaseRespParam {

    /**
     * total : 总条数
     * pages : 总页数
     * limit : 每页最大条数
     * page :  当前页数
     */
    private int total;
    private int pages;
    private int limit;
    private int page;

    /**
     * 获取分页显示信息的方法
     * @param total
     * @param pages
     * @param limit
     * @param page
     * @return
     */
    public static BaseRespParam getPage(int total, int pages, int limit, int page){
        BaseRespParam baseRespParam = new BaseRespParam();
        baseRespParam.setTotal(total);
        baseRespParam.setPages(pages);
        baseRespParam.setLimit(limit);
        baseRespParam.setPage(page);
        return baseRespParam;
    }
}
