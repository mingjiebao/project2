package com.cskaoyan.model;

import lombok.Data;
import java.util.List;

/**
 * @Author: AhaNg
 * @description: admin/comment/list
 * @return:
 */
@Data
public class BaseData {
    Integer page;
    Integer limit;
    Integer total;
    Integer pages;
    List list;

    public static BaseData list(List list, long total, int pages, int limit, int page) {
        BaseData data = new BaseData();
        data.setList(list);
        data.setTotal((int) total);
        data.setPages(pages);
        data.setLimit(limit);
        data.setPage(page);
        return data;
    }
}

