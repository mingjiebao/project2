package com.cskaoyan.model;

import lombok.Data;

/**
 * @description: 基本接受参数类型 类似 page=1&limit=20&sort=add_time&order=desc  分页信息
 * @keypoint:
 * @tags:
 * @related:
 */
@Data
public class BaseParam {
    Integer page;
    Integer limit;
    String sort;
    String order;
}
