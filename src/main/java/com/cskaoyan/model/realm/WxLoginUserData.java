package com.cskaoyan.model.realm;

import lombok.Data;

/**
 * @Author:
 * @description:
 * @keypoint:
 * @tags:
 * @related:
 */
@Data
public class WxLoginUserData {
    Integer id; // 用户id
    String username; //用户名字
    String login_time; //本次登录时间
    String login_ip; //本次登录的ip
}
