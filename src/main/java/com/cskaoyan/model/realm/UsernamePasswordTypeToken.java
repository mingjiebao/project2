package com.cskaoyan.model.realm;

import lombok.Data;
import org.apache.shiro.authc.UsernamePasswordToken;

/**
 * @Author:
 * @description:
 * @keypoint:
 * @tags:
 * @related:
 */

@Data
public class UsernamePasswordTypeToken extends UsernamePasswordToken {
    String type;

    public UsernamePasswordTypeToken(String username,String password,String host,String type){
        super(username,password,host);
        this.type = type;
    }
}
