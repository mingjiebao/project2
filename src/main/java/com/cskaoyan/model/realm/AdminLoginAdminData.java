package com.cskaoyan.model.realm;

import lombok.Data;

import java.util.List;

/**
 * @Author:
 * @description:
 * @keypoint:
 * @tags:
 * @related:
 */
@Data
public class AdminLoginAdminData {
    private Integer id;
    private String username;
    private String password;
    private String avatar;
    private String last_login_ip;
    private Integer[] roleIds;
    private List<String> roleNames;
    private List<String> perms;
}
