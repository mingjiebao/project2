package com.cskaoyan.model.admin.coupon.bo;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CouponUserBo {
    private Integer page;
    private Integer limit;
    private Integer couponId;
    private Integer userId;
    private Integer status;
    private String sort;
    private String desc;
}
