package com.cskaoyan.model.admin.coupon.bo;

import com.cskaoyan.annotation.DateValidate;
import com.cskaoyan.annotation.IntValidate;
import com.cskaoyan.annotation.NonNegativeValidate;
import lombok.Data;
import lombok.NoArgsConstructor;


import javax.validation.constraints.*;
import java.util.List;

@NoArgsConstructor
@Data
@DateValidate(message = "天数不能为负数，或开始和结束时间设置有问题")
public class CouponCreateInfoBo {

    @NotNull(message = "优惠券名称不能为null")
    private String name;


    private String desc;
    private String tag;

    @NotEmpty
    @IntValidate(message = "优惠券总数必须为自然数")
    @Min(value = 1,message = "最低数量必须为1")
    private String total; // 总数不能为负数

    @NotEmpty
    @NonNegativeValidate(message = "满减金额必须大于0")
    private String discount;

    @NotEmpty
    @NonNegativeValidate(message = "最低消费必须大于0")
    private String min;

    @NotEmpty
    @NonNegativeValidate(message = "每人限领必须大于0")
    private String limit;

    private Integer type;
    private Integer status;

    private Integer goodsType;
    private List<Integer> goodsValue;
    private Integer timeType;
    private Integer days;

    private String startTime;
    private String endTime;
    private List<CouponGoodsListBo> couponGoodsList;

    // 事后处理的数据
    private String goodsValueOfString;
    private String code;
    private Integer id;
}
