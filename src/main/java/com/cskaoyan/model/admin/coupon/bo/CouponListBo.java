package com.cskaoyan.model.admin.coupon.bo;


import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CouponListBo {
    private Integer page;
    private Integer limit;
    private String name;
    private Integer type;
    private Integer status;
    private String sort;
    private String order;
}
