package com.cskaoyan.model.admin.coupon.vo;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class CouponUserVo {
    private Integer id;
    private Integer userId;
    private Integer couponId;
    private Integer status;
    private String startTime;
    private String endTime;
    private String addTime;
    private String updateTime;
    private Boolean deleted;
}
