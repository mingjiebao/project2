package com.cskaoyan.model.admin.coupon.vo;

import lombok.Data;
import lombok.NoArgsConstructor;


@NoArgsConstructor
@Data
public class CouponReadVo {

    private Integer id;
    private String name;
    private String desc;
    private String tag;
    private String total;
    private Double discount;
    private Double min;
    private Integer limit;
    private String code;
    private Integer type;
    private Integer status;
    private Integer goodsType;
    private String[] goodsValue;
    private Integer timeType;
    private Integer days;
    private String addTime;
    private String updateTime;
    private Boolean deleted;
}
