package com.cskaoyan.model.admin.coupon.vo;

import com.cskaoyan.model.admin.pojo.MarketCouponPojo;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
public class CouponListDataVo {
    private Integer id;
    private String name;
    private String desc;
    private String tag;
    private String total;
    private BigDecimal discount;
    private BigDecimal min;
    private Integer limit;
    private Integer type;
    private Integer status;
    private Integer goodsType;
    // 提前创建，防止空值
    private List<Integer> goodsValue = new ArrayList<>();
    private String code;
    private Short timeType;
    private Short days;
    private String addTime;
    private String updateTime;
    private Boolean deleted;

    public CouponListDataVo(MarketCouponPojo pojo) {
        this.id = pojo.getId();
        this.name = pojo.getName();
        this.desc = pojo.getDesc();
        this.tag = pojo.getTag();

        this.total = String.valueOf(pojo.getTotal());
        if (pojo.getTotal() == -1) {
            this.total = "不限";
        } else if (pojo.getTotal() == 0) {
            this.total = "售空";
        }
        this.discount = pojo.getDiscount();
        this.min = pojo.getMin();

        this.limit = pojo.getLimit();
        this.type = pojo.getType();
        this.status = pojo.getStatus();
        this.goodsType = pojo.getGoodsType();

        // GoodsValue比较特殊
        setGoodsValue(pojo.getGoodsValue());

        this.code = pojo.getCode();
        this.timeType = pojo.getTimeType();
        this.days = pojo.getDays();
        this.addTime = pojo.getAddTime().toString();
        this.updateTime = pojo.getUpdateTime().toString();

        setDeleted(pojo.getDeleted());
    }

    /**
     * 运行到setter就说明goodsValue有值
     *
     * @param goodsValue:[1，2，3] 字符串长得像数字数组
     */
    public void setGoodsValue(String goodsValue) {
        // 长度等于2的情况就是空数组[]
        if (goodsValue.length() != 2) {
            goodsValue = goodsValue.substring(1, goodsValue.length() - 1);
            String[] arrays = goodsValue.split(",");
            for (String array : arrays) {
                this.goodsValue.add(Integer.valueOf(array.trim()));
            }
        }
    }

    public void setDeleted(Integer deleted) {
        if (deleted == 1) {
            this.deleted = true;
        } else {
            this.deleted = false;
        }
    }
}
