package com.cskaoyan.model.admin.coupon.bo;

import com.cskaoyan.annotation.IntValidate;
import com.cskaoyan.annotation.NonNegativeValidate;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import java.util.List;

@NoArgsConstructor
@Data
public class CouponUpdateBo {

    private Integer id;
    private String name;
    private String desc;
    private String tag;

    @IntValidate(message = "总数不能为负数或者小数")
    private String total;

    @NotEmpty(message = "折扣不能为空")
    @NonNegativeValidate(message = "折扣不能为负数")
    private String discount;

    @NotEmpty(message = "最小消费金额不能为空")
    @NonNegativeValidate(message = "低价必须大于0哦")
    private String min;

    @NotEmpty(message = "没人限额不能为空")
    @IntValidate(message = "每人限领必须大于0的整数")
    private String limit;

    private Integer type;
    private Integer status;
    private Integer goodsType;
    private List<Integer> goodsValue;
    private String code;
    private Integer timeType;
    private Integer days;
    private String addTime;
    private String updateTime;
    private Boolean deleted;
    private Integer daysType;
    private String startTime;
    private String endTime;
    private List<CouponGoodsListDTO> couponGoodsList;

    private String goodsValueOfString;

    @NoArgsConstructor
    @Data
    public static class CouponGoodsListDTO {
        private Integer goodsId;
        private String goodsName;
        private String goodsSn;
    }
}
