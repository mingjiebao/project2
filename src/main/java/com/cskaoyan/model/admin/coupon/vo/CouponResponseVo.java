package com.cskaoyan.model.admin.coupon.vo;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@Data
public class CouponResponseVo {

    private Integer total;
    private Integer pages;
    private Integer limit;
    private Integer page;
    private Object list;
}
