package com.cskaoyan.model.admin.coupon.bo;

import lombok.Data;

@Data
public class CouponGoodsListBo {
    private Integer goodsId;
    private String goodsName;
    private String goodsSn;
}
