package com.cskaoyan.model.admin.coupon.vo;

import com.cskaoyan.model.admin.coupon.bo.CouponUpdateBo;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@Data
public class CouponAddDataVo {

    private Integer id;
    private String name;
    private String desc;
    private String tag;
    private String total;
    private String discount;
    private String min;
    private String limit;
    private Integer type;
    private Integer status;
    private Integer goodsType;
    private List<Integer> goodsValue;
    private Integer timeType;
    private Integer days;
    private String startTime;
    private String endTime;
    private String addTime;
    private String updateTime;

    public CouponAddDataVo(CouponUpdateBo bo) {
        this.id = bo.getId();
        this.name = bo.getName();
        this.desc = bo.getDesc();
        this.tag = bo.getTag();
        this.total = bo.getTotal();
        this.discount = bo.getDiscount();
        this.min = bo.getMin();
        this.limit = bo.getLimit();
        this.type = bo.getType();
        this.status = bo.getStatus();
        this.goodsType = bo.getGoodsType();
        this.goodsValue = bo.getGoodsValue();
        this.timeType = bo.getTimeType();
        this.days = bo.getDays();
        this.startTime = bo.getStartTime();
        this.endTime = bo.getEndTime();
        this.addTime = bo.getAddTime();
        this.updateTime = bo.getUpdateTime();
    }
}
