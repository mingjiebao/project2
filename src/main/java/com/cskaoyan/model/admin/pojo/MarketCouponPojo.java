package com.cskaoyan.model.admin.pojo;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

@Data
@NoArgsConstructor
public class MarketCouponPojo {
    private Integer id;
    private String name;
    private String desc;
    private String tag;
    // 0代表无限
    private Integer total;

    private BigDecimal discount;
    private BigDecimal min; //使用消费券的最低金额
    private Integer limit;
    private Integer type;

    private Integer status;

    private Integer goodsType;

    private String goodsValue;

    private String code;

    private Short timeType;

    private Short days;

    private Date startTime;

    private Date endTime;

    private Date addTime;

    private Date updateTime;

    private Integer deleted;
}
