package com.cskaoyan.model.admin.pojo;

import lombok.Data;

@Data
public class MarketAdPojo {
    private Integer id;
    private String name;
    private String link;
    private String url;
    private Integer position;
    private String content;
    private String startTime;
    private String endTime;
    private Integer enabled;
    private String addTime;
    private String updateTime;
    private Integer deleted;
}
