package com.cskaoyan.model.admin.pojo;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
public class MarketTopicPojo {
    private Integer id;
    private String title;
    private String subtitle;
    private String content;
    private BigDecimal price;
    private String readCount;
    private String picUrl;
    private Integer sortOrder;
    private String goods;
    private String addTime;
    private String updateTime;
    private Integer deleted;
}
