package com.cskaoyan.model.admin.ad.vo;

import com.cskaoyan.model.admin.ad.bo.AdListBO;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@Data
public class AdListVo {
    private Integer total;
    private Integer pages;
    private Integer limit;
    private Integer page;
    private List<AdListDataVo> list;

    public AdListVo(AdListBO listBO) {
        this.pages = listBO.getPage();
        this.limit = listBO.getLimit();
        this.page = listBO.getPage();
    }
}
