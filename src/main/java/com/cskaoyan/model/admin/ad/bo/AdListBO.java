package com.cskaoyan.model.admin.ad.bo;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class AdListBO {
    private Integer page;
    private Integer limit;
    //    广告名称
    private String name;
    //    广告内容
    private String content;
    private String sort;
    private String order;
}
