package com.cskaoyan.model.admin.ad.bo;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@NoArgsConstructor
@Data
public class AdUpdateBo {
    @NotNull(message = "广告信息的ID不能为Null")
    private Integer id;

    @NotNull(message = "广告名称不能为null")
    @NotEmpty(message = "广告名称不能为空")
    private String name;

    private String link;
    private String url;
    private Integer position;
    private String content;
    private Boolean enabled;
    private String addTime;
    private String updateTime;
    private Boolean deleted;
}
