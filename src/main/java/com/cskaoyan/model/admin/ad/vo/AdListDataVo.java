package com.cskaoyan.model.admin.ad.vo;


import com.cskaoyan.model.admin.pojo.MarketAdPojo;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class AdListDataVo {

    private Integer id;

    private String name;

    private String url;

    private Integer position;

    private String content;


    private String link;
    private Boolean enabled;
    private String addTime;
    private String updateTime;
    private Boolean deleted;

    public AdListDataVo(MarketAdPojo pojo) {
        this.id = pojo.getId();
        this.name = pojo.getName();
        this.link = pojo.getLink();
        this.url = pojo.getUrl();
        this.position = pojo.getPosition();
        this.content = pojo.getContent();
        this.addTime = pojo.getAddTime();
        this.updateTime = pojo.getUpdateTime();

        if (this.enabled = pojo.getEnabled() != null) {
            this.enabled = pojo.getEnabled() == 1;
        }

        if (pojo.getDeleted() != null) {
            this.deleted = pojo.getDeleted() != 0;
        }
    }
}
