package com.cskaoyan.model.admin.ad.bo;


import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class AdCreateBO {
    private String name;
    private String content;
    private String url;
    private String link;
    private Integer position;
    private Boolean enabled;
}
