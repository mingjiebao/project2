package com.cskaoyan.model.admin.configmanage.vo;

import lombok.Data;

@Data
public class ConfigMallVO {
    private String market_mall_longitude;
    private String market_mall_latitude;
    private String market_mall_address;
    private String market_mall_qq;
    private String market_mall_phone;
    private String market_mall_name;
}
