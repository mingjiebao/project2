package com.cskaoyan.model.admin.configmanage.vo;

import lombok.Data;

@Data
public class ConfigExpressVO {
    private String market_express_freight_min;
    private String market_express_freight_value;
}
