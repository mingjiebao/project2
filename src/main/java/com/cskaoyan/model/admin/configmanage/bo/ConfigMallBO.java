package com.cskaoyan.model.admin.configmanage.bo;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
public class ConfigMallBO {

    @NotNull
    @Pattern(regexp="^(\\-|\\+)?(((\\d|[1-9]\\d|1[0-7]\\d|0{1,3})\\.\\d{0,6})|(\\d|[1-9]\\d|1[0-7]\\d|0{1,3})|180\\.0{0,6}|180)$", message = "输入的经度范围应为-180°～180°，请重新输入")
    private String market_mall_longitude;

    @NotNull
    @Pattern(regexp="^(\\-|\\+)?([0-8]?\\d\\.\\d{0,6}|90\\.0{0,6}|[0-8]?\\d|90)$", message = "输入的纬度的范围应为-90°～−90°，请重新输入")
    private String market_mall_latitude;

    @NotNull
    private String market_mall_address;

    @NotNull
    @Pattern(regexp="^[0-9]*$", message = "输入的QQ号码必须是纯数字，请重新输入")
    @Length(max = 11, min = 5, message = "输入的QQ号码长度为5-11位，请重新输入")
    private String market_mall_qq;

    @NotNull
    @Pattern(regexp="^(((010)|(0[2-9]\\d)|(13\\d)|(14[57])|(15[0-35-9])|(166)|(17[0-35-9])|(18\\d)|(19[13589]))\\-\\d{4}\\-\\d{4})|(0\\d{3}\\-\\d{3}\\-\\d{4})$", message = "输入的电话号码格式有误，请重新输入")
    //电话格式为xxx-xxxx-xxxx或者xxxx-xxx-xxxx
    private String market_mall_phone;

    @NotNull
    private String market_mall_name;
}
