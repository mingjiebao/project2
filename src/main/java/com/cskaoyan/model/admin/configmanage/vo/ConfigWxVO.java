package com.cskaoyan.model.admin.configmanage.vo;

import lombok.Data;

@Data
public class ConfigWxVO {
    private String market_wx_catlog_goods;
    private String market_wx_catlog_list;
    private String market_wx_index_brand;
    private String market_wx_index_hot;
    private String market_wx_index_new;
    private String market_wx_index_topic;
    private String market_wx_share;
}
