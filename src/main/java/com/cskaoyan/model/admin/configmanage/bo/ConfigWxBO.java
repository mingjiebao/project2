package com.cskaoyan.model.admin.configmanage.bo;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
public class ConfigWxBO {

    @NotNull
    @Pattern(regexp = "^[0-9]*$", message = "输入的显示数量应为数字，请查证后输入")
    @Min(value = 0, message = "输入的显示数量不可能为负数，请查证后输入")
    private String market_wx_catlog_goods;

    @NotNull
    @Pattern(regexp = "^[0-9]*$", message = "输入的显示数量应为数字，请查证后输入")
    @Min(value = 0, message = "输入的显示数量不可能为负数，请查证后输入")
    private String market_wx_catlog_list;

    @NotNull
    @Pattern(regexp = "^[0-9]*$", message = "输入的显示数量应为数字，请查证后输入")
    @Min(value = 0, message = "输入的显示数量不可能为负数，请查证后输入")
    private String market_wx_index_brand;

    @NotNull
    @Pattern(regexp = "^[0-9]*$", message = "输入的显示数量应为数字，请查证后输入")
    @Min(value = 0, message = "输入的显示数量不可能为负数，请查证后输入")
    private String market_wx_index_hot;

    @NotNull
    @Pattern(regexp = "^[0-9]*$", message = "输入的显示数量应为数字，请查证后输入")
    @Min(value = 0, message = "输入的显示数量不可能为负数，请查证后输入")
    private String market_wx_index_new;

    @NotNull
    @Pattern(regexp = "^[0-9]*$", message = "输入的显示数量应为数字，请查证后输入")
    @Min(value = 0, message = "输入的显示数量不可能为负数，请查证后输入")
    private String market_wx_index_topic;

    @NotNull
    private String market_wx_share;
}
