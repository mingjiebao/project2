package com.cskaoyan.model.admin.configmanage.vo;

import lombok.Data;

@Data
public class ConfigOrderVO {
    private String market_order_comment;
    private String market_order_unconfirm;
    private String market_order_unpaid;
}
