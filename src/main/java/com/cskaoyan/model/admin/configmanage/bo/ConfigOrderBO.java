package com.cskaoyan.model.admin.configmanage.bo;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
public class ConfigOrderBO {

    @NotNull
    @Pattern(regexp = "^[0-9]*$", message = "输入的收货后超期时间应为纯数字，请重新输入")
    @Min(value = 0, message = "输入的收货后超期时间不可能为负数，请重新输入")
    private String market_order_comment;

    @NotNull
    @Pattern(regexp = "^[0-9]*$", message = "输入的发货后超期时间应为纯数字，请重新输入")
    @Min(value = 0, message = "输入的发货后超期时间不可能为负数，请重新输入")
    private String market_order_unconfirm;

    @NotNull
    @Pattern(regexp = "^[0-9]*$", message = "输入的下单后超期时间应为纯数字，请重新输入")
    @Min(value = 0, message = "输入的下单后超期时间不可能为负数，请重新输入")
    private String market_order_unpaid;
}
