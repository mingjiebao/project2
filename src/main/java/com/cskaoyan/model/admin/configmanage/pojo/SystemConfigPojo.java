package com.cskaoyan.model.admin.configmanage.pojo;

import lombok.Data;

import java.util.Date;

@Data
public class SystemConfigPojo {
    private Integer id;
    private String configName;
    private String configValue;
    private Date addTime;
    private Date updateTime;
    private Boolean deletedFlag;
}
