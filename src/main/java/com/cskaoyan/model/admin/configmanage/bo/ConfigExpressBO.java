package com.cskaoyan.model.admin.configmanage.bo;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
public class ConfigExpressBO {

    @NotNull
    @Pattern(regexp = "^[0-9]*$", message = "输入的满减最低消费应为纯数字，请重新输入")
    @Min(value = 0, message = "输入的最低消费不可能为负数，请重新输入")
    private String market_express_freight_min;

    @NotNull
    @Pattern(regexp = "^[0-9]*$", message = "输入的运费应为纯数字，请重新输入")
    @Min(value = 0, message = "输入的运费不可能为负数，请重新输入")
    private String market_express_freight_value;
}
