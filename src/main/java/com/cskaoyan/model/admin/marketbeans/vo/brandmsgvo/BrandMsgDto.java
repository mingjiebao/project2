package com.cskaoyan.model.admin.marketbeans.vo.brandmsgvo;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * @author : [Dragon]
 * @version : [v1.0]
 * @className : BrandMsgDto
 * @createTime : [2022/1/7 17:20]
 * @description :商城管理模块-品牌制造商-获取品牌商信息-中间类
 */

@Data
@NoArgsConstructor
public class BrandMsgDto {
    private Integer id;
    private String name;
    private String desc;
    private String picUrl;
    private Integer sortOrder;
    private BigDecimal floorPrice;
    private String addTime;
    private String updateTime;
    private Integer deleted;
}
