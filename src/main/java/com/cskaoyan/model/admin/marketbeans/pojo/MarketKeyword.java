package com.cskaoyan.model.admin.marketbeans.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @package: com.cskaoyan.model.admin.marketbeans.pojo
 * @Description: 数据库——商城——关键词
 * @author: 北青
 * @date: 2022/1/8 10:02
 */
@Data
public class MarketKeyword {
    Integer id; //关键词id
    String keyword; //关键字
    String url; //关键字跳转链接
    Boolean isHot; //是否是热门关键字
    Boolean isDefault; //是否是默认关键字
    Integer sortOrder; //排序

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    Date addTime; //创建时间

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    Date updateTime; //更新时间
    Boolean deleted; //逻辑删除

}
