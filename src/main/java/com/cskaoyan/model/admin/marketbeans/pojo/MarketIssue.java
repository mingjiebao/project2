package com.cskaoyan.model.admin.marketbeans.pojo;

import lombok.Data;

/**
 * @package: com.cskaoyan.model.admin.marketbeans.pojo
 * @Description: 数据库——商城——通用问题
 * @author: 北青
 * @date: 2022/1/7 20:30
 */
@Data
public class MarketIssue {
    Integer id; //问题id
    String question; //问题标题
    String answer; //问题答案
    String addTime; //创建时间
    String updateTime; //更新时间
    Boolean deleted; //逻辑删除
}
