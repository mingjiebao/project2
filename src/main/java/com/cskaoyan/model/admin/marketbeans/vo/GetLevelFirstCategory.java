package com.cskaoyan.model.admin.marketbeans.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author : [Dragon]
 * @createTime : [2022/1/8 21:48]
 * @description :
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class GetLevelFirstCategory {

    private int total;
    private int pages;
    private int limit;
    private int page;
    private List<GetLevelFirstChirldCategoryDto> list;

}
