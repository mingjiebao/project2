package com.cskaoyan.model.admin.marketbeans.bo;

import com.cskaoyan.model.BaseParam;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @package: com.cskaoyan.model.admin.marketbeans.bo
 * @Description: 售后信息模块显示全部售后信息的请求参数封装
 * @author: 北青
 * @date: 2022/1/6 23:29
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class MarketAftersaleListBo extends BaseParam {
    String aftersaleSn; //售后编号
    Integer orderId; //订单ID
    Integer status; //售后状态，0是可申请，1是用户已申请，2是管理员审核通过，3是管理员退款成功，4是管理员审核拒绝，5是用户已取消
}
