package com.cskaoyan.model.admin.marketbeans.bo;

import com.cskaoyan.model.BaseParam;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @package: com.cskaoyan.model.admin.marketbeans.bo
 * @Description: 商城——关键词——关键词信息显示请求参数封装
 * @author: 北青
 * @date: 2022/1/8 10:00
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class MarketKeywordListBo extends BaseParam {
    String keyword; //查询的关键词
    String url; //查询的跳转链接
}
