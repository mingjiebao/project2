package com.cskaoyan.model.admin.marketbeans.vo;

import lombok.Data;

import java.util.List;

/**
 * @package: com.cskaoyan.model.admin.marketbeans.pojo
 * @Description: 订单详情显示元素_商品信息
 * @author: 北青
 * @date: 2022/1/9 17:17
 */
@Data
public class MarketOrderGoodsD {

    /**
     * productId : 101
     * addTime : 2022-01-07 16:22:47
     * orderId : 22
     * goodsId : 1084003
     * goodsSn : 1084003
     * updateTime : 2022-01-07 16:22:47
     * specifications : ["标准"]
     * number : 1
     * picUrl : http://yanxuan.nosdn.127.net/cf40c167e7054fe184d49f19121f63c7.png
     * deleted : false
     * price : 199
     * comment : 0
     * id : 25
     * goodsName : 纯棉美式绞花针织盖毯
     */
    private Integer productId;
    private String addTime;
    private Integer orderId;
    private Integer goodsId;
    private String goodsSn;
    private String updateTime;
    private List<String> specifications;
    private Integer number;
    private String picUrl;
    private Boolean deleted;
    private Integer price;
    private Integer comment;
    private Integer id;
    private String goodsName;
}
