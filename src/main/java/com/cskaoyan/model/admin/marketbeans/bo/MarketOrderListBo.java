package com.cskaoyan.model.admin.marketbeans.bo;

import com.cskaoyan.model.BaseParam;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * @package: com.cskaoyan.model.admin.marketbeans.bo
 * @Description: 商城——订单管理——订单信息显示——根据条件获取订单信息的请求参数封装
 * @author: 北青
 * @date: 2022/1/8 22:49
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class MarketOrderListBo extends BaseParam {
    String[] timeArray;
    Integer[] orderStatusArray;
    Integer orderId;
    String start;
    String end;
    Integer userId;
    String OrderSn;
}
