package com.cskaoyan.model.admin.marketbeans.vo.Regionvo;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author : [邓京龙]
 * @version : [v1.0]
 * @className : ProvinceDto
 * @createTime : [2022/1/7 11:58]
 * @description :商城管理模块-行政区域的数据-省
 */
@Data
@NoArgsConstructor
public class ProvinceDto {
    private int id;
    private String name;
    private int type;
    private int code;
    private List<CityDto> children;
}
