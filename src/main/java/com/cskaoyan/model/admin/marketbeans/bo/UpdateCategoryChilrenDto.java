package com.cskaoyan.model.admin.marketbeans.bo;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author : [Dragon]
 * @createTime : [2022/1/9 11:11]
 * @description :
 */

@Data
@NoArgsConstructor
public class UpdateCategoryChilrenDto {
    private Integer id;
    private Integer pid;
    private String name;
    private String keywords;
    private String desc;
    private String iconUrl;
    private String picUrl;
    private String level;
}
