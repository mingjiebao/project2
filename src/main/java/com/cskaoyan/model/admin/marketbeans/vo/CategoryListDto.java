package com.cskaoyan.model.admin.marketbeans.vo;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author : [Dragon]
 * @createTime : [2022/1/8 21:15]
 * @description :
 */

@Data
@NoArgsConstructor
public class CategoryListDto {
    private Integer id;
    private String name;
    private String keywords;
    private String desc;
    private String iconUrl;
    private String picUrl;
    private String level;
    private List<CategoryListChildrenDto> children;

}
