package com.cskaoyan.model.admin.marketbeans.pojo;

import com.cskaoyan.annotation.NonNegativeValidate;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * @author : [Dragon]
 * @createTime : [2022/1/8 20:29]
 * @description :
 */

@NoArgsConstructor
@Data
public class UpdateBrand {

    private Integer id;
    @NotNull
    @NotEmpty
    private String name;
    @NotNull
    @NotEmpty
    private String desc;
    @NotNull
    @NotEmpty
    private String picUrl;
    private Integer sortOrder;
    @NonNegativeValidate(message = "价格必须为大于零的数")
    private String floorPrice;
    private String addTime;
    private String updateTime;
    private boolean deleted;
}
