package com.cskaoyan.model.admin.marketbeans.vo.Regionvo;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author : [邓京龙]
 * @version : [v1.0]
 * @className : CountyDto
 * @createTime : [2022/1/7 12:04]
 * @description :商城管理模块-行政区域的数据-市
 */
@Data
@NoArgsConstructor
public class CountyDto {
    private int id;
    private String name;
    private int type;
    private int code;
}
