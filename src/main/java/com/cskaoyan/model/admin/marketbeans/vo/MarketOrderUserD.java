package com.cskaoyan.model.admin.marketbeans.vo;

import lombok.Data;

/**
 * @package: com.cskaoyan.model.admin.marketbeans.pojo
 * @Description: 订单详情显示元素_用户信息
 * @author: 北青
 * @date: 2022/1/9 17:19
 */
@Data
public class MarketOrderUserD {

    /**
     * nickname : userfasdf
     * avatar :
     */
    private String nickname;
    private String avatar;

}
