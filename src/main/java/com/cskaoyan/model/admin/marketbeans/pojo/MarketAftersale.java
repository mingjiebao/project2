package com.cskaoyan.model.admin.marketbeans.pojo;

import lombok.Data;

/**
 * @package: com.cskaoyan.model.admin.marketbeans.pojo
 * @Description: 数据库——商城——售后信息
 * @author: 北青
 * @date: 2022/1/6 23:14
 */
@Data
public class MarketAftersale {
    Integer id; //售后信息id
    String aftersaleSn; //售后编号
    Integer orderId; //订单ID
    Integer userId; //用户ID
    Integer type; //售后类型，0是未收货退款，1是已收货（无需退货）退款，2用户退货退款
    String reason; //退款原因
    Double amount; //退款金额
    String[] pictures; //退款凭证图片链接数组
    String comment; //退款说明
    Integer status; //售后状态，0是可申请，1是用户已申请，2是管理员审核通过，3是管理员退款成功，4是管理员审核拒绝，5是用户已取消
    String handleTime; //管理员操作时间
    String addTime; //添加时间
    String updateTime; //更新时间
    Boolean deleted; //逻辑删除
}
