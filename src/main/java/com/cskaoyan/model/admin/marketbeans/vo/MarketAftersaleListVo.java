package com.cskaoyan.model.admin.marketbeans.vo;

import com.cskaoyan.model.BaseRespParam;
import com.cskaoyan.model.admin.marketbeans.pojo.MarketAftersale;
import lombok.Data;

import java.util.List;

/**
 * @package: com.cskaoyan.model.admin.marketbeans.vo
 * @Description: 售后模块查询售后信息结果返回参数中data属性中的参数封装
 * @author: 北青
 * @date: 2022/1/6 23:33
 */
@Data
public class MarketAftersaleListVo extends BaseRespParam {

    /**
     * list : 售后信息详细信息集合
     */

    private List<MarketAftersale> list;

    /**
     * 获得返回参数实例的方法
     * @param total: 总条数
     * @param pages: 总页数
     * @param limit: 单页最大条数
     * @param page: 当前页
     * @param list: 售后信息详情
     * @return
     */
    public static MarketAftersaleListVo addData(long total, int pages, int limit, int page, List<MarketAftersale> list){

        MarketAftersaleListVo marketAftersaleListVO = new MarketAftersaleListVo();
        marketAftersaleListVO.setTotal((int) total);
        marketAftersaleListVO.setPages(pages);
        marketAftersaleListVO.setLimit(limit);
        marketAftersaleListVO.setPage(page);

        marketAftersaleListVO.setList(list);
        return marketAftersaleListVO;



    }
}
