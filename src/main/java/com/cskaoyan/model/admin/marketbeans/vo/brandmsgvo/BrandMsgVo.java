package com.cskaoyan.model.admin.marketbeans.vo.brandmsgvo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author : [Dragon]
 * @version : [v1.0]
 * @className : BrandMsgVo
 * @createTime : [2022/1/7 16:41]
 * @description :商城管理模块-品牌制造商-获取品牌商信息-返回类
 */

@NoArgsConstructor
@AllArgsConstructor
@Data
public class BrandMsgVo {
    private int total;
    private int pages;
    private int limit;
    private int page;
    private List<BrandMsgDto> list;
}
