package com.cskaoyan.model.admin.marketbeans.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @package: com.cskaoyan.model.admin.marketbeans.pojo
 * @Description: 数据库——商城——订单
 * @author: 北青
 * @date: 2022/1/8 22:23
 */
@Data
public class MarketOrder {
    Integer id; //订单ID
    Integer userId; //用户表的用户ID
    String orderSn; //订单编号
    Integer orderStatus; //订单状态
    Integer aftersaleStatus; //售后状态，0是可申请，1是用户已申请，2是管理员审核通过，3是管理员退款成功，4是管理员审核拒绝，5是用户已取消
    String consignee; //收货人名称
    String mobile; //收货人手机号
    String address; //收货具体地址
    String message; //用户订单留言
    BigDecimal goodsPrice; //商品总费用
    BigDecimal freightPrice; //配送费用
    BigDecimal couponPrice; //优惠券减免
    BigDecimal integralPrice; //用户积分减免
    BigDecimal grouponPrice; //团购优惠价减免
    BigDecimal orderPrice; //订单费用， = goodsPrice + freightPrice - couponPrice
    BigDecimal actualPrice; //实付费用， = orderPrice - integralPrice
    String payId; //微信付款编号
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    Date payTime; //微信付款时间
    String shipSn; //发货编号
    String shipChannel; //发货快递公司
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    Date shipTime; //发货开始时间
    BigDecimal refundAmount; //实际退款金额，（有可能退款金额小于实际支付金额）
    String refundType; //退款方式
    String refundContent; //退款备注
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    Date refundTime; //退款时间
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    Date confirmTime; //用户确认收货时间
    Integer comments; //待评价订单商品数量
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    Date endTime; //订单关闭时间
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    Date addTime; //创建时间
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    Date updateTime; //更新时间
    Boolean deleted; //逻辑删除
}
