package com.cskaoyan.model.admin.marketbeans.vo;

import lombok.Data;

/**
 * @package: com.cskaoyan.model.admin.marketbeans.bo
 * @Description: 快递信息显示请求参数封装
 * @author: 北青
 * @date: 2022/1/9 16:14
 */
@Data
public class MarketChannelVo {
    String code;
    String name;
}
