package com.cskaoyan.model.admin.marketbeans.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 * @author : [Dragon]
 * @createTime : [2022/1/8 11:19]
 * @description :商城管理模块-品牌制造商-添加商品-返回类
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class BrandAddVo {

    private Integer id;
    private String name;
    private String desc;
    private String picUrl;
    private String floorPrice;
    private String addTime;
    private String updateTime;
}
