package com.cskaoyan.model.admin.marketbeans.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author : [Dragon]
 * @createTime : [2022/1/8 21:14]
 * @description :
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class CategoryListVo {

    private Integer total;
    private Integer pages;
    private Integer limit;
    private Integer page;
    private List<CategoryListDto> list;
}
