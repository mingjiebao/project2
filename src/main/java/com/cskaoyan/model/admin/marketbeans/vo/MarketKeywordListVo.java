package com.cskaoyan.model.admin.marketbeans.vo;

import com.cskaoyan.model.BaseRespParam;
import lombok.Data;

import java.util.List;

/**
 * @package: com.cskaoyan.model.admin.marketbeans.vo.Regionvo
 * @Description: 商城模块——关键字——显示信息封装
 * @author: 北青
 * @date: 2022/1/8 10:58
 */
@Data
public class MarketKeywordListVo extends BaseRespParam {
    List<MarketKeywordVo> list;
    /**
     * 获得返回参数实例的方法
     * @param total: 总条数
     * @param pages: 总页数
     * @param limit: 单页最大条数
     * @param page: 当前页
     * @param list: 信息详情
     * @return
     */
    public static MarketKeywordListVo addData(long total, int pages, int limit, int page, List list){

        MarketKeywordListVo marketKeywordListVo = new MarketKeywordListVo();
        marketKeywordListVo.setTotal((int) total);
        marketKeywordListVo.setPages(pages);
        marketKeywordListVo.setLimit(limit);
        marketKeywordListVo.setPage(page);

        marketKeywordListVo.setList(list);
        return marketKeywordListVo;
    }
}
