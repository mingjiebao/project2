package com.cskaoyan.model.admin.marketbeans.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @package: com.cskaoyan.model.admin.marketbeans.vo
 * @Description: 商城模块——关键字——显示信息封装中单个关键词元素信息
 * @author: 北青
 * @date: 2022/1/8 14:16
 */
@Data
public class MarketKeywordVo {
    Integer id; //关键词id
    String keyword; //关键字
    String url; //关键字跳转链接
    Boolean isHot; //是否是热门关键字
    Boolean isDefault; //是否是默认关键字
    Integer sortOrder; //排序

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    Date addTime; //创建时间

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    Date updateTime; //更新时间
}
