package com.cskaoyan.model.admin.marketbeans.bo;

import com.cskaoyan.annotation.NonNegativeValidate;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * @author : [Dragon]
 * @createTime : [2022/1/8 10:47]
 * @description :商城管理模块-品牌制造商-添加商品-接收类
 */

@Data
@NoArgsConstructor
public class AddBrandBo {
    Integer id;
    @NotNull
    @NotEmpty
    String desc;
    @NonNegativeValidate(message = "请输入数字")
    String floorPrice;
    @NotNull
    String name;
    @NotNull
    String picUrl;

    String addTime;
    String updateTime;

}
