package com.cskaoyan.model.admin.marketbeans.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * @author : [Dragon]
 * @createTime : [2022/1/8 22:08]
 * @description :
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class AddCategory {

    private Integer id;
    @NotEmpty
    @NotNull
    private String name;
    @NotEmpty
    @NotNull
    private String keywords;
    @NotEmpty
    @NotNull
    private String desc;
    private Integer pid;
    @NotEmpty
    @NotNull
    private String iconUrl;
    @NotEmpty
    @NotNull
    private String picUrl;
    private String level;

    private String addTime;
    private String updateTime;
}
