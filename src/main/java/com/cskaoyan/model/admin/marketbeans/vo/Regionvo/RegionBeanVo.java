package com.cskaoyan.model.admin.marketbeans.vo.Regionvo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @author : [邓京龙]
 * @version : [v1.0]
 * @className : RegionBeanVo
 * @createTime : [2022/1/6 21:52]
 * @description :返回商城管理模块-行政区域的数据
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class RegionBeanVo implements Serializable {

    private int total;
    private int pages;
    private int limit;
    private int page;
    private List<ProvinceDto> list;

}
