package com.cskaoyan.model.admin.marketbeans.vo;

import com.cskaoyan.model.BaseRespParam;
import com.cskaoyan.model.admin.marketbeans.pojo.MarketOrder;
import lombok.Data;

import java.util.List;

/**
 * @package: com.cskaoyan.model.admin.marketbeans.vo
 * @Description: 商城——订单管理——订单信息显示返回参数
 * @author: 北青
 * @date: 2022/1/8 23:10
 */
@Data
public class MarketOrderInfoVo extends BaseRespParam {

    List<MarketOrder> list;

    public static MarketOrderInfoVo addData(long total, int pages, int limit, int page, List list){

        MarketOrderInfoVo marketOrderInfoVo = new MarketOrderInfoVo();
        marketOrderInfoVo.setTotal((int) total);
        marketOrderInfoVo.setPages(pages);
        marketOrderInfoVo.setLimit(limit);
        marketOrderInfoVo.setPage(page);

        marketOrderInfoVo.setList(list);
        return marketOrderInfoVo;
    }
}
