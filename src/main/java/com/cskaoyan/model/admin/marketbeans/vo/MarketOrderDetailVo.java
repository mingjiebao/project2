package com.cskaoyan.model.admin.marketbeans.vo;

import lombok.Data;

import java.util.List;

/**
 * @package: com.cskaoyan.model.admin.marketbeans.vo
 * @Description: 商城订单详情响应参数
 * @author: 北青
 * @date: 2022/1/9 17:15
 */
@Data
public class MarketOrderDetailVo {
    MarketOrderVo order;
    List<MarketOrderGoodsD> ordergoods;
    MarketOrderUserD user;
}
