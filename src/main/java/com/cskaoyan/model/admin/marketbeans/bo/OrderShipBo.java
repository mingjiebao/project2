package com.cskaoyan.model.admin.marketbeans.bo;

import com.cskaoyan.annotation.NonNegativeValidate;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

/**
 * @author : [Dragon]
 * @createTime : [2022/1/9 21:58]
 * @description :
 */
@Data
@NoArgsConstructor
public class OrderShipBo {
    private Integer orderId;
    private String shipChannel;
    @NotNull
    @NonNegativeValidate(message = "快递编号必须为正整数")
    private String shipSn;
}
