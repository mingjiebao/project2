package com.cskaoyan.model.admin.marketbeans.pojo;

import lombok.Data;

/**
 * @package: com.cskaoyan.model.pojo
 * @Description: 数据库——商城订单——快递信息
 * @author: 北青
 * @date: 2022/1/9 16:12
 */
@Data
public class MarketChannel {
    Integer id; //快递信息id
    String code; //快递代码
    String name; //快递名称
}
