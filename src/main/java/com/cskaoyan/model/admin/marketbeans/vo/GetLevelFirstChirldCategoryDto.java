package com.cskaoyan.model.admin.marketbeans.vo;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author : [Dragon]
 * @createTime : [2022/1/8 21:49]
 * @description :
 */

@Data
@NoArgsConstructor
public class GetLevelFirstChirldCategoryDto {
    private Integer value;
    private String label;
}
