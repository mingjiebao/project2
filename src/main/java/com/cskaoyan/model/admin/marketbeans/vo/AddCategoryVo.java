package com.cskaoyan.model.admin.marketbeans.vo;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author : [Dragon]
 * @createTime : [2022/1/8 22:22]
 * @description :
 */
@NoArgsConstructor
@Data
public class AddCategoryVo {

    private int id;
    private String name;
    private String keywords;
    private String desc;
    private int pid;
    private String iconUrl;
    private String picUrl;
    private String level;
    private String addTime;
    private String updateTime;
}
