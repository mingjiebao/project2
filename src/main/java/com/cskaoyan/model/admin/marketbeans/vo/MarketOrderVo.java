package com.cskaoyan.model.admin.marketbeans.vo;

import lombok.Data;

/**
 * @package: com.cskaoyan.model.admin.marketbeans.vo
 * @Description: 订单详情信息订单信息封装
 * @author: 北青
 * @date: 2022/1/9 17:36
 */
@Data
public class MarketOrderVo {
    
    private String consignee;
    private String address;
    private Integer comments;
    private String addTime;
    private String orderSn;
    private Integer actualPrice;
    private String shipTime;
    private String mobile;
    private Integer orderStatus;
    private String updateTime;
    private String message;
    private String shipChannel;
    private Integer userId;
    private Integer grouponPrice;
    private Boolean deleted;
    private Integer aftersaleStatus;
    private Integer goodsPrice;
    private Integer couponPrice;
    private Integer orderPrice;
    private Integer id;
    private Integer freightPrice;
    private Integer integralPrice;
    private String shipSn;
}
