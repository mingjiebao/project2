package com.cskaoyan.model.admin.marketbeans.bo;

import lombok.Data;

/**
 * @package: com.cskaoyan.model.admin.marketbeans.bo
 * @Description: 售后处理——批量处理售后信息id 接收json数据时的javaBean封装
 * @author: 北青
 * @date: 2022/1/7 19:48
 */
@Data
public class BatchIdsBo {
    Integer[] ids;
}
