package com.cskaoyan.model.admin.marketbeans.vo;

import com.cskaoyan.model.BaseRespParam;
import com.cskaoyan.model.admin.marketbeans.pojo.MarketIssue;
import lombok.Data;

import java.util.List;

/**
 * @package: com.cskaoyan.model.admin.marketbeans.vo
 * @Description: 商城模块——显示信息封装
 * @author: 北青
 * @date: 2022/1/7 20:38
 */
@Data
public class MarketIssueListVo extends BaseRespParam {
    /**
     * list : 商城信息详细信息集合
     */

    private List<MarketIssue> list;

    /**
     * 获得返回参数实例的方法
     * @param total: 总条数
     * @param pages: 总页数
     * @param limit: 单页最大条数
     * @param page: 当前页
     * @param list: 售后信息详情
     * @return
     */
    public static MarketIssueListVo addData(long total, int pages, int limit, int page, List list){

        MarketIssueListVo marketIssueListVo = new MarketIssueListVo();
        marketIssueListVo.setTotal((int) total);
        marketIssueListVo.setPages(pages);
        marketIssueListVo.setLimit(limit);
        marketIssueListVo.setPage(page);

        marketIssueListVo.setList(list);
        return marketIssueListVo;
    }
}
