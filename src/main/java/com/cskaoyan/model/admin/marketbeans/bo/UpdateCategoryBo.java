package com.cskaoyan.model.admin.marketbeans.bo;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author : [Dragon]
 * @createTime : [2022/1/8 22:43]
 * @description :
 */
@NoArgsConstructor
@Data
public class UpdateCategoryBo {


    private Integer id;
    private Integer pid;
    private String name;
    private String keywords;
    private String desc;
    private String iconUrl;
    private String picUrl;
    private String level;
    private List<UpdateCategoryChilrenDto> children;

}
