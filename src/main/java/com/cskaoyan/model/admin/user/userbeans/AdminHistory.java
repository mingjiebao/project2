package com.cskaoyan.model.admin.user.userbeans;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description:
 * @author: XM
 * @date: 2022/1/8 9:15
 */
@NoArgsConstructor
@Data
public class AdminHistory {

        private Integer id;
        private Integer userId;
        private String keyword;
        private String from;
        private String addTime;
        private String updateTime;
        private Integer deleted;
}
