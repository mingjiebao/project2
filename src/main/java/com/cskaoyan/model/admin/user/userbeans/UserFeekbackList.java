package com.cskaoyan.model.admin.user.userbeans;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * @description:
 * @author: XM
 * @date: 2022/1/8 10:18
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserFeekbackList {

    private Integer id;
    private Integer userId;
    private String username;
    private String mobile;
    private String feedType;
    private String content;
    private Integer status;
    private boolean hasPicture;
    private String[] picUrls;
    private String addTime;
    private String updateTime;
    private Integer deleted;



}


