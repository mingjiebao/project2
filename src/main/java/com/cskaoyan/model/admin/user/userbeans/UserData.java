package com.cskaoyan.model.admin.user.userbeans;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @description:用户（响应）
 * @author: XM
 * @date: 2022/1/7 16:13
 */

@NoArgsConstructor
@Data
public class UserData {



    private Integer total;
    private Integer pages;
    private Integer limit;
    private Integer page;
    private List list;


        public static UserData data(long total, Integer pages, Integer limit, Integer page, List list) {
            UserData userData = new UserData();
            userData.setTotal((int)total);
            userData.setPages(pages);
            userData.setPage(page);
            userData.setLimit(limit);
            userData.setList(list);
            return userData;
        }
    }
