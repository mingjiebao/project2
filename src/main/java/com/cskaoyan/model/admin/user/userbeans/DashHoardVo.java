package com.cskaoyan.model.admin.user.userbeans;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description:
 * @author: 首页返回值类型
 * @date: 2022/1/6 21:45
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class DashHoardVo {
    /**
     * goodsTotal : 219
     * userTotal : 1
     * productTotal : 224
     * orderTotal : 0
     */

    private Integer goodsTotal;//商品总计
    private Integer userTotal;//用户总计
    private Integer productTotal;//货目总计
    private Integer orderTotal;//订单总计
}