package com.cskaoyan.model.admin.user.userbeans;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description:
 * @author: 会员管理
 * @date: 2022/1/7 15:28
 */
@NoArgsConstructor
@Data
public class UserListVo {

    /**
     * id : 1
     * username : user123
     * password : $2a$10$lTu9qi0hr19OC800Db.eludFr0AXuJUSrMHi/iPYhKRlPFeqJxlye
     * gender : 1
     * lastLoginTime : 2022-01-07 15:17:44
     * lastLoginIp : 111.175.52.36
     * userLevel : 0
     * nickname : user123
     * mobile : 12345678916
     * avatar :
     * weixinOpenid :
     * sessionKey :
     * status : 0
     * addTime : 2019-04-20 22:17:43
     * updateTime : 2022-01-07 15:17:44
     * deleted : false
     */

    private Integer id;
    private String username;
    private String password;
    private Integer gender;
    private String birthday;//
    private String lastLoginTime;
    private String lastLoginIp;
    private Integer userLevel;
    private String nickname;
    private String mobile;
    private String avatar;
    private String weixinOpenid;
    private String sessionKey;
    private Integer status;
    private String addTime;
    private String updateTime;
    private boolean deleted;
}