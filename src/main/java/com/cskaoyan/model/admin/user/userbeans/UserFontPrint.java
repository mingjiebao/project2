package com.cskaoyan.model.admin.user.userbeans;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description:
 * @author: XM
 * @date: 2022/1/7 22:25
 */
@NoArgsConstructor
@Data
public class UserFontPrint {
    private Integer id;
    private Integer userId;
    private Integer goodsId;
    private String addTime;
    private String updateTime;
    private Integer deleted;
}
