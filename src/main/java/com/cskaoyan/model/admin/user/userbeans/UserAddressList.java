package com.cskaoyan.model.admin.user.userbeans;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description:收货地址（响应）
 * @author: XM
 * @date: 2022/1/7 19:57
 */
@NoArgsConstructor
@Data
public class UserAddressList {


        private Integer id;
        private String name;
        private Integer userId;
        private String province;
        private String city;
        private String county;
        private String addressDetail;
        private String areaCode;
        private String tel;
        private Integer isDefault;
        private String addTime;
        private String updateTime;
        private Integer deleted;
    }
