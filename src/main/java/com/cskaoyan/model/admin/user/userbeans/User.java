package com.cskaoyan.model.admin.user.userbeans;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.validation.constraints.NotEmpty;

/**
 * @description:
 * @author: user
 * @date: 2022/1/7 15:28
 */
@NoArgsConstructor
@Data
public class User {

    private Integer id;

    private String username;
    @NonNull
    @NotEmpty
    private String password;

    private Integer gender;
    private String birthday;
    private String lastLoginTime;
    private String lastLoginIp;

    private Integer userLevel;
    @NonNull
    @NotEmpty
    private String nickname;
    @NonNull
    @NotEmpty
    private String mobile;
    private String avatar;
    private String weixinOpenid;
    private String sessionKey;

    private Integer status;
    private String addTime;
    private String updateTime;
    private boolean deleted;
}