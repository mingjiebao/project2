package com.cskaoyan.model.admin.user.userbeans;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description:会员收藏表
 * @author: XM
 * @date: 2022/1/7 20:54
 */
@NoArgsConstructor
@Data
public class UserCollect {
    private Integer id;
    private Integer userId;
    private Integer valueId;
    private Integer type;
    private String addTime;
    private String updateTime;
    private boolean deleted;
}
