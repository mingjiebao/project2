package com.cskaoyan.model.admin.adminmanage.build;

import lombok.Data;

import java.util.List;

/**
 * @package: com.cskaoyan.mall.bean
 * @Description:
 * @author: 北青
 * @date: 2022/1/8 16:47
 */
@Data
public class Bean1 {
//    "id": "商品管理",
//            "label": "商品管理",
//            "children": [
    String id;
    String label;
    List<Bean2> children;

}
