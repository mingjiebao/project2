package com.cskaoyan.model.admin.adminmanage.storage;

import com.cskaoyan.model.BaseParam;
import lombok.Data;

/**
 * @description:
 * @keypoint:
 * @tags:
 * @related:
 */

@Data
public class StorageParam extends BaseParam {
    String key;
    String name;
}
