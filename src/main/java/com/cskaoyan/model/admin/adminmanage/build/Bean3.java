package com.cskaoyan.model.admin.adminmanage.build;

import lombok.Data;

/**
 * @package: com.cskaoyan.mall.bean
 * @Description:
 * @author: 北青
 * @date: 2022/1/8 16:52
 */
@Data
public class Bean3 {
//    "id"       : "admin:goods:read",
//    "label"  : "详情",
//    "api"     :  "GET /admin/goods/detail"
    String id;
    String label;
    String api;

}
