package com.cskaoyan.model.admin.adminmanage.log;

import lombok.Data;

import java.util.Date;

/**
 * @Author:
 * @description:
 * @keypoint:
 * @tags:
 * @related:
 */
@Data
public class LogMessagePOJO {
    Integer id;
    String admin;
    String ip;
    Integer type;
    String action;
    Boolean status;
    String result;
    String comment;
    Date add_time;
    Date update_time;
    Boolean deleted;
}
