package com.cskaoyan.model.admin.adminmanage.role;

import lombok.Data;

import java.util.List;

/**
 * @Author:
 * @description:
 * @keypoint:
 * @tags:
 * @related:
 */

@Data
public class SystemPermissonVO {

    private List<ModuleVO> systemPermissions;


}
