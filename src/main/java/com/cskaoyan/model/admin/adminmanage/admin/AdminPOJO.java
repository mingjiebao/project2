package com.cskaoyan.model.admin.adminmanage.admin;

import lombok.Data;

/**
 * @Author:
 * @description:
 * @keypoint:
 * @tags:
 * @related:
 */
@Data
public class AdminPOJO {
    Integer id;
    String username;
    String password;
    String last_login_ip;
    String last_login_time;
    String avatar;
    String add_time;
    String update_time;
    Boolean deleted;
    Integer[] role_ids;
}
