package com.cskaoyan.model.admin.adminmanage.admin;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author:
 * @description:
 * @keypoint:
 * @tags:
 * @related:
 */

@NoArgsConstructor
@Data
public class AdminCreateVO {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("username")
    private String username;
    @JsonProperty("password")
    private String password;
    @JsonProperty("avatar")
    private String avatar;
    @JsonProperty("addTime")
    private String addTime;
    @JsonProperty("updateTime")
    private String updateTime;
    @JsonProperty("roleIds")
    private Integer[] roleIds;
}
