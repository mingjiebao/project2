package com.cskaoyan.model.admin.adminmanage.log;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author:
 * @description:
 * @keypoint:
 * @tags:
 * @related:
 */

@NoArgsConstructor
@Data
public class LogListDTO {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("admin")
    private String admin;
    @JsonProperty("ip")
    private String ip;
    @JsonProperty("type")
    private Integer type;
    @JsonProperty("action")
    private String action;
    @JsonProperty("status")
    private Boolean status;
    @JsonProperty("result")
    private String result;
    @JsonProperty("comment")
    private String comment;
    @JsonProperty("addTime")
    private String addTime;
    @JsonProperty("updateTime")
    private String updateTime;
    @JsonProperty("deleted")
    private Boolean deleted;
}
