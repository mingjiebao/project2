package com.cskaoyan.model.admin.adminmanage.build;

import lombok.Data;

import java.util.List;

/**
 * @package: com.cskaoyan.mall.bean
 * @Description:
 * @author: 北青
 * @date: 2022/1/8 16:46
 */
@Data
public class SystemPermission {
    List<Bean1> systemPermissions;
}
