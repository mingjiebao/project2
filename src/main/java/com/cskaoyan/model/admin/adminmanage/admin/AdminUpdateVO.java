package com.cskaoyan.model.admin.adminmanage.admin;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author:
 * @description:
 * @keypoint:
 * @tags:
 * @related:
 */

@NoArgsConstructor
@Data
public class AdminUpdateVO {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("username")
    private String username;
    @JsonProperty("avatar")
    private String avatar;
    @JsonProperty("updateTime")
    private String updateTime;
    @JsonProperty("roleIds")
    private Integer[] roleIds;
}
