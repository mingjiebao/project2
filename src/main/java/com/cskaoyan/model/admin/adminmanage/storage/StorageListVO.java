package com.cskaoyan.model.admin.adminmanage.storage;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @description:
 * @keypoint:
 * @tags:
 * @related:
 */

@NoArgsConstructor
@Data
public class StorageListVO {

    @JsonProperty("total")
    private Integer total;
    @JsonProperty("pages")
    private Integer pages;
    @JsonProperty("limit")
    private Integer limit;
    @JsonProperty("page")
    private Integer page;
    @JsonProperty("list")
    private List<StorageVO> list;

    public static StorageListVO create(Long total,Integer pages, Integer limit,Integer page, List<StorageVO> list){
        StorageListVO storageListVO = new StorageListVO();
        storageListVO.setTotal(Math.toIntExact(total));
        storageListVO.setPages(pages);
        storageListVO.setLimit(limit);
        storageListVO.setPage(page);
        storageListVO.setList(list);
        return storageListVO;
    }

}
