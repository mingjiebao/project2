package com.cskaoyan.model.admin.adminmanage.storage;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description:
 * @keypoint:
 * @tags:
 * @related:
 */

@NoArgsConstructor
@Data
public class StorageDeleteBO {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("key")
    private String key;
    @JsonProperty("name")
    private String name;
    @JsonProperty("type")
    private String type;
    @JsonProperty("size")
    private Integer size;
    @JsonProperty("url")
    private String url;
    @JsonProperty("addTime")
    private String addTime;
    @JsonProperty("updateTime")
    private String updateTime;
    @JsonProperty("deleted")
    private Boolean deleted;
}
