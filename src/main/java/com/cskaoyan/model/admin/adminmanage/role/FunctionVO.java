package com.cskaoyan.model.admin.adminmanage.role;

import lombok.Data;

import java.util.List;

/**
 * @Author:
 * @description:
 * @keypoint:
 * @tags:
 * @related:
 */

@Data
public class FunctionVO {


    private Integer pid;
    private String id;
    private String label;
    private List<ApiVO> children;

}
