package com.cskaoyan.model.admin.adminmanage.role;

import lombok.Data;

/**
 * @Author:
 * @description:
 * @keypoint:
 * @tags:
 * @related:
 */


@Data
public class RoleIdAndName {
    Integer id;
    String name;
}
