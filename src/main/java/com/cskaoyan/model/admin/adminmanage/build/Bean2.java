package com.cskaoyan.model.admin.adminmanage.build;

import lombok.Data;

import java.util.List;

/**
 * @package: com.cskaoyan.mall.bean
 * @Description:
 * @author: 北青
 * @date: 2022/1/8 16:49
 */
@Data
public class Bean2 {
    String id;
    String label;
    List<Bean3> children;

}
