package com.cskaoyan.model.admin.adminmanage.role;

import lombok.Data;

/**
 * @Author:
 * @description:
 * @keypoint:
 * @tags:
 * @related:
 */

@Data
public class ApiVO {
    private Integer pid;
    private String id;
//    @JsonProperty("id")
//    private Integer pid;
    private String label;
    private String api;
}
