package com.cskaoyan.model.admin.adminmanage.admin;

import lombok.Data;

/**
 * @Author:
 * @description:
 * @keypoint:
 * @tags:
 * @related:
 */

@Data
public class AdminDeleteBO {
    private Integer id;
    private String username;
    private String avatar;
    private Integer[] roleIds;
}
