package com.cskaoyan.model.admin.adminmanage.role;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @Author:
 * @description:
 * @keypoint:
 * @tags:
 * @related:
 */

@NoArgsConstructor
@Data
public class PermissionUpdateBO {

    @JsonProperty("roleId")
    private Integer roleId;
    @JsonProperty("permissions")
    private List<String> permissions;
}
