package com.cskaoyan.model.admin.adminmanage.role;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author:
 * @description:
 * @keypoint:
 * @tags:
 * @related:
 */
@NoArgsConstructor
@Data
public class RoleDTO {

    @JsonProperty("value")
    private Integer value; //对应id
    @JsonProperty("label")
    private String label; //对应name
}
