package com.cskaoyan.model.admin.adminmanage.role;

import lombok.Data;

/**
 * @Author:
 * @description:
 * @keypoint:
 * @tags:
 * @related:
 */

@Data
public class RoleCreateBO {

    private String name;
    private String desc;
}
