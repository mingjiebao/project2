package com.cskaoyan.model.admin.adminmanage.admin;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Pattern;

/**
 * @Author:
 * @description:
 * @keypoint:
 * @tags:
 * @related:
 */

@NoArgsConstructor
@Data
public class AdminCreateBO {

    @Pattern(regexp = "^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,12}$",message = "用户名不合法,需要6-12位包含字母和数字")
    private String username;
    @JsonProperty("password")
    @Length(min = 6,max = 12,message = "密码长度在6和12之间")
    private String password;
    @JsonProperty("avatar")
    private String avatar;
    @JsonProperty("roleIds")
    private Integer[] roleIds;
}
