package com.cskaoyan.model.admin.statisticsreport.pojo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class OrderPojo {
    private Integer id;

    private Integer userId;
    private String orderSn;
    private Byte orderStatus;
    private Byte aftersaleStatus;
    private String consignee;

    private String mobile;
    private String address;
    private String message;
    private BigDecimal goodsPrice;
    private BigDecimal freightPrice;

    private BigDecimal couponPrice;
    private BigDecimal integralPrice;
    private BigDecimal grouponPrice;
    private BigDecimal orderPrice;
    private BigDecimal actualPrice;

    private String payId;
    private Date payTime;
    private String shipSn;
    private String shipChannel;
    private Date shipTime;

    private BigDecimal refundAmount;
    private String refundType;
    private String refundContent;
    private Date refundTime;
    private Date confirmTime;

    private Short comments;
    private Date endTime;
    private Date addTime;
    private Date updateTime;
    private Boolean deleted;
}
