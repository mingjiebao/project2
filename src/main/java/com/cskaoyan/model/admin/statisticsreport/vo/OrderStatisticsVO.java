package com.cskaoyan.model.admin.statisticsreport.vo;

import lombok.Data;

import java.util.List;

@Data
public class OrderStatisticsVO {
    private List<String> columns;
    private List<OrderStatisticsRowVO> rows;
}
