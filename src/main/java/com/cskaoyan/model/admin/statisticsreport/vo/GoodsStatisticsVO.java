package com.cskaoyan.model.admin.statisticsreport.vo;

import lombok.Data;

import java.util.List;

@Data
public class GoodsStatisticsVO {
    private List<String> columns;
    private List<GoodsStatisticsRowVO> rows;
}
