package com.cskaoyan.model.admin.statisticsreport.vo;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class GoodsStatisticsRowVO {
    private BigDecimal amount;
    private String day;
    private Integer orders;
    private Integer products;
}
