package com.cskaoyan.model.admin.statisticsreport.vo;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class OrderStatisticsRowVO {
    private BigDecimal amount;
    private Integer customers;
    private String day;
    private Integer orders;
    private BigDecimal pcr;
}
