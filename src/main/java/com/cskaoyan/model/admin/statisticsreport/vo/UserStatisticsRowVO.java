package com.cskaoyan.model.admin.statisticsreport.vo;

import lombok.Data;

@Data
public class UserStatisticsRowVO {
    private String day;
    private Integer users;
}
