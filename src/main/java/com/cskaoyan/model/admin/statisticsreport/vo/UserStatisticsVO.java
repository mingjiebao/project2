package com.cskaoyan.model.admin.statisticsreport.vo;

import lombok.Data;

import java.util.List;

@Data
public class UserStatisticsVO {
    private List<String> columns;
    private List<UserStatisticsRowVO> rows;
}
