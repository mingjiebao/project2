package com.cskaoyan.model.admin.statisticsreport.pojo;

import lombok.Data;

import java.util.Date;

@Data
public class UserPojo {
    private Integer id;
    private String username;
    private String password;
    private Byte gender;
    private Date birthday;
    private Date lastLoginTime;
    private String lastLoginIp;
    private Byte user_level;
    private String nickName;
    private String mobile;
    private String avatar;
    private String weixinOpenId;
    private String sessionKey;
    private Byte status;
    private Date addTime;
    private Date updateTime;
    private Boolean deleted;
}
