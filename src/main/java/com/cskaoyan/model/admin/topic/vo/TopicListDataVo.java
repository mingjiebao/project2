package com.cskaoyan.model.admin.topic.vo;

import com.cskaoyan.model.admin.pojo.MarketTopicPojo;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Data
@NoArgsConstructor
public class TopicListDataVo {
    private Integer id;
    private String title;
    private String subtitle;
    private Double price;
    private String readCount;
    private String picUrl;
    private Integer sortOrder;
    private List<Integer> goods;
    private String addTime;
    private String updateTime;
    private Boolean deleted;
    private String content;

    // 数据库只保证了前两个字段非null,剩下的都要进行判断
    public TopicListDataVo(MarketTopicPojo pojo) {
        this.id = pojo.getId();
        this.title = pojo.getTitle();
        this.subtitle = Optional.ofNullable(pojo.getSubtitle()).orElse("");
        if (pojo.getPrice() != null) {
            this.price = pojo.getPrice().doubleValue();
        }
        this.readCount = Optional.ofNullable(pojo.getReadCount()).orElse("");
        this.picUrl = Optional.ofNullable(pojo.getPicUrl()).orElse("");
        this.sortOrder = Optional.ofNullable(pojo.getSortOrder()).orElse(0);

        this.goods = new ArrayList<>();
        if (pojo.getGoods().length() != 2) {
            String substring = pojo.getGoods().substring(1, pojo.getGoods().length() - 1);
            final String[] split = substring.split(",");
            for (String s : split) {
                goods.add(Integer.valueOf(s.trim()));
            }
        }

        this.addTime = pojo.getAddTime();
        this.updateTime = pojo.getUpdateTime();

        this.deleted = pojo.getDeleted() == 1;

        this.content = pojo.getContent();
    }
}
