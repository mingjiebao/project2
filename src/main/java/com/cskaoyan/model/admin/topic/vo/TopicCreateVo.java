package com.cskaoyan.model.admin.topic.vo;

import com.cskaoyan.model.admin.topic.bo.TopicCreateBo;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


@Data
@NoArgsConstructor
public class TopicCreateVo {
    private String addTime;
    private String content;
    private List<Integer> goods;
    private Integer id;
    private String picUrl;
    private Double price;
    private String subtitle;
    private String title;
    private String updateTime;

}
