package com.cskaoyan.model.admin.topic.vo;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@Data
public class TopicBaseRespVo {

    private Integer total;
    private Integer pages;
    private Integer limit;
    private Integer page;
    private List<Object> list;

}
