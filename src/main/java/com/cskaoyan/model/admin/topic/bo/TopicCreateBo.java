package com.cskaoyan.model.admin.topic.bo;

import com.cskaoyan.annotation.NonNegativeValidate;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@NoArgsConstructor
public class TopicCreateBo {
    private String content;
    private List<Integer> goods;

    //    @NotNull(message = "请至少添加一个图片")
    private String picUrl;

    @NonNegativeValidate(message = "请检查价格")
    private String price;

    private String subtitle;
    private String title;
    private String readCount;
}
