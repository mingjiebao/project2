package com.cskaoyan.model.admin.topic.bo;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class TopicListBo {
    private Integer page;
    private Integer limit;
    private String title;
    private String subtitle;
    private String sort;
    private String order;
}
