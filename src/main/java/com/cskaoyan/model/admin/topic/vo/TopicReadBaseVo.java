package com.cskaoyan.model.admin.topic.vo;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class TopicReadBaseVo {
    List<TopicGoodsListVo> goodsList;
    TopicListDataVo topic;
}
