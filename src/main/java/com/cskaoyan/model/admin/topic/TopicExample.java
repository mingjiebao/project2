package com.cskaoyan.model.admin.topic;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class TopicExample {
    private Integer id;
    private String title;
    private String subtitle;
    private String sort;
    private String order;

}

