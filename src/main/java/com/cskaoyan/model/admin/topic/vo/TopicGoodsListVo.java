package com.cskaoyan.model.admin.topic.vo;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
public class TopicGoodsListVo {
    private String brief;
    private BigDecimal counterPrice;
    private Integer id;
    private Boolean isHot;
    private Boolean isNew;
    private String name;
    private String picUrl;
    private BigDecimal retailPrice;
}
