package com.cskaoyan.model.admin.topic.bo;

import com.cskaoyan.annotation.NonNegativeValidate;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.List;

@NoArgsConstructor
@Data
public class TopicUpdateBo {
    @NotNull(message = "id不能为NULl")
    private Integer id;
    private String title;
    private String subtitle;

    @NonNegativeValidate(message = "金额不能为负数")
    private String price;
    private String readCount;

    private String picUrl;
    private Integer sortOrder;
    private List<Integer> goods;
    private String addTime;
    private String updateTime;
    private Boolean deleted;
    private String content;

    private Double topicValue;
    private String goodsValue;
}
