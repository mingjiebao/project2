package com.cskaoyan.model.admin.goodsbean;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
public class GoodsSpecification {
    private Integer id;

    private Integer goodsId;

    private String specification;

    private String value;

    private String picUrl;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date addTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;

    private Boolean deleted;

    public void setSpecification(String specification) {
        this.specification = specification == null ? null : specification.trim();
    }
    public void setValue(String value) {
        this.value = value == null ? null : value.trim();
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl == null ? null : picUrl.trim();
    }

}