package com.cskaoyan.model.admin.goodsbean.vo;

import com.cskaoyan.model.admin.goodsbean.Goods;
import lombok.Data;

import java.util.List;

/**
 * @Author AhaNg
 * @Date 2022/1/7 17:37
 * @description:
 * @return:
 */
@Data
public class GoodsDataVO {
      Integer total;
      Integer pages;
      Integer limit;
      Integer page;
      List<Goods> list;

}
