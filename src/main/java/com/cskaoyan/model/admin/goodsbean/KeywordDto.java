package com.cskaoyan.model.admin.goodsbean;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class KeywordDto {
    private Integer id;
    private Integer goods_id;
    private String keyword;
    private Integer deleted;
}
