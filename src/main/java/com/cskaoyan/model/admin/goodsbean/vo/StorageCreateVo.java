package com.cskaoyan.model.admin.goodsbean.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * @Author AhaNg
 * @Date 2022/1/7 17:37
 * @description:
 * @return:
 */

@Data
public class StorageCreateVo {

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" , timezone = "GMT+8")
    Date addTime;

    Integer id;

    String key;

    String name;

    Integer size;

    String type;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" , timezone = "GMT+8")
    Date updateTime;

    String url;

    public StorageCreateVo() {
    }

    public StorageCreateVo(Date addTime, Integer id, String key, String name, Integer size, String type, Date updateTime, String url) {
        this.addTime = addTime;
        this.id = id;
        this.key = key;
        this.name = name;
        this.size = size;
        this.type = type;
        this.updateTime = updateTime;
        this.url = url;
    }
}
