package com.cskaoyan.model.admin.goodsbean.vo;

import lombok.Data;

import java.util.List;

/**
 * @Author AhaNg
 * @Date 2022/1/7 17:37
 * @description:
 * @return:
 */

@Data
public class CategoryListVo {
    Integer value;
    String label;
    List<CategoryListOfChildrenVo> children;

    public CategoryListVo() {
    }

    public CategoryListVo(Integer value, String label) {
        this.value = value;
        this.label = label;
    }
}
