package com.cskaoyan.model.admin.goodsbean.vo;


import lombok.Data;

import java.util.List;

/**
 * @Author AhaNg
 * @Date 2022/1/7 17:37
 * @description:
 * @return:
 */
@Data
public class CatAndBrandVo {
    List<BrandListVo> brandList;
    List<CategoryListVo> categoryList;

    public CatAndBrandVo(List<BrandListVo> brandList, List<CategoryListVo> categoryList) {
        this.brandList = brandList;
        this.categoryList = categoryList;
    }

    public CatAndBrandVo() {
    }
}
