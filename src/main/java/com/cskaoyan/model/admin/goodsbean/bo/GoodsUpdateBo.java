package com.cskaoyan.model.admin.goodsbean.bo;


import com.cskaoyan.model.admin.goodsbean.Goods;
import com.cskaoyan.model.admin.goodsbean.GoodsAttribute;
import com.cskaoyan.model.admin.goodsbean.GoodsProduct;
import com.cskaoyan.model.admin.goodsbean.GoodsSpecification;
import lombok.Data;

import javax.validation.Valid;
import java.util.List;

/**
 * @Author AhaNg
 * @Date 2022/1/7 17:37
 * @description:
 * @return:
 */

@Data
public class GoodsUpdateBo {

    @Valid
    Goods goods;

    List<GoodsAttribute> attributes;

    List<GoodsSpecification> specifications;

    List<GoodsProduct> products;
}
