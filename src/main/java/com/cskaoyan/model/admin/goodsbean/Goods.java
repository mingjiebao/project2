package com.cskaoyan.model.admin.goodsbean;

import com.cskaoyan.annotation.IntValidate;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.math.BigDecimal;
import java.util.Date;

@Data
public class Goods {
    private Integer id;

    private String goodsSn;

    @NotEmpty(message = "商品名称必须要有")
    private String name;

    private Integer categoryId;

    private Integer brandId;

    private String[] gallery;

    private String keywords;

    private String brief;

    private Boolean isOnSale;

    private Short sortOrder;

    private String picUrl;

    private String shareUrl;

    private Boolean isNew;

    private Boolean isHot;

    private String unit;

//    @NonNegativeValidate(message = "市场零售价必须为正数")
    private BigDecimal counterPrice;

    private BigDecimal retailPrice;

    private Date addTime;

    private Date updateTime;

    private Boolean deleted;

    private String detail;

}