package com.cskaoyan.model.admin.goodsbean.bo.list;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author AhaNg
 * @Date 2022/1/7 17:45
 * @description: 分页查询商品的BO
 * @return:
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GoodsListBO {
    Integer page;
    Integer limit;
    String goodsSn;
    String name;
    String sort;
    String order;
    Integer goodsId;
}
