package com.cskaoyan.model.admin.goodsbean.vo;

import lombok.Data;

/**
 * @Author AhaNg
 * @Date 2022/1/7 17:37
 * @description:
 * @return:
 */

@Data
public class BrandListVo {
    Integer value;
    String label;

    public BrandListVo(Integer value, String label) {
        this.value = value;
        this.label = label;
    }

    public BrandListVo() {
    }
}
