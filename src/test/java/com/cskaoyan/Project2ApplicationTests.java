package com.cskaoyan;

import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class Project2ApplicationTests {

    @Test
    void contextLoads() {
        String accessKeyId ="LTAI5t8gpxPTCR6W58RnZq4u";
        String accessKeySecret = "mVPbpbxia0JQotb7HyJAREV8QUuq8h";
        String signName = "stone4j";
        String templateCode = "SMS_173765187";
        String phoneNumber = "15136202656";

        DefaultProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId, accessKeySecret);
        DefaultAcsClient client = new DefaultAcsClient(profile);

        CommonRequest request = new CommonRequest();
        request.setMethod(MethodType.POST);
        request.setDomain("dysmsapi.aliyuncs.com");
        request.setVersion("2017-05-25");
        request.setAction("SendSms");
        request.putQueryParameter("RegionId","cn-hangzhou");
        request.putQueryParameter("PhoneNumbers",phoneNumber);
        request.putQueryParameter("TemplateCode",templateCode);
        request.putQueryParameter("SignName",signName);
        request.putQueryParameter("TemplateParam","{\"code\":\"65536\"}");

        try {
            CommonResponse response = client.getCommonResponse(request);
            System.out.println(response.getData());
        } catch (ClientException e) {
            e.printStackTrace();
        }
    }

}
